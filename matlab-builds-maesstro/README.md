# matlab-builds for MAESSTRO

## Presentation

This package contains matlab-builds for MAESSTRO

## Installation
Get the source codes by cloning this repo with git
```
git clone https://gitlab.com/benjamin.elie/maesstro.git

```
or by downloading its content.

## Matlab Runtime

Some modules of MAESSTRO need to execute compiled Matlab codes using the Matlab Runtime Compiler (MRC), which is freely available. The version that is used is 9.4 (R2018a), that you can download at :
```
https://fr.mathworks.com/products/compiler/matlab-runtime.html
```

Once you downloaded the right version of MRC, unzip the file and move the files in a temporary folder. Then, from a terminal, inside the temp folder, execute the following line to launch the installation in silent mode
```
./install -mode silent -agreeToLicense yes
```

It is necessary to change the environment variable 'LD_LIBRARY_PATH' so that the proper MATLAB libraries are used when the codes are launched. For that purpose, you should add the following lines to your .bashrc file:

```
export MCR_PATH=/usr/local/MATLAB/MATLAB_Runtime/v94
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$MCR_PATH/runtime/glnxa64::$MCR_PATH/bin/glnxa64::$MCR_PATH/sys/os/glnxa64:
```

In order to compile the MATLAB codes using the MATLAB Runtime Compiler, you can simply execute the file 'compile_module.m' located at the root of the maesstro-matlab folder. The executable files are then stored in the folder named 'matlab-builds', which will be created if it does not exist yet.

You should also configure the path to the different sources by editing the file named 'config.ini', which is located in '/home/user/.config/maesstro/'. If you do not find this file, you can create it using this command line in a terminal
```
maesstro init-config
```

Several paths are required:
* 'modes-table': path to the MATLAB executable file that computes the modal basis of the soundboard
* 'mouvement-table': path to the MATLAB executable file that computes the motion of the soundboard
* 'rayonnement': path to the MATLAB executable file that computes the azcoustic radiation of the soundboard
* 'runtime-path': path to the MRC compiler
* 'path': path to the Montjoie library

Usually, the path to the MATLAB executables files look like the following:
```
$MATLAB_EXE/matlab-builds/<name_of_the_module_linux>,
```
with '$MATLAB_EXE' being the path to the 'matlab-builds' folder


## Dependencies

MAESSTRO may require to use external libraries that should be installed independently. These dependencies include:
* [Montjoie](http://montjoie.gforge.inria.fr/), a C++ library used to compute the modal basis of the soundboard using finite-element models
* [GMSH](https://gmsh.info/), a 2-D and 3-D free mesh generator

Visit the webpages to know how to install these libraries. 


## Updates

MAESSTRO is in its development stage, so that you may expect regular available updates. Simply run the following command in the maesstro folder
```
git pull origin master
```

## Contact

For questions and remarks, please contact me at bnjmn.elie (at) gmail.com

## Papers to cite

If you publish results based on MAESSTRO and if you :
* used the finite-element computation of the soundboard modal basis, please cite [1] and [2],
* used the semi-analytical model for the computation of the modal basis, please cite [3],

In any case, please cite the original paper describing the software [4]

## References:
[1] J. Chabassier, A. Chaigne, P. Joly, Modeling and simulation of a grand piano, The Journal of the Acoustical Society of America 134(1), pp. 648-655, 2013

[2] C. Geuzaine and J.-F. Remacle. Gmsh: a three-dimensional finite element mesh generator with built-in pre- and post-processing facilities. International Journal for Numerical Methods in Engineering 79(11), pp. 1309-1331, 2009

[3] B. Trévisan, K. Ege, B. Laulagnet, A modal approach to piano soundboard vibroacoustic behavior, Journal of the Acoustical Society of America 141(2), pp. 690–709, 2017

[4] B. Elie, X. Boutillon, J. Chabassier, K. Ege, B. Laulagnet, B. Trévisan, B. Cotté, N. Chauvat, MAESSTRO: A sound synthesis framework for Computer-Aided Design of piano soundboards, in ISMA 2019, Detmold, Germany, 2019.

Please visit the website of the MAESSTRO project for further information
```
https://www.maesstro.cnrs.fr/
https://www.maesstro.cnrs.fr/cao-sonore/
```

