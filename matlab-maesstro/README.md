# MAESSTRO Matlab toolbox

## Presentation

This part of MESSTRO is a matlab toolbox that:
* computes the modal basis of virtual soundboard using the semi-analytical method from [1]
* generates 2D mesh of virtual soundboards for finite-element methods
* computes the soundboard displacement and acoustic radiation for a given string force


## Installation
Step 1. Get the source codes by cloning this repo with git
```
git clone https://gitlab.com/benjamin.elie/maesstro.git

```
or by downloading its content.

Step 2. If you want the Matlab toolbox to be automatically added tou your MATLAB path when you launch MATLAB, just add the following line in your `pathdef.m` file (usually in `$MATLABROOT/toolbox/local/`)
```
'/path_to_maesstro/matlab-maesstro:', ...

```
Add this line somewhere between `%%% BEGIN ENTRIES %%%` and `%%% END ENTRIES %%%`. If you want MAESSTRO to be in your MATLAB workspace only when asked to, add the folloing line to your scripts

```
addpath(genpath('/path_to_maesstro/matlab-maesstro'));
```

* WARNING: Please, do not modify anything in the maesstro folder, including demo files, unless you want to modify the repository.

## Dependencies

MAESSTRO may require to use external libraries that should be installed independently. These dependencies include:
* [Montjoie](http://montjoie.gforge.inria.fr/), a C++ library used to compute the modal basis of the soundboard using finite-element models
* [GMSH](https://gmsh.info/), a 2-D and 3-D free mesh generator

Visit the webpages to know how to install these libraries. 

## Updates

MAESSTRO is in its development stage, so that you may expect regular available updates. Simply run the following command in the maesstro folder
```
git pull origin master
```

## Contact

For questions and remarks, please contact me at bnjmn.elie (at) gmail.com

## Papers to cite

If you publish results based on MAESSTRO, please cite the original paper describing the software [2]

## References:
[1] B. Trévisan, K. Ege, B. Laulagnet, A modal approach to piano soundboard vibroacoustic behavior, Journal of the Acoustical Society of America 141(2), pp. 690–709, 2017
[2] B. Elie, X. Boutillon, J. Chabassier, K. Ege, B. Laulagnet, B. Trévisan, B. Cotté, N. Chauvat, MAESSTRO: A sound synthesis framework for Computer-Aided Design of piano soundboards, in ISMA 2019, Detmold, Germany, 2019.


Please visit the website of the MAESSTRO project for further information
```
https://www.maesstro.cnrs.fr/
https://www.maesstro.cnrs.fr/cao-sonore/
```

