function readdata(obj, file)
% read bridge data stored in the .xls file specified by file

try
    a = xlsread(file);
catch
    error('file is not a valid xls file');
end

obj.mass_density = a(1,:);
obj.deport = a(2,:);
obj.thickness = a(3,:);
obj.width = a(4,:);
% obj.length = a(4);
obj.young_modulus = a(5,:);
obj.shear_yz = a(6,:);
obj.shear_xz = a(7,:);
obj.y_position = a(8,:);
obj.damping = a(9,:);
obj.initial_position = a(10,:);
obj.final_position = a(11,:);
obj.number = size(a,2);

end
