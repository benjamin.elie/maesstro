classdef Bridge < handle
    % Bridge is a class that allows simulations of bridge dynmaics of a piano
    % soundboard
    
    properties
        
        % Geometry 
        deport = [];
        width = [];
        length = [];
        thickness = [];
        area = [];
        number = [];
        x_normalized = [];
        y_normalized = [];
        obj_angle = [];
        x_extended = [];
        y_extended = [];
        x_poly = [];
        y_poly = [];
        poly = [];
        
        % Mechanical properties
        material = [];
        mass_density = [];  
        young_modulus = [];
        poisson_coeff = [];
        shear_coeff = [];
        damping = [];        
        orthotropy_angle = [];
        mass = [];
        rigidity_vector = [];
        mass_matrix_flexion = [];
        stiffness_matrix_flexion = [];
        mass_matrix_torsion = [];
        stiffness_matrix_torsion = [];
        flexion_inertia = [];
        torsion_inertia = [];
        rigidity = [];  
        basis_dim = [];
        
    end
    
    methods
        
        % Constructor method
        function obj = Bridge(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        % read bridge date stored in the .xls file specified by file
        readdata(obj, file)
        
        % generate the mass and stiffness matrix of the bridge
        [ M_bridge_flexion, K_bridge_flexion, M_bridge_torsion, K_bridge_torsion ] = computebridgematrix( obj, dimPlate, xDim, yDim )   
        
        cleanmat(obj)
    end
    
end

