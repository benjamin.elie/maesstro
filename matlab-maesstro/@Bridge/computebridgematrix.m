function [ M_bridge_flexion, K_bridge_flexion, M_bridge_torsion, K_bridge_torsion ] = computebridgematrix( obj, dimPlate, xDim, yDim )
% generate the mass and stiffness matrix of the plate

% Input arguments:
% xdim: number of modes in the x direction
% ydim: number of modes in the y direction
%
% Output arguments:
% M_plate: mass matrix (square matrix of size (xDim*yDim)x(xDim*yDim))
% K_plate: stiffness matrix (square matrix of size (xDim*yDim)x(xDim*yDim))

if nargin < 2; dimPlate = [ 1 1 ]; end
if nargin < 3; xDim = 18; end
if nargin < 4; yDim = 16; end

b = obj.width;
H = obj.thickness;
dep = obj.deport;
Gxz_c = obj.shear_coeff(1);
Gyz_c = obj.shear_coeff(2);
l = dimPlate(1);
L = dimPlate(2);
rho_c = obj.mass_density;
yc = obj.y_extended(1:end-1);
x_ini = obj.x_extended(1:end-1);
x_fin = obj.x_extended(2:end);

Hb3 = H.*b.^3;
If = 1/3*b.*((H+dep).^3-dep.^3);
Ig = Hb3/12+((H+dep).^3-dep.^3).*b/3;
Dg = Gyz_c.*If+1/12*Gxz_c.*Hb3;
piWidth = pi/l;
piLength = pi/L;
EIf = obj.young_modulus(1).*If*piWidth^2;
Dgpi = Dg*piLength^2*piWidth;
Ifpi = If*piWidth^2;

M_bridge_flexion = 0;
K_bridge_flexion = 0;
M_bridge_torsion = 0;
K_bridge_torsion = 0;

qvec = 1:yDim;
pvec = 1:xDim;
[ Cq, Cp ] = meshgrid(qvec, pvec);
Cq = Cq(:);
Cp = Cp(:);
Cpt = Cp.';
dC = Cp-Cpt;
idx = find(dC==0);
[ r, ~ ] = ind2sub(size(dC), idx);

for kbr = 1:obj.number
    g0mp = 1/2*(l./dC/pi.*(sin(dC*pi/l*x_fin(kbr))-sin(dC*pi/l*x_ini(kbr)))-l./(Cp+Cpt)/pi.*(sin((Cp+Cpt)*pi/l*x_fin(kbr))-sin((Cp+Cpt)*pi/l*x_ini(kbr)))); 
    g0mp(idx) = 1/2*(x_fin(kbr)-x_ini(kbr)-l/2./Cp(r)/pi.*(sin(2*Cp(r)*pi/l*x_fin(kbr))-sin(2*Cp(r)*pi/l*x_ini(kbr))));
    g1mp = 1/2*(l./dC/pi.*(sin(dC*pi/l*x_fin(kbr))-sin(dC*pi/l*x_ini(kbr)))+l./(Cp+Cpt)/pi.*(sin((Cp+Cpt)*pi/l*x_fin(kbr))-sin((Cp+Cpt)*pi/l*x_ini(kbr))));
    g1mp(idx) = 1/2*(x_fin(kbr)-x_ini(kbr)+l/2./Cp(r)/pi.*(sin(2*Cp(r)*pi/l*x_fin(kbr))-sin(2*Cp(r)*pi/l*x_ini(kbr)))); 
    
    Mftmp = rho_c(kbr).*(b*H(kbr).*g0mp+(Cp*Cp.')*Ifpi(kbr).*g1mp).*(sin(Cq*piLength*yc(kbr))*sin(Cq.'*piLength*yc(kbr)));
    M_flex_barre = Mftmp;
    clear Mftmp
    Mttmp = rho_c(kbr).*Ig(kbr)*(Cq*piLength.*cos(Cq*piLength*yc(kbr)))*(cos(Cq.'*piLength*yc(kbr)).*Cq.'*piLength).*g0mp; 
    M_tor_1_barre = Mttmp;
    clear Mttmp
    Kftmp = EIf(kbr)*(Cp*(Cp.'*piWidth)).^2.*(sin(Cq*piLength*yc(kbr))*sin(Cq.'*piLength*yc(kbr))).*g0mp;
    K_flex_barre = Kftmp;
    clear Kftmp    
    Kttmp = Dgpi(kbr)*piWidth*g1mp.*(Cp*Cp.').*((Cq.*cos(Cq*piLength*yc(kbr)))*(cos(Cq.'*piLength*yc(kbr)).*Cq.'));
    K_tor_1_barre = Kttmp;
    clear Kttmp

    M_flex_barre(abs(M_flex_barre) < 1e-10) = 0;
    K_flex_barre(abs(M_flex_barre) < 1e-10) = 0;
    M_bridge_flexion = M_bridge_flexion+M_flex_barre;
    K_bridge_flexion = K_bridge_flexion+K_flex_barre;

    M_tor_1_barre(abs(M_tor_1_barre) < 1e-10) = 0;
    K_tor_1_barre(abs(M_tor_1_barre) < 1e-10) = 0;
    M_bridge_torsion = M_bridge_torsion+M_tor_1_barre;
    K_bridge_torsion = K_bridge_torsion+K_tor_1_barre;

end

clear M_flex_barre K_flex_barre M_tor_1_barre K_tor_1_barre
obj.flexion_inertia = If;
obj.torsion_inertia = Ig;
obj.rigidity = Dg;
obj.mass_matrix_flexion = M_bridge_flexion;
obj.stiffness_matrix_flexion = K_bridge_flexion;
obj.mass_matrix_torsion = M_bridge_torsion;
obj.stiffness_matrix_torsion = K_bridge_torsion;
obj.basis_dim = [ xDim, yDim ];

end