function cleanmat(obj)
% clear big matrices

obj.mass_matrix_flexion = [];
obj.stiffness_matrix_flexion = [];
obj.mass_matrix_torsion = [];
obj.stiffness_matrix_torsion = [];

end

