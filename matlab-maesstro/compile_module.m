function compile_module(mname)

if nargin < 1; mname = 'all'; end

if ~exist('../matlab-builds', 'dir')
    mkdir('../matlab-builds')
end
if ~exist('../matlab-builds/MODES_TABLE_linux', 'dir')
    mkdir('../matlab-builds/MODES_TABLE_linux')
    mkdir('../matlab-builds/MOUVEMENT_TABLE_linux')
    mkdir('../matlab-builds/RAYONNEMENT_linux')
    mkdir('../matlab-builds/MODES_TABLE_linux/for_redistribution_files_only/')
    mkdir('../matlab-builds/MOUVEMENT_TABLE_linux/for_redistribution_files_only/')
    mkdir('../matlab-builds/RAYONNEMENT_linux/for_redistribution_files_only/')
end

if ~iscell(mname)
    switch mname
        case 'all'

            %%% Compile MODES_TABLE
            disp('Compilation of MODES_TABLE started')
            mcc -m ./MODES_TABLE.m ...
                -a ./@Soundboard/*.m ...
                -a ./@Ribs/*.m ...
                -a ./@Bridge/*.m ... 
                -a ./@Geometry/*.m ... 
                -a ./@Element/*.m ... 
                -a ./readjson.m ...
                -a ./loadND.m ...
                -d ../matlab-builds/MODES_TABLE_linux/for_redistribution_files_only/
            disp('Compilation of MODES_TABLE done')

            disp('Compilation of MOUVEMENT_TABLE started')
            %%% Compile MOUVEMENT_TABLE
            mcc -m ./MOUVEMENT_TABLE.m ...          
                -d ../matlab-builds/MOUVEMENT_TABLE_linux/for_redistribution_files_only
            disp('Compilation of MOUVEMENT_TABLE done')

            disp('Compilation of RAYONNEMENT started')
            %%% Compile RAYONNEMENT
            mcc -m ./RAYONNEMENT.m ...
                -a ./Coordonnee_contribution.m ...
                -d ../matlab-builds/RAYONNEMENT_linux/for_redistribution_files_only
            disp('Compilation of RAYONNEMENT done')
        
        case 'modes-table'
            
            %%% Compile MODES_TABLE
            disp('Compilation of MODES_TABLE started')
            mcc -m ./MODES_TABLE.m ...
                -a ./@Soundboard/*.m ...
                -a ./@Ribs/*.m ...
                -a ./@Bridge/*.m ... 
                -a ./readjson.m ...
                -a ./loadND.m ...
                -d ../matlab-builds/MODES_TABLE_linux/for_redistribution_files_only
            disp('Compilation of MODES_TABLE done')
            
        case 'mouvement-table'
            
            disp('Compilation of MOUVEMENT_TABLE started')
            %%% Compile MOUVEMENT_TABLE
            mcc -m ./MOUVEMENT_TABLE.m ...   
                -d ../matlab-builds/MOUVEMENT_TABLE_linux/for_redistribution_files_only
            disp('Compilation of MOUVEMENT_TABLE done')
            
        case 'rayonnement'
            
            disp('Compilation of RAYONNEMENT started')
            %%% Compile RAYONNEMENT
            mcc -m ./RAYONNEMENT.m ...  
                -a ./Coordonnee_contribution.m ...
                -d ../matlab-builds/RAYONNEMENT_linux/for_redistribution_files_only
            disp('Compilation of RAYONNEMENT done')
            
        otherwise
            error('Module name is wrong')
            
    end
    
else
    for k = 1:length(mname)
        mtmp = mname{k};
        switch mtmp
            case 'modes_table'
            
            %%% Compile MODES_TABLE
            disp('Compilation of MODES_TABLE started')
            mcc -m ./MODES_TABLE.m ...
                -a ./@Soundboard/*.m ...
                -a ./@Ribs/*.m ...
                -a ./@Bridge/*.m ... 
                -a ./readjson.m ...
                -a ./loadND.m ...
                -d ../matlab-builds/MODES_TABLE_linux/for_redistribution_files_only
            disp('Compilation of MODES_TABLE done')
            
        case 'mouvement_table'
            
            disp('Compilation of MOUVEMENT_TABLE started')
            %%% Compile MOUVEMENT_TABLE
            mcc -m ./MOUVEMENT_TABLE.m ... 
                -d ../matlab-builds/MOUVEMENT_TABLE_linux/for_redistribution_files_only
            disp('Compilation of MOUVEMENT_TABLE done')
            
        case 'rayonnement'
            
            disp('Compilation of RAYONNEMENT started')
            %%% Compile RAYONNEMENT
            mcc -m ./RAYONNEMENT.m ...      
                -a ./Coordonnee_contribution.m ...
                -d ../matlab-builds/RAYONNEMENT_linux/for_redistribution_files_only
            disp('Compilation of RAYONNEMENT done')
            
        otherwise
            error('Module name is wrong')
        end
    end
end   

end

