function [ M_ribs_flexion, K_ribs_flexion, M_ribs_torsion, K_ribs_torsion ] = computeribsmatrix( obj, dimPlate, xDim, yDim )
% generate the mass and stiffness matrix of the plate

% Input arguments:
% xdim: number of modes in the x direction
% ydim: number of modes in the y direction
%
% Output arguments:
% M_plate: mass matrix (square matrix of size (xDim*yDim)x(xDim*yDim))
% K_plate: stiffness matrix (square matrix of size (xDim*yDim)x(xDim*yDim))

if nargin < 2; dimPlate = [ 1 1 ]; end
if nargin < 3; xDim = 18; end
if nargin < 4; yDim = 16; end

b = 1/2*(obj.width(1:end-1)+obj.width(2:end));
H = obj.thickness;
H = min(H(1:end-1), H(2:end));

dep = obj.deport;
Gxy_r = obj.shear_coeff(1);
Gyz_r = obj.shear_coeff(2);
l = dimPlate(1);
L = dimPlate(2);
rho_r = obj.mass_density*ones(size(b));
xr = obj.x_extended(1:end-1);
y_ini = obj.y_extended(1:end-1);
y_fin = obj.y_extended(2:end);

Hb = H.*b;
Hb3 = H.*b.^3;
If = 1/3*b.*((H+dep).^3-dep.^3);
Ig = Hb3/12+((H+dep).^3-dep.^3).*b/3;
Dg = Gxy_r.*If+1/12*Gyz_r.*Hb3;
piWidth = pi/l;
piLength = pi/L;
Dgpi = Dg*piWidth*piLength;
Hb3pi = Hb3/12*piWidth^2;
Igrho = rho_r.*Ig*piWidth;
Ifpi = If*piLength^2;
EIf = obj.young_modulus(1).*Ifpi;

M_ribs_flexion = 0;
K_ribs_flexion = 0;
M_ribs_torsion = 0;
K_ribs_torsion = 0;

qvec = 1:yDim;
pvec = 1:xDim;

[ Cq, Cp ] = meshgrid(qvec, pvec);
Cq = Cq(:);
Cp = Cp(:);
Cqt = Cq.';
dC = Cq-Cqt;
idx = find(dC==0);
[ r, ~ ] = ind2sub(size(dC), idx);

for kribs = 1:obj.number

    g0nq = 1/2*(L./dC/pi.*(sin(dC*pi/L*y_fin(kribs))-sin(dC*pi/L*y_ini(kribs)))-L./(Cq+Cqt)/pi.*(sin((Cq+Cqt)*pi/L*y_fin(kribs))-sin((Cq+Cqt)*pi/L*y_ini(kribs))));       
    g0nq(idx) = 1/2*(y_fin(kribs)-y_ini(kribs)-L/2./Cq(r)/pi.*(sin(2*Cq(r)*pi/L*y_fin(kribs))-sin(2*Cq(r)*pi/L*y_ini(kribs))));  
    g1nq = 1/2*(L./dC/pi.*(sin(dC*pi/L*y_fin(kribs))-sin(dC*pi/L*y_ini(kribs)))+L./(Cq+Cqt)/pi.*(sin((Cq+Cqt)*pi/L*y_fin(kribs))-sin((Cq+Cqt)*pi/L*y_ini(kribs))));
    g1nq(idx) = 1/2*(y_fin(kribs)-y_ini(kribs)+L/2./Cq(r)/pi.*(sin(2*Cq(r)*pi/L*y_fin(kribs))-sin(2*Cq(r)*pi/L*y_ini(kribs))));
    Mftmp = rho_r(kribs).*(Hb(kribs)*g0nq+Ifpi(kribs)*(Cq*Cq.').*g1nq- ...
        (Cp*Cp.')*Hb3pi(kribs).*g0nq).*(sin(Cp*piWidth*xr(kribs))*sin(Cp*piWidth*xr(kribs)).');
    M_flex_barre = Mftmp;
    clear Mftmp
    
    Kftmp = EIf(kribs)*(Cq.^2.*sin(Cp*piWidth*xr(kribs)))*((Cq.'*piLength).^2.*sin(Cp.'*piWidth*xr(kribs))).*g0nq;
        
    K_flex_barre = Kftmp;
    clear Kftmp

    Mttmp = (Cp*Igrho(kribs).*cos(Cp*piWidth*xr(kribs)))*(Cp.'*piWidth.*cos(Cp.'*piWidth*xr(kribs))).*g0nq; 
    M_tor_1_barre = Mttmp; 
    clear Mttmp
    Kttmp = (Dgpi(kribs)*Cq.*Cp.*cos(Cp*piWidth*xr(kribs)))*(Cp.'*piWidth.*Cq.'*piLength.*cos(Cp.'*piWidth*xr(kribs))).*g1nq;       
    K_tor_1_barre = Kttmp;
    clear Kttmp;   

    M_flex_barre(abs(M_flex_barre) < 1e-10) = 0;
    K_flex_barre(abs(K_flex_barre) < 1e-10) = 0;
    M_ribs_flexion = M_ribs_flexion+M_flex_barre;
    K_ribs_flexion = K_ribs_flexion+K_flex_barre;

    M_tor_1_barre(abs(M_tor_1_barre) < 1e-10) = 0;
    K_tor_1_barre(abs(K_tor_1_barre) < 1e-10) = 0;
    M_ribs_torsion = M_ribs_torsion+M_tor_1_barre;
    K_ribs_torsion = K_ribs_torsion+K_tor_1_barre;
   
end

clear M_flex_barre K_flex_barre M_tor_1_barre K_tor_1_barre

obj.flexion_inertia = If;
obj.torsion_inertia = Ig;
obj.rigidity = Dg;
obj.mass_matrix_flexion = M_ribs_flexion; 
obj.stiffness_matrix_flexion = K_ribs_flexion;
obj.mass_matrix_torsion = M_ribs_torsion;
obj.stiffness_matrix_torsion = K_ribs_torsion;
obj.basis_dim = [ xDim, yDim ];

end