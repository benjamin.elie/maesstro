%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fonction read_parameters : read the parameters in the ini file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ max_freq, modalsolver, nbr_p, nbr_q, k_block, l_px, l_py, dt, c_s, rho_a, x_ac, y_ac, z_ac ] = read_parameters(fname)

if exist(fname,'file')
    dataout = textscan(fopen(fname),'%s','Delimiter','\n');
    dataout = dataout{1};
     
    idx = find(cellfun(@(x)contains(x,'max_freq'),dataout));
    if ~isempty(idx)
        tmp = dataout{idx};
        max_freq = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Max freq is not defined');
    end
    
    idx = find(cellfun(@(x)contains(x,'solver'),dataout));
    if ~isempty(idx)
        tmp = dataout{idx};
        modalsolver = tmp(strfind(tmp,'=')+2:end);
    else
        warning('Solver is not defined. Montjoie is used as default');
        modalsolver = 'montjoie';
    end
    
    idx = find(cellfun(@(x)contains(x,'n_xsin'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        nbr_p = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Number of x sinus not defined');
    end
    
    idx = find(cellfun(@(x)contains(x,'n_ysin'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        nbr_q = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Number of y sinus not defined');
    end
    
    idx = find(cellfun(@(x)contains(x,'spring_stiffness'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        k_block = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Spring stiffness not defined');
    end
    
    idx = find(cellfun(@(x)contains(x,'l_px'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        l_px = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('L_x is not defined');
    end
    
    idx = find(cellfun(@(x)contains(x,'l_py'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        l_py = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('L_y not defined');
    end
    
    idx = find(cellfun(@(x)contains(x,'time_step'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        dt = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Time step not defined');
    end    
    
    idx = find(cellfun(@(x)contains(x,'sound_velocity'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        c_s = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Sound velocity not defined');
    end    
    
    idx = find(cellfun(@(x)contains(x,'air_density'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        rho_a = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('Air density not defined');
    end    
    
    idx = find(cellfun(@(x)contains(x,'x_ac'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        x_ac = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('x_ac not defined');
    end    
    
    idx = find(cellfun(@(x)contains(x,'y_ac'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        y_ac = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('y_ac not defined');
    end    
    
    idx = find(cellfun(@(x)contains(x,'z_ac'),dataout));
    if ~isempty(idx)
         tmp = dataout{idx};
        z_ac = str2double(tmp(strfind(tmp,'=')+1:end));
    else
        error('z_ac not defined');
    end    
    
else    
    error(['File ' fname ' does not exist.']);
end