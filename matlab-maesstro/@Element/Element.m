classdef Element < Geometry
    % Element defines the geometry of each physical surface of the mesh
    
    properties
        mechanical_properties = [];
        x_coordinates = [];
        y_coordinates = [];
        thickness = [];
        point_index = [];
        line_index = [];
        index = [];        
    end
    
    methods
        % Constructor method
        function obj = Element(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
    end
end

