function [ modalDensity, freqVec ] = computemodaldensity(obj, ord)
% modalDensity = computemodaldensity(obj, ord)
% compute the modal density of the soundboard object

% Input arguments:
% obj: Soundboard object
% ord: order of the moving average filter applied to the eigenmode spacing
%
% Output arguments:
% modalDensity: modal density of the soundboard object
% freqVec: frequency vector
%
% MAESSTRO, 2019
%
% See also Soundboard, Ribs, Bridge

fk = obj.eigen_frequencies;
df = diff(fk);
df_smooth = smooth(1./df, ord, 'rlowess');
modalDensity = df_smooth;
freqVec = fk(1:end-1) + df/2;
obj.modal_density_frequency_vector = freqVec;
obj.modal_density = modalDensity;

end

