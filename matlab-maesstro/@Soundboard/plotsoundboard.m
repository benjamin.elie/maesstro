function out_fig = plotsoundboard(obj, typedata, fig_handle)
% plotsoundboard(obj, typedata)
% method of Soundboard objects to plot the geometry
% 
% Input arguments:
% obj: Soundboard object
% typedata: specify whether you want to plot in the real geometry or the
% simplified geometry in the extended soundboard space
% fig_handle: specify a figure handle into which the geometry should be
% plotted
% 
%
% MAESSTRO, 2019
%
% See also Soundboard, Ribs, Bridge

if nargin < 2; typedata = 'normalized'; end
if nargin < 3; fig_handle = []; end

if isempty(fig_handle)
    fig_handle = figure;
else
    figure(fig_handle);
end

switch typedata    
    case 'normalized'
        xsb = obj.x_normalized;
        ysb = obj.y_normalized;
        hold on;
        plot(xsb, ysb, 'linewidth',2)        
        axis equal
        set(gca,'ticklabelinterpreter','latex')

        for k = 1 : length(obj.ribs)
            rtmp = obj.ribs{k};
            
            if isempty(rtmp.x_poly)            
                xr = rtmp.x_normalized;
                yr = rtmp.y_normalized;
                thtrib = -deg2rad(rtmp.obj_angle);
                wr = rtmp.width;

                xi = xr*cos(thtrib)-yr*sin(thtrib);
                yi = xr*sin(thtrib)+yr*cos(thtrib);
                [ xis, isrt ] = sort(xi, 'ascend');
                yis = yi(isrt);
                xtmp = [ xis(:); flipud(xis(:)); xis(1) ];
                ytmp = [ yis(:)-wr(:)/2 ; flipud(yis(:))+wr(:)/2; yis(1)-wr(1)/2 ];
                xi = xtmp*cos(thtrib)+ytmp*sin(thtrib);
                yi = -xtmp*sin(thtrib)+ytmp*cos(thtrib);
                
            else
                xi = rtmp.x_poly;
                yi = rtmp.y_poly;
            end
            poly = polyshape(xi, yi);
             plot(poly,'facecolor',[0.8 0.8 0.8 ], 'edgecolor',[0.8 0.8 0.8 ],'facealpha',1)
        end

        for k = 1 : length(obj.bridge)
            rtmp = obj.bridge{k};
            if isempty(rtmp.x_poly)   
                thtrib = -deg2rad(rtmp.obj_angle);
                wr = rtmp.width;
                xr = rtmp.x_normalized;
                yr = rtmp.y_normalized;

                xi = xr*cos(thtrib)-yr*sin(thtrib);
                yi = xr*sin(thtrib)+yr*cos(thtrib);
                [ xis, isrt ] = sort(xi, 'ascend');
                yis = yi(isrt);
                xtmp = [ xis(:); flipud(xis(:)); xis(1) ];
                ytmp = [ yis(:)-wr(1)/2 ; flipud(yis(:))+wr/2; yis(1)-wr(1)/2 ];
                xi = xtmp*cos(thtrib)+ytmp*sin(thtrib);
                yi = -xtmp*sin(thtrib)+ytmp*cos(thtrib);
                poly = polyshape(xi, yi);
            else
                poly = rtmp.poly;                
            end
            plot(poly, 'facecolor',[0.8 0.8 0.8 ], 'edgecolor',[0.8 0.8 0.8 ],'facealpha',1)
            
        end
    case 'code'        
        xsb = obj.x_extended;
        ysb = obj.y_extended;
        hold on;
        plot(xsb, ysb)
        axis equal
        set(gca,'ticklabelinterpreter','latex')

        for k = 1 : length(obj.ribs)
            rtmp = obj.ribs{k};
            xr = rtmp.x_extended;
            yr = rtmp.y_extended;            
            thtrib = -deg2rad(rtmp.obj_angle-90);
            wr = rtmp.width;
            
            xi = xr*cos(thtrib)-yr*sin(thtrib);
            yi = xr*sin(thtrib)+yr*cos(thtrib);
            [ xis, isrt ] = sort(xi, 'ascend');
            yis = yi(isrt);
            xtmp = [ xis(:); flipud(xis(:)); xis(1) ];
            ytmp = [ yis(:)-wr(:)/2 ; flipud(yis(:))+wr(:)/2; yis(1)-wr(1)/2 ];
            xi = xtmp*cos(thtrib)+ytmp*sin(thtrib);
            yi = -xtmp*sin(thtrib)+ytmp*cos(thtrib);

            plot(xi, yi, 'color',[0 0.498 0])

        end
        
        for k = 1 : length(obj.bridge)
            rtmp = obj.bridge{k};
            wr = rtmp.width;
            xr = rtmp.x_extended;
            yr = rtmp.y_extended;
            
            xi = xr*cos(thtrib)-yr*sin(thtrib);
            yi = xr*sin(thtrib)+yr*cos(thtrib);
            [ xis, isrt ] = sort(xi, 'ascend');
            yis = yi(isrt);
            xtmp = [ xis(:); flipud(xis(:)); xis(1) ];
            ytmp = [ yis(:)-wr(:)/2 ; flipud(yis(:))+wr(:)/2; yis(1)-wr(1)/2 ];
            xi = xtmp*cos(thtrib)+ytmp*sin(thtrib);
            yi = -xtmp*sin(thtrib)+ytmp*cos(thtrib);            
            plot(xi, yi, 'r')
        end
        
end

if nargout > 0
    out_fig = fig_handle;
end

end

