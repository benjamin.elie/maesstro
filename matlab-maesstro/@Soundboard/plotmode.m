function plotmode(obj, Nmode, isSubplot, isRibs)
% plot the mode shape

if nargin < 2; Nmode = 1; end
if nargin < 3; isSubplot = false; end
if nargin < 4; isRibs = true; end
if length(Nmode) == 1; isSubplot = false; end

objBridge = obj.bridge;
objRibs = obj.ribs;

xDim = obj.basis_dim(1);
yDim = obj.basis_dim(2);
Mr = obj.modal_mass;

L = obj.length;
l = obj.width;

xNode = 130;
yNode = round(xNode*L/l);
[Cx, Cy ] = meshgrid(0:l/xNode:l,0:L/yNode:L);

if strcmp(obj.solver,'lva')
    xvec = 0:l/xNode:l;
    yvec = 0:L/yNode:L;
elseif strcmp(obj.solver,'montjoie')
    xmin = min(obj.x_normalized);
    ymin = min(obj.y_normalized);
    xvec = xmin:l/xNode:l+xmin;
    yvec = ymin:L/yNode:L+ymin;
end

% modalBasis = zeros(yNode+1,xNode+1,xDim*yDim);
modalBasis = zeros((yNode+1)*(xNode+1),xDim*yDim);
cpt=1;
for q=1:yDim
    sinql = sin(q*pi.*Cy(:)/L);
    for p=1:xDim    
        modalBasis(:,cpt) = sin(p*pi.*Cx(:)/l).*sinql;
        cpt = cpt + 1;
    end
end

if isSubplot
    figure;
    if length(Nmode) == 2
        Nx = 2;
        Ny = 1; 
    else
        Nx = ceil(sqrt(length(Nmode)));
        Ny = Nx;    
    end
end

kplt = 1;
ZmaxTot = 0;

if strcmp(obj.solver, 'lva')
    xctr = obj.x_extended;
    yctr = obj.y_extended;
    xsb = obj.x_normalized;
    ysb = obj.y_normalized;
    thet = -deg2rad(obj.orthotropy_angle+180);

    xtr = xsb*cos(thet)-ysb*sin(thet);
    ytr = xsb*sin(thet)+ysb*cos(thet);
    xorig = min(xtr);
    yorig = min(ytr);
else
    xctr = obj.x_normalized;
    yctr = obj.y_normalized;
end

[ xmsh, ymsh ] = meshgrid(xvec, yvec);

for km = Nmode
    U = obj.eigen_vectors(:,km);

    fk = obj.eigen_frequencies(km);
    fkstr = num2str(fk, '%.2f');

    modeShape = reshape(modalBasis*U(:),[yNode+1,xNode+1])/sqrt(Mr(km));
    clear Mtmp

    % Create figure
    if isSubplot 
        subplot(Ny, Nx, kplt);        
    else
        figure;
    end
    
    kmText = num2str(km);
    switch kmText
        case '1'
            expScript = '$^{st}$';
        case '2'
            expScript = '$^{nd}$';
        case '3'
            expScript = '$^{rd}$';
        otherwise
            expScript = '$^{th}$';
    end
    
    % imagesc(Cx, Cy, modeShape); axis xy
    signmedian = sign(median(modeShape(:)));
    if signmedian == 0; signmedian = 1; end
    modeShape = modeShape*signmedian;

    xall = xmsh(:);
    yall = ymsh(:);
    modeShapevec = modeShape(:);
    
    if strcmp(obj.solver,'lva')
        xctr = obj.x_extended;
        yctr = obj.y_extended;
    end

    [ in, on ] = inpolygon(xall, yall, xctr, yctr);
    
    if strcmp(obj.solver,'lva')
        xnewall = (xall+xorig)*cos(thet)+(yall+yorig)*sin(thet);
        ynewall = -(xall+xorig)*sin(thet)+(yall+yorig)*cos(thet);
        xctr = obj.x_normalized;
        yctr = obj.y_normalized;
        xall = xnewall;
        yall = ynewall;
    end

    h = scatter(xall(in | on), yall(in | on), 25, modeShapevec(in | on), 'filled', 's');
    hold on;
    
    hP(kplt) = h.Parent;
    hold on;
    title([num2str(km),expScript,' mode at ',fkstr,' Hz'],'FontSize',16,'interpreter','latex')
    kplt = kplt+1;
    
    
    Zmax = max(abs(modeShape(:)));
    if Zmax > ZmaxTot; ZmaxTot = Zmax; end

    Z = [-ZmaxTot, ZmaxTot];
    if isRibs 
        for kbrg = 1:length(objBridge)

            if strcmp(obj.solver,'lva')
                xall = objBridge{kbrg}.x_extended;
                yall = objBridge{kbrg}.y_extended;
                Xbridge = (xall+xorig)*cos(thet)+(yall+yorig)*sin(thet);
                Ybridge = -(xall+xorig)*sin(thet)+(yall+yorig)*cos(thet);
            else
                Xbridge = objBridge{kbrg}.x_poly;
                Ybridge = objBridge{kbrg}.y_poly;
            end
            plot(Xbridge,Ybridge,'linewidth',4,'color',[0.65,0.65,0.65])
            
        end
        
        for kribs = 1:length(objRibs)
            
            if strcmp(obj.solver,'lva')
                xall = objRibs{kribs}.x_extended;
                yall = objRibs{kribs}.y_extended;
                Xribs = (xall+xorig)*cos(thet)+(yall+yorig)*sin(thet);
                Yribs = -(xall+xorig)*sin(thet)+(yall+yorig)*cos(thet);
            else
                Xribs = objRibs{kribs}.x_poly;
                Yribs = objRibs{kribs}.y_poly;
            end
            for kr = 1:objRibs{kribs}.number 
                if min(objRibs{kribs}.thickness) > 0.007
                    plot(Xribs,Yribs,'linewidth',4,'color',[0.65,0.65,0.65])  
                else
                    plot(Xribs,Yribs,'linewidth',4,'color',[0.85,0.85,0.85])  
                end
            end
        end
        
    end

    caxis(Z)
    colorbar
    axis equal
    if strcmp(obj.solver,'montjoie')
        xlim([xmin xmin+l]);
        ylim([ymin ymin+L]);
    end

    set(gca,'xtick',[],'ytick',[],'ztick',[])
	plot(xctr, yctr, 'k', 'linewidth',5)

	set(gcf, 'units','normalized','position',[0 0 1 1 ])    
    axis off

end % for Nmode
% Link z axis
% if length(hP) > 1
%     for k = 1:length(hP)
%         hP(k).CLim = Z;
%     end
% end

