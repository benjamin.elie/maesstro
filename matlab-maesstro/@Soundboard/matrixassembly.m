function [ massMatrix,stiffMatrix ] = matrixassembly(obj)
%   generate the general matrix of the system

objRibs = obj.ribs;
objBridge = obj.bridge;

Mbr_flex = summat(objBridge,'mass_matrix_flexion');
Mbr_tor = summat(objBridge,'mass_matrix_torsion');
Mribs_flex = summat(objRibs,'mass_matrix_flexion');
Mribs_tor = summat(objRibs,'mass_matrix_torsion');

massMatrix = obj.panel_mass_matrix + Mbr_flex + Mbr_tor + Mribs_flex + Mribs_tor;

Kbr_flex = summat(objBridge,'stiffness_matrix_flexion');
Kbr_tor = summat(objBridge,'stiffness_matrix_torsion');
Kribs_flex = summat(objRibs,'stiffness_matrix_flexion');
Kribs_tor = summat(objRibs,'stiffness_matrix_torsion');

stiffMatrix = obj.extended_matrix + obj.panel_stiffness_matrix + ...
    Kbr_flex + Kbr_tor + Kribs_flex + Kribs_tor;

obj.mass_matrix = massMatrix;
obj.stiffness_matrix = stiffMatrix;

end

function s = summat(x,field)

    s = 0;
    for k = 1:length(x)
        s = s+x{k}.(field);
    end

end
