function convergencestudy( obj,fmax, red )
% study the convergence of modal truncature

if nargin < 2; fmax = 1000; end
if nargin < 3; red = 3; end

xDim = obj.basis_dim(1);
yDim = obj.basis_dim(2);
damp = obj.damping;

fmode = fmax*(1+damp/2);
highMode = find(obj.eigen_frequencies > fmode,1,'first');

RD_P = xDim*0.15;
RD_Q = yDim*0.15;

if (xDim-2*round(RD_P))*(yDim-2*round(RD_Q)) < highMode
    BOX = error('error : Truncature is not sufficient'); 
end

eigenmat = zeros(3,highMode);


Fc = 62.5*2.^(0:11);  % third-octave bands
[~,idx]=min(abs(Fc*sqrt(2)-fmax));
Fc = Fc(1:idx);
Nmat = zeros(3,idx); 

for ktrunc = 1:red

    NP(ktrunc) = xDim + round(RD_P*(1-ktrunc));
    NQ(ktrunc) = yDim + round(RD_Q*(1-ktrunc));

    if ktrunc == 1
        MT = obj.mass_matrix;
        KT = obj.stiffness_matrix;
    else
        MT = matrixreduction(xDim,NP(ktrunc),NQ(ktrunc),obj.mass_matrix);
        KT = matrixreduction(xDim,NP(ktrunc),NQ(ktrunc),obj.stiffness_matrix);
    end


    % New modal basis
    [~,lbd] = eig(MT\KT);

    % val_propres_ligne = diag(val_propres);
    lbd = sort(diag(lbd));
    eigenmodes = sqrt(lbd)/(2*pi);
    eigenmat(ktrunc,:) = eigenmodes(1:highMode) ;
    
    n_model = zeros(idx,1);
    for jjk=1:idx
        cpt = 0;
        for jjj=1:length(eigenmodes)
            if eigenmodes(jjj) >= Fc(jjk)/2^(1/2) && eigenmodes(jjj) <= Fc(jjk)*2^(1/2)
                cpt = cpt + 1;
            end
        end

        n_model(jjk) = cpt/(Fc(jjk)*2^(1/2) - Fc(jjk)/2^(1/2));

    end

    Nmat(ktrunc,:) = n_model;
end

scolor = pink(round(red*1.5));
figure('position',[300 300 1000 400])

axesHandles = subplot(1,2,1);
hold on
clear NUM_MODE2 RATIO_F RATIO_V legh3
NUM_MODE2 = 1 : red-1;
ratioMat = zeros(red-1,1);

for jqr=1:red-1
    ratioMat(jqr) = eigenmat(red-jqr,highMode)/eigenmat(red-jqr+1,highMode)*100 -100;
    legh3 {jqr} =  sprintf(['(',num2str(NP(red - jqr)),',',num2str(NQ(red - jqr)),') / (',num2str(NP(red + 1 - jqr)),',',num2str(NQ(red + 1 - jqr)),')']);    
end

ax=plot(NUM_MODE2,abs(ratioMat),'linestyle','--','Marker','v','linewidth',2,'color',[0 0.4 0.9]);

box on
grid on
xlim([0 red])
% ylim([0 5])
xlabel(axesHandles,'(P,Q) ratio =','FontSize',16,'fontweight','bold');
ylabel(axesHandles,'Frequency ratio (%)','FontSize',16,'fontweight','bold');
set(axesHandles,'XTick',1:red-1,'XTickLabel',legh3)
xticklabel_rotate([],35,[],'Fontsize',14)
title(['frequency convergence of mode ',num2str(highMode)],'FontWeight','bold','FontSize',16);

set(axesHandles,'position',[0.0600    0.2775    0.41    0.6475])

%%%%% Modal density
axesHandle = subplot(1,2,2);

hold on
for i=1:length(NP)  
    hN(i)=plot(Fc,Nmat(i,:),'o--','color',scolor(i,:),'linewidth',2,'markersize',10) ;
	legh{i} =  sprintf(['(P,Q) = (',num2str(NP(i)),',',num2str(NQ(i)),')']);
end

box on
xlabel('Frequency (Hz)','FontSize',16,'fontweight','bold');
ylabel('n(f) (modes.Hz^-^1)','FontSize',16,'fontweight','bold');
title(['Modal density by octave band '],'FontWeight','bold','FontSize',16);
set(gca,'xscale','log')
xlim([min(Fc) max(Fc)]);
grid on
legend(legh,'Location','northwest')
set(axesHandle,'position',[0.5703    0.1100    0.41    0.8150])


end

function M = matrixreduction(Po,Pn,Qn,Mat)
% Allow the reduction of a matrix Mat in order to change the truncation order
% - Po and Qo the old order of trunctaion
% - Pn and Qn the new order of truncation
% Note : Pn and Qn could be below Po and Qo


% STEP 1: Reduction on the order Q. The new matrix called has a
% truncation order of (Po,Qn)
M = Mat(1:Po*Qn,1:Po*Qn);

% STEP 2: Reduction on the order P. At the end, "Matrix" has a trucation
% order of (Pn,Qn)
if Pn ~= Po
    cpt = 0;
    for ok = 1:Qn
        for ou = Pn+1:Po

            M( : , ou + Po*(ok-1) - cpt ) = []; % remove column
            M( ou + Po*(ok-1) - cpt , : ) = []; % remove line

            % Matter : the matrix changes its size and we have to add a cpt in order to
            % know where the good terms we have to remove are.
            cpt = cpt+1;
        end
    end

end

end

