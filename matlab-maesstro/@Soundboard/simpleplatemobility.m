function Y = simpleplatemobility( obj, pos, fvec )
% Synthese de reponse impulsionnelle d'une plaque plane.
%
% Arguments d'entree:
% 
% L : dimensions de la plaque selon x et y (en m)
% epp : epaisseur de la palque (en m)
% rho : masse volumique du materiau (en kg/m^3)
% D : module de rigidite (en N.m)
% eta : facteur de perte
% pos : position (x,y) du point d'excitation et d'observation (en m)
% Fe : frequence d'echantillonnage (en Hz)
% tmax : duree du signal de synthese en s (defaut = 1)
% w : vecteur pulsation (defaut = 2*pi*[0 Fe/2])
%
% Arguments de sortie :
%
% h : reponse impulsionnelle du systeme physique
% Y : reponse en frequence
% t : vecteur temps
% theta : parametres modaux
%
% Benjamin ELIE Janvier 2013


if nargin < 2; pos = [ obj.length/2; obj.width/2 ]; end

w = fvec*2*pi;

N_f = length(w);

M = obj.mass;
D = obj.rigidity;
L = [ obj.width, obj.length ];
epp = mean(obj.thickness);
rho = obj.mass_density;
eta = obj.damping;

x = pos(1);
y = pos(2);

nbr_p = obj.basis_dim(1);
nbr_q = obj.basis_dim(2);

w_nm = zeros(nbr_p, nbr_q); % Inialisation de la matrice w_nm, 500 pour prendre large
w_k = zeros(numel(w_nm),1);
eta_k = w_k;
Amp = eta_k;
iter = 1;

for n = 1:nbr_p % On prend large...       
    for m = 1:nbr_q % On prend large...       
        wsquare = pi^4/(rho*epp)*(D(1)*(n/L(1))^4+D(3)*(m/L(2))^4+(D(2)+D(4))*m^2*n^2/prod(L.^2));
        wnm = sqrt(wsquare); % Pulsation propre du mode (m,n) (en rad/s)   
        w_nm(n,m) = wnm; 
        w_k(iter) = wnm;
        eta_k(iter) = eta;
        Def1 = sin(n*pi*x/L(1))*sin(m*pi*y/L(2));%*2/sqrt(M);
        Def2 = sin(n*pi*x/L(1))*sin(m*pi*y/L(2));%*2/sqrt(M);
        b = Def1.*Def2;
        Amp(iter) = b;
        iter = iter + 1; % On indique au programme qu'on a fait un mode   
    end
end

[~,Ind] = sort(w_k);
w_k = w_k(Ind);
Amp = Amp(Ind);
eta_k = eta_k(Ind);
nModes = length(Amp);
fk = w_k/2/pi;

wk = fk*2*pi;
P = zeros(N_f,nModes);
bk = Amp(:);

for i = 1:nModes    
    P(:,i) = 1i*w./(wk(i)^2+1i*eta_k(i)*wk(i)*w-w.^2);    
end

Y = P*bk/M;
obj.mobility = Y;

end
