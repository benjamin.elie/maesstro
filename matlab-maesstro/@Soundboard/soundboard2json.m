function soundboard2json(obj, filename)
% soundboard2json(obj, filename)
% method of Soundboard objects to export the geometry into JSON files
% 
% Input arguments:
% obj: Soundboard object
% filename: path to the output JSON file
%
% MAESSTRO, 2019
%
% See also Soundboard, readjson

nbrg = length(obj.bridge);
nrib = length(obj.ribs);
id = struct('panel',[],'bridges',[],'ribs',[]);
id.panel.materialId = obj.material;
id.panel.contour = struct('x',cell(length(obj.x_extended)-1,1),'y',cell(length(obj.x_extended)-1,1),'thickness',cell(length(obj.x_extended)-1,1));
id.panel.orthotropicAngleDeg = 0;

for k = 1:length(id.panel.contour)
    id.panel.contour(k).x = obj.x_extended(k)*1000;
    id.panel.contour(k).y = obj.y_extended(k)*1000;
    id.panel.contour(k).thickness = obj.thickness(k)*1000;
end

id.bridges = cell(nbrg,1);

for k = 1:nbrg
    id.bridges{k}.medianLine(1).x = obj.bridge{k}.x_extended(1)*1000;
    id.bridges{k}.medianLine(1).y = obj.bridge{k}.y_extended(1)*1000;
    id.bridges{k}.medianLine(1).height = obj.bridge{k}.thickness*1000;
    id.bridges{k}.medianLine(1).width = obj.bridge{k}.width*1000;
    id.bridges{k}.medianLine(1).isReference = 'false';
    id.bridges{k}.medianLine(2).x = obj.bridge{k}.x_extended(2)*1000;
    id.bridges{k}.medianLine(2).y = obj.bridge{k}.y_extended(2)*1000;
    id.bridges{k}.medianLine(2).height = obj.bridge{k}.thickness*1000;
    id.bridges{k}.medianLine(2).width = obj.bridge{k}.width*1000;
    id.bridges{k}.medianLine(2).isReference = 'false';
    id.bridges{k}.materialId = obj.bridge{k}.material;
end

id.ribs = cell(nrib,1);
for k = 1:nrib
    id.ribs{k}.start.x = obj.ribs{k}.x_extended(1)*1000;
    id.ribs{k}.start.y = obj.ribs{k}.y_extended(1)*1000;
    id.ribs{k}.start.width = obj.ribs{k}.width(1)*1000;
    id.ribs{k}.start.height = obj.ribs{k}.thickness(1)*1000;
    id.ribs{k}.end.x = obj.ribs{k}.x_extended(2)*1000;
    id.ribs{k}.end.y = obj.ribs{k}.y_extended(2)*1000;
    id.ribs{k}.end.width = obj.ribs{k}.width(2)*1000;
    id.ribs{k}.end.height = obj.ribs{k}.thickness(2)*1000;
    id.ribs{k}.materialId = obj.ribs{k}.material;
end

txt = jsonencode(id);

fid = fopen(filename,'w');
fwrite(fid,txt,'char')
fclose(fid);

end

