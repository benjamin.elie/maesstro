function Y = computemobility( obj, excPoint, obsPoint, freqVec)
% Y = computemobility( obj, excPoint, obsPoint, freqVec)
% compute the mobility of the soundboard at points defined by excPoint and
% obsPoint.

% Input arguments:
% obj: Soundboard object
% excPoint: Nx2 Matrix containing the N excitation points, the first column
% contains the abscissa and the second the ordinates
% obsPoint: Nx2 Matrix containing the N observation points, the first column
% contains the abscissa and the second the ordinates
% freqVec: frequency for which the mobility is computed
%
% Output argument:
% Y: complex admittance of the SOundboard object
%
% MAESSTRO, 2019
%
% See also Soundboard, Ribs, Bridge

eta = obj.damping;
xDim = obj.basis_dim(1);
yDim = obj.basis_dim(2);
L = obj.length;
l = obj.width;
nMode = xDim*yDim;
nPts = size(excPoint,1);
nFreq = length(freqVec);
omg = freqVec*2*pi;
genForce = zeros(nMode, nPts);

U = obj.eigen_vectors;
mr = obj.modal_mass;
kr = obj.modal_stiffness;
Mr = diag(mr);
Kr = diag(kr);
modemax = length(obj.modal_mass);
lbd = (2*pi*obj.eigen_frequencies).^2;

% Damping matrix
Cr = eye(modemax)*eta*sqrt(diag(lbd))*Mr;
cr = diag(Cr);

if strcmpi(obj.solver,'montjoie')
    Xmin = min(obj.x_normalized);
    Ymin = min(obj.y_normalized);
else
    Xmin = 0;
    Ymin = 0;
end
xe = excPoint(:,1)-Xmin;
ye = excPoint(:,2)-Ymin;
xo = obsPoint(:,1)-Xmin;
yo = obsPoint(:,2)-Ymin;
cpt = 1;

for q=1:yDim
    sinql = sin(q*pi.*ye/L);
    for p=1:xDim    
        genForce(cpt,:) = sin(p*pi.*xe/l).*sinql;
        cpt = cpt + 1;
    end
end
genForce2 = U.'*genForce;

a = zeros(modemax,nFreq, nPts);
for k = 1:nFreq    
   elem = (kr-mr*omg(k)^2 + 1i*omg(k)*cr );
   a(:, k,:) = diag(1./elem)*genForce2;
end
Pa = zeros(nMode,nFreq, nPts);
for k = 1:nPts
    Pa(:,:,k) = U*squeeze(a(:,:,k));
end

gF = repmat(reshape(genForce,[nMode, 1, nPts]), [1, nFreq, 1]);

W = reshape(sum(Pa.*gF,1),[nFreq,nPts]);
Y = 1i*repmat(omg(:),[1, nPts]).*W;

obj.mobility = Y;

end

