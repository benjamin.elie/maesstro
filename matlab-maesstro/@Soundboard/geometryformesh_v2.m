function geometryformesh_v2(obj, fmax, isOrtho, N, discOrder)
% build elements and geometry objects to build mesh for montjoie


if nargin < 2; fmax = 3e3; end
if nargin < 3; isOrtho = false; end
if nargin < 4; N = 4; end
if nargin < 5; discOrder = 1; end

%%%% Check if object is properly defined %%%%%
if isempty(obj.x_normalized)
    error('Soundboard geometry not defined')
end
% 
if isempty(obj.mass_density)
    error('Soundboard property not defined')
end

if isempty(obj.rigidity)
    Ex = obj.young_modulus(1);
    Ey = obj.young_modulus(2);
    nuxy = obj.poisson_coeff(1);
    nuyx = obj.poisson_coeff(2);
    h = mean(obj.thickness);
    D1 = (Ex*h^3)/(12*(1-nuxy*nuyx));
    D3 = (Ey*h^3)/(12*(1-nuxy*nuyx));
    D2 = (Ex*nuyx*h^3)/(6*(1-nuxy*nuyx));
    D4 = (obj.shear_coeff(1)*h^3)/3;
    obj.rigidity = [D1, D2, D3, D4];
end

if ~isempty(obj.ribs)
    if isempty(obj.ribs{1}.x_poly)
        obj.geometry;
    end
end

if ~isempty(obj.bridge)
    if isempty(obj.bridge{1}.x_poly)
        obj.geometry;
    end
end
    
sbthick = mean(obj.thickness);
sbthet = obj.orthotropy_angle;
dthet = +90;

xsb = obj.x_normalized;
ysb = obj.y_normalized;

if (xsb(1) == xsb(end)) && (ysb(1) == ysb(end))
    xsb = xsb(1:end-1);
    ysb = ysb(1:end-1);
end

sbpoly = polyshape;
sbpoly.Vertices = [ xsb(:), ysb(:) ];
lc = 1/N*sqrt(2*pi/fmax)*(min(obj.rigidity([1 3]))/obj.mass_density/sbthick)^0.25*discOrder;
geom = Geometry('characteristic_length',lc,'discretization_order',discOrder);

% geom.characteristic_length = geom.characteristic_length*geom.discretization_order;
obj.montjoiemesh = geom;
% 
nudePanel = Element('x_coordinates',xsb,'y_coordinates',ysb, ...
    'thickness',sbthick);
% 
sbnude = sbpoly;

SB_prop = [ obj.mass_density, obj.young_modulus, obj.poisson_coeff, obj.shear_coeff, 0.85, 0.85 ]; 
nudePanel.mechanical_properties = [' ORTHOTROPE ',...
    num2str(SB_prop,'%.2g '),' ANGLE ',num2str(sbthet)];
geom.elements = {nudePanel};

%%%% Now for ribs
polyribunion = polyshape;
polybridgeunion = polyshape;

if ~isempty(obj.ribs) % Check if soundboard has ribs
    polyribarray = [];
    thickRibArray = zeros(length(obj.ribs));
    for kr = 1:length(obj.ribs)
        rtmp = obj.ribs{kr};
        xsb = rtmp.x_poly;
        ysb = rtmp.y_poly;
        if (xsb(1) == xsb(end)) && (ysb(1) == ysb(end))
            xsb = xsb(1:end-1);
            ysb = ysb(1:end-1);
        end
        polyrib = polyshape(xsb, ysb);
        polyribarray = [ polyribarray, polyrib ];
        thickRibArray(kr) = min(rtmp.thickness);
        polyribunion = union(polyribunion, polyrib);
    end
end

if ~isempty(obj.bridge) % Check if soundboard has bridge
    polybridgearray = [];
    thickBridgeArray = cell(length(obj.bridge),1);
    for kr = 1:length(obj.bridge)
        rtmp = obj.bridge{kr};
        xsb = rtmp.x_poly;
        ysb = rtmp.y_poly;
        if (xsb(1) == xsb(end)) && (ysb(1) == ysb(end))
            xsb = xsb(1:end-1);
            ysb = ysb(1:end-1);
        end
        polybridge = polyshape(xsb, ysb);
        thickBridgeArray{kr} = 1/2*(rtmp.thickness(1:end-1)+rtmp.thickness(2:end));
        polybridgeunion = union(polybridgeunion, polybridge);
    end
end

superstruct = union(polybridgeunion, polyribunion);
sbpoly = subtract(sbpoly, superstruct);

for kr = 1:length(obj.ribs)
    polyrib = polyribarray(kr);
    polyint = intersect(polyrib, polybridgeunion);
    polysub = subtract(polyrib, polybridgeunion);

    if polyint.NumRegions > 0
        intRegions = regions(polyint);
        for k = 1:length(intRegions)
            [ xtmp, ytmp ] = intRegions(k).boundary;
            if xtmp(end)==xtmp(1) && ytmp(end) == ytmp(1)
                    xtmp = xtmp(1:end-1);
                    ytmp = ytmp(1:end-1);
            end
            rtmp = Element('x_coordinates',xtmp,'y_coordinates',ytmp);
            thrib = thickRibArray(kr);

            % Find which bridge
            bool = 0;
            kbr = 1;
            while ~bool
                polyBrTmp = obj.bridge{kbr}.poly;
                for kpoly = 1:length(polyBrTmp)
                    bool = overlaps(polyBrTmp(kpoly), intRegions(k)); 
                    if bool; break; end
                end
                if ~bool; kbr = kbr+1; end
            end          
            thbridge = thickBridgeArray{kbr}(kpoly);
            rtmp.thickness = thrib + thbridge + sbthick;

            rhotmp = (obj.mass_density*sbthick + ...
                obj.ribs{kr}.mass_density*thrib + ...
                obj.bridge{kbr}.mass_density*thbridge)/rtmp.thickness;

            %%%%% .ini file            
            if isOrtho
                props = [ rhotmp, obj.bridge{kbr}.young_modulus, ...
                obj.bridge{kbr}.poisson_coeff, obj.bridge{kbr}.shear_coeff, 0.85, 0.85 ];
                rtmp.mechanical_properties = ...
                    [' ORTHOTROPE ',num2str(props,'%.2g '),...
                    ' ANGLE ',num2str(sbthet)];   
            else
                props = [ rhotmp, obj.bridge{kbr}.young_modulus(1), ...
                obj.bridge{kbr}.poisson_coeff(1) ];
                rtmp.mechanical_properties = ...
                    [' YOUNG_POISSON ',num2str(props,'%.2g ')];    
            end
            geom.elements{end+1} = rtmp;
        end % for intregions
    end % if intregions

    if polysub.NumRegions > 0
        intRegions = regions(polysub);
        for k = 1:length(intRegions)
            [ xtmp, ytmp ] = intRegions(k).boundary;
            if xtmp(end)==xtmp(1) && ytmp(end) == ytmp(1)
                    xtmp = xtmp(1:end-1);
                    ytmp = ytmp(1:end-1);
            end
            rtmp = Element('x_coordinates',xtmp,'y_coordinates',ytmp);
            thrib = thickRibArray(kr); 
            rtmp.thickness = thrib + sbthick;

            rhotmp = (obj.mass_density*sbthick + ...
                obj.ribs{kr}.mass_density*thrib )/rtmp.thickness;

            %%%%% .ini file            
            if isOrtho
                props = [ rhotmp, obj.ribs{kr}.young_modulus, ...
                obj.ribs{kr}.poisson_coeff, obj.ribs{kr}.shear_coeff, 0.85, 0.85 ];
                rtmp.mechanical_properties = ...
                    [' ORTHOTROPE ',num2str(props,'%.2g '),...
                    ' ANGLE ',num2str(sbthet)];   
            else
                props = [ rhotmp, obj.ribs{kr}.young_modulus(1), ...
                obj.ribs{kr}.poisson_coeff(1) ];
                rtmp.mechanical_properties = ...
                    [' YOUNG_POISSON ',num2str(props,'%.2g ')];    
            end
            geom.elements{end+1} = rtmp;
        end % for subregions
    end % if subRegions
end % for ribs

for kbr = 1:length(obj.bridge)
    polytmp = obj.bridge{kbr}.poly;
    for kPolyBridge = 1:length(polytmp)
        polybridge = polytmp(kPolyBridge);
        polysub = subtract(polybridge, polyribunion);

        thbridge = thickBridgeArray{kbr}(kPolyBridge);
        if polysub.NumRegions > 0
            intRegions = regions(polysub);
            for k = 1:length(intRegions)
                [ xtmp, ytmp ] = intRegions(k).boundary;
                if xtmp(end)==xtmp(1) && ytmp(end) == ytmp(1)
                    xtmp = xtmp(1:end-1);
                    ytmp = ytmp(1:end-1);
                end
                rtmp = Element('x_coordinates',xtmp,'y_coordinates',ytmp);
                rtmp.thickness = thbridge + sbthick;

                rhotmp = (obj.mass_density*sbthick + ...
                    obj.bridge{kbr}.mass_density*thrib )/rtmp.thickness;

                %%%%% .ini file            
                if isOrtho
                    props = [ rhotmp, obj.bridge{kbr}.young_modulus, ...
                    obj.bridge{kbr}.poisson_coeff, obj.bridge{kbr}.shear_coeff, 0.85, 0.85 ];
                    rtmp.mechanical_properties = ...
                        [' ORTHOTROPE ',num2str(props,'%.2g '),...
                        ' ANGLE ',num2str(sbthet)];   
                else
                    props = [ rhotmp, obj.bridge{kbr}.young_modulus(1), ...
                    obj.bridge{kbr}.poisson_coeff(1) ];
                    rtmp.mechanical_properties = ...
                        [' YOUNG_POISSON ',num2str(props,'%.2g ')];    
                end
                geom.elements{end+1} = rtmp;
            end % for subregions
        end % if subRegions            
    end % for polybridge     
end % for nBridge

%%% Check for inner enclosed regions
polyout = regions(sbpoly);
if length(polyout) > 1
    for k = 2:length(polyout)
        xtmp = polyout(k).Vertices(:,1);
        ytmp = polyout(k).Vertices(:,2);
        if xtmp(end)==xtmp(1) && ytmp(end) == ytmp(1)
            xtmp = xtmp(1:end-1);
            ytmp = ytmp(1:end-1);
        end
        
        ytmp = ytmp(~isnan(ytmp));
        xtmp = xtmp(~isnan(xtmp));
        
        innerNudePanel = Element('x_coordinates',xtmp,'y_coordinates',ytmp, ...
            'thickness',sbthick);

        innerSB_prop = [ obj.mass_density, obj.young_modulus, obj.poisson_coeff, obj.shear_coeff, 0.85, 0.85 ]; 
        innerNudePanel.mechanical_properties = [' ORTHOTROPE ',...
            num2str(innerSB_prop,'%.2g '),' ANGLE ',num2str(sbthet)];
        geom.elements{end+1} = innerNudePanel;
    end
end

geom.rearrangelines
geom.rearrangegroups

end

