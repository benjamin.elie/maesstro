function initmontjoie(obj, centralFreq, numModes, k_ord)
%%% initialization for montjoie

if nargin < 2; centralFreq = 10; end
if nargin < 3; numModes = 20; end
if nargin < 4; k_ord = 3; end
geom = obj.montjoiemesh;
geom.center_frequency = centralFreq;
geom.writemeshfile
geom.discretization_order = k_ord;
tol = 1e-4;
status = 1;
while status
    status = system('gmsh geofile.geo -2');
    if status
        warning('Refining Mesh');
        tol = tol*10;
        geom.rearrangelines(tol);
        geom.rearrangegroups
        geom = obj.montjoiemesh;
        geom.writemeshfile

    end
    if tol > 1
        error('Something is wrong with the geometry. Check log for details')
    end
end

geom.writeini(numModes)

obj.montjoiemesh = geom;

end

