function exportnormalizeddata( obj, outputFile )
% write output normalized in a hdf5 file, whose name is specified by
% outputFile

if nargin < 2; outputFile = 'SORTIES_MODES_TABLE.h5'; end
datasetname = {'/amortissements_modaux','/coefficients_deformees','/masses_modales','/raideurs_modales','/basis_dimension','/soundboard_dimension'};
dta{1} = obj.modal_damping;
dta{2} = obj.eigen_vectors;
dta{3} = obj.modal_mass;
dta{4} = obj.modal_stiffness;
dta{5} = obj.basis_dim;
dta{6} = [ obj.width, obj.length ];
if exist(outputFile,'file'); delete(outputFile); end

for k = 1:length(datasetname)
    dim = size(dta{k});
    h5create(outputFile,datasetname{k},dim)    
    h5write(outputFile,datasetname{k}, dta{k})
end

end

