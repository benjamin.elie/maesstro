function extendedplate( obj )
% define the extended soundboard geometry

xsb = obj.x_normalized;
ysb = obj.y_normalized;
thet = -deg2rad(obj.orthotropy_angle+180);

xtr = xsb*cos(thet)-ysb*sin(thet);
ytr = xsb*sin(thet)+ysb*cos(thet);
xorig = min(xtr);
yorig = min(ytr);

obj.origin = [ xorig, yorig ];
obj.x_extended = xtr-xorig;
obj.y_extended = ytr-yorig;

newplate_x = [ min(xtr); min(xtr); max(xtr); max(xtr) ];
newplate_y = [ min(ytr); max(ytr); max(ytr); min(ytr) ];
obj.length = max(newplate_y)-min(newplate_y);
obj.width = max(newplate_x)-min(newplate_x);

%%%% RIBS
cellRibs = obj.ribs;
for k = 1:length(cellRibs)
    rtmp = cellRibs{k};
    xi = rtmp.x_normalized*cos(thet)-rtmp.y_normalized*sin(thet)-xorig;
    yi = rtmp.x_normalized*sin(thet)+rtmp.y_normalized*cos(thet)-yorig;
    xi = 1/2*(xi(1)+xi(end))*ones(length(yi),1);
    [ xo, yo ] = replacepoints( xi, yi, obj.x_extended, obj.y_extended, 'x' );  
    rtmp.x_extended = xo;
    rtmp.y_extended = yo;
    rtmp.number = length(yo)-1;
end

%%%% Bridges
cellBr = obj.bridge;
for k = 1:length(cellBr)
    rtmp = cellBr{k};
    yi = rtmp.x_normalized([1 end])*sin(thet)+rtmp.y_normalized([1 end])*cos(thet)-yorig;
    xi = rtmp.x_normalized([1 end])*cos(thet)-rtmp.y_normalized([1 end])*sin(thet)-xorig;
    yi = mean(yi)*ones(2,1);
    [ xo, yo ] = replacepoints( xi, yi, obj.x_extended, obj.y_extended, 'y' );  
    rtmp.y_extended = yo;
    rtmp.x_extended = xo;
    rtmp.number = length(xo)-1;
    rtmp.width = mean(rtmp.width);
    rtmp.thickness = mean(rtmp.thickness);
end

end

function [ xo, yo ] = replacepoints( xi, yi, xp, yp, dim )
% replace points that are outside the soundboard to the soundboard
% boundaries

    xo = xi;
    yo = yi;
    in = inpolygon(xi, yi, xp, yp);
    
    if ~all(in)
        if strcmp(dim, 'x')
            xitmp = repmat(xi(1), 1e3, 1);
            yitmp = linspace(yi(1), yi(end), 1e3).';
        elseif strcmp(dim, 'y')
            yitmp = repmat(yi(1), 1e3, 1);
            xitmp = linspace(xi(1), xi(end), 1e3).';
        end
        intmp = inpolygon(xitmp, yitmp, xp, yp);
        xitmp = xitmp(intmp);
        yitmp = yitmp(intmp);  
        
        if strcmp(dim, 'x')
            yo(yo < min(yitmp)) = min(yitmp);
            yo(yo > max(yitmp)) = max(yitmp);
        elseif strcmp(dim, 'y')
            xo(xo < min(xitmp)) = min(xitmp);
            xo(xo > max(xitmp)) = max(xitmp);            
        end
    else
        if strcmp(dim, 'x')
            [ yo, isx ] = sort(yo, 'ascend');
            xo = xo(isx);
        elseif strcmp(dim, 'y')
            [ xo, isx ] = sort(xo, 'ascend');
            yo = yo(isx);           
        end
    end
end
