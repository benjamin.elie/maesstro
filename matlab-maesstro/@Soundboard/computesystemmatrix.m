function extMatrix = computesystemmatrix(obj, xDim, yDim)
% Compute the matrix of the soundboard panel

L = obj.length;
l = obj.width;
order = obj.order;

X_mk = obj.mesh(:,1);
Y_mk = obj.mesh(:,2);

Xmax = max(X_mk(:));
if Xmax/l > 100 % mesh probably in mm. Turn it back to meters
    warning('Meshgrid is probably in mm. Turn it back into meters!');
    obj.mesh = obj.mesh/1000;
    X_mk = obj.mesh(:,1);
    Y_mk = obj.mesh(:,2);
end

vec_tmp = zeros(xDim*yDim, order);%,'single');   

for q=1:yDim
    sinql = sin(q*pi*Y_mk/L);
    for p=1:xDim        
        Qtmp = sin(p*pi*X_mk/l).*sinql;
        vec_tmp(p+xDim*(q-1),:) = Qtmp;
    end
end

extMatrix = vec_tmp*vec_tmp.';
clear vec_tmp
obj.extended_matrix = obj.block_stiffness*extMatrix;

end
