function montjoiemodalfrequency(obj, max_freq)
% compute soundboard modes with montjoie

if nargin < 2; max_freq = 3e3; end

if ~exist('./MontjoieOutput/','dir')
    mkdir('./MontjoieOutput/');
end
tic

checkfile = dir('./MontjoieOutput/*.dat');
if ~isempty(checkfile)
    warning('Output files already exist, they will be overwritten')
    for k = 1:length(checkfile)
    	delete(['./MontjoieOutput/',checkfile(k).name]);
    end
end

% system('export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/montjoie/MONTJOIE/lib')
status = system('~/montjoie/MONTJOIE/soundboard.x montjoie.ini LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/montjoie/MONTJOIE/lib');
% status = system('~/montjoie/MONTJOIE/StringSoundboardStiffNL.x montjoie.ini LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:~/montjoie/MONTJOIE/lib');

if status
    error('Montjoie did not work properly. Check log for details');
end
be=toc;
disp(['Elapsed time to compute modal basis with Montjoie: ',num2str(be),' seconds'])
datlist = dir('*.dat');

for k = 1:length(datlist)
    movefile(datlist(k).name, './MontjoieOutput/');
end

lbd = load('./MontjoieOutput/eigen_values.dat');
fk = sqrt(lbd)/2/pi;
fk = fk(fk <= max_freq);
obj.eigen_frequencies = fk;

end
