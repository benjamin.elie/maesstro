function geometry(obj)
% define true soundboard geometry for mesh generation

    xsb = obj.x_normalized;
    ysb = obj.y_normalized;

    % check if contour is closed
    if ~(xsb(end) ==  xsb(1) && ysb(end) == ysb(1))
        xsb(end+1) = xsb(1);
        ysb(end+1) = ysb(1);
    end
%     [ xsb, ysb ] = interpconvexhull_v2( xsb, ysb, 500 );
%     
%     obj.x_normalized = xsb;
%     obj.y_normalized = ysb;

    d_edge = 2.5e-2;
    polysb = polyshape(xsb, ysb);

    for k = 1 : length(obj.ribs)
        rtmp = obj.ribs{k};
        xr = rtmp.x_normalized;
        yr = rtmp.y_normalized;
        thtrib = -deg2rad(rtmp.obj_angle);
        wr = rtmp.width;

        xi = xr*cos(thtrib)-yr*sin(thtrib);
        yi = xr*sin(thtrib)+yr*cos(thtrib);
        [ xis, isrt ] = sort(xi, 'ascend');
        yis = yi(isrt);
        xtmp = [ xis(:); flipud(xis(:)); xis(1) ];
        ytmp = [ yis(:)-wr(:)/2 ; flipud(yis(:))+wr(:)/2; yis(1)-wr(1)/2 ];
        xi = xtmp*cos(thtrib)+ytmp*sin(thtrib);
        yi = -xtmp*sin(thtrib)+ytmp*cos(thtrib);

        poly2 = polyshape(xi, yi);
        
        outofpanel = subtract(poly2, polysb);
        if ~isempty(outofpanel)
            poly2 = translate(poly2, -d_edge*cos(thtrib), d_edge*sin(thtrib));
            poly2 = intersect(poly2, polysb);
            poly2 = translate(poly2, d_edge*cos(thtrib), -d_edge*sin(thtrib));
            outofpanel = subtract(poly2, polysb);
            if ~isempty(outofpanel)
                poly2 = translate(poly2, d_edge*cos(thtrib), -d_edge*sin(thtrib));
                poly2 = intersect(poly2, polysb);
                poly2 = translate(poly2, -d_edge*cos(thtrib), d_edge*sin(thtrib));
            end
        end
        
        polyout = poly2;
        xi = [ polyout.Vertices(:,1); polyout.Vertices(1,1) ];
        yi = [ polyout.Vertices(:,2); polyout.Vertices(1,2) ];
        
        rtmp.x_poly = xi;
        rtmp.y_poly = yi; 
        rtmp.poly = polyshape(xi, yi);

    end

    for k = 1 : length(obj.bridge)

        polybridgeunion = polyshape;
        rtmp = obj.bridge{k};
        
        wr = rtmp.width;
        xr = rtmp.x_normalized;
        yr = rtmp.y_normalized;
        
        rtmp.poly = [];

        for kpts = 1:length(xr)-1 
            xvec = [ xr(kpts); xr(kpts+1) ];
            yvec = [ yr(kpts); yr(kpts+1) ];
            wvec = [ wr(kpts); wr(kpts+1) ];
            obj_angle = atand(diff(yvec)/diff(xvec));
            thtrib = -deg2rad(rtmp.obj_angle);
            xi = xvec*cos(thtrib)-yvec*sin(thtrib);
            yi = xvec*sin(thtrib)+yvec*cos(thtrib);
            xtmp = [ xi(:); flipud(xi(:)) ];
            ytmp = [ yi(:)-wvec(:)/2 ; flipud(yi(:)+wvec(:)/2) ];
            xi = xtmp*cos(thtrib)+ytmp*sin(thtrib);
            yi = -xtmp*sin(thtrib)+ytmp*cos(thtrib);
            
            if kpts > 1
                xi([ 1 4 ]) = xcons;
                yi([ 1 4 ]) = ycons;
            end            
            xcons = xi(2:3);
            ycons = yi(2:3);
            poly2 = polyshape(xi, yi);
            
            polyout = intersect(polysb,poly2);
            polybridgeunion = union(polybridgeunion, polyout);
           
            rtmp.poly = [ rtmp.poly, polyout ];
        end       
        
        xi = [ polybridgeunion.Vertices(:,1) ; polybridgeunion.Vertices(1,1) ];
        yi = [ polybridgeunion.Vertices(:,2) ; polybridgeunion.Vertices(1,2) ];
        rtmp.x_poly = xi;
        rtmp.y_poly = yi;
    end

end


function [ Xi, Yi ] = interpconvexhull_v2( Xch, Ych, D )
% linear interpolation of the convex hull with upsampling factor D

    N = length(Xch);
    Xi = [];
    Yi = [];
    for k = 1:N-1
        if Xch(k) == Xch(k+1)
            Yt = linspace(Ych(k), Ych(k+1), D);%         
            Yt(isnan(Yt)) = [];
            Xt = Xch(k)*ones(size(Yt));        
        else        
            p = polyfit(Xch(k:k+1), Ych(k:k+1), 1);
            Xt = linspace(Xch(k),Xch(k+1), D);
            Yt = polyval(p,Xt);
            Xt(isnan(Yt)) = [];
            Yt(isnan(Yt)) = [];        
        end    
        Xi = [ Xi ; Xt(:) ];
        Yi = [ Yi ; Yt(:) ];    
    end

    Cu = unique([Xi(:), Yi(:)], 'rows', 'stable');
    Xi = Cu(:,1); Yi = Cu(:,2);

end


