function extendedplate_v2( obj )
% define the extended soundboard geometry

xsb = obj.x_normalized;
ysb = obj.y_normalized;
thet = -deg2rad(obj.orthotropy_angle+180);

xtr = xsb*cos(thet)-ysb*sin(thet);
ytr = xsb*sin(thet)+ysb*cos(thet);
xorig = min(xtr);
yorig = min(ytr);

obj.x_extended = xtr-xorig;
obj.y_extended = ytr-yorig;

newplate_x = [ min(xtr); min(xtr); max(xtr); max(xtr) ];
newplate_y = [ min(ytr); max(ytr); max(ytr); min(ytr) ];
obj.length = max(newplate_y)-min(newplate_y);
obj.width = max(newplate_x)-min(newplate_x);

%%%% RIBS
cellRibs = obj.ribs;
xribs = zeros(length(cellRibs),1);
for k = 1:length(cellRibs)
    rtmp = cellRibs{k};
    xi = rtmp.x_normalized*cos(thet)-rtmp.y_normalized*sin(thet)-xorig;
    yi = rtmp.x_normalized*sin(thet)+rtmp.y_normalized*cos(thet)-yorig;
    xi = 1/2*(xi(1)+xi(end))*ones(length(yi),1);
    [ xo, yo ] = replacepoints( xi, yi, obj.x_extended, obj.y_extended, 'x' );  
    rtmp.x_extended = xo;
    rtmp.y_extended = yo;
    rtmp.number = length(yo)-1;
    xribs(k) = xi(1);
end

%%%% Bridges
cellBr = obj.bridge;
numitem = 1;

for k = 1:length(cellBr)
    rtmp = cellBr{k};
    yi = rtmp.x_normalized*sin(thet)+rtmp.y_normalized*cos(thet)-yorig;
    xi = rtmp.x_normalized*cos(thet)-rtmp.y_normalized*sin(thet)-xorig;
    
    H = rtmp.thickness;
    W = rtmp.width;
    x1 = min(xi);
    x2 = max(xi);
    xtmp = sort(xribs(xribs >= x1 & xribs <= x2));
    xtmp = [ x1; xtmp(:); x2 ];
    ytmp = interp1(xi,yi,xtmp);
    
   
    xmid = 1/2*(xtmp(1:end-1)+xtmp(2:end));
    Htmp = interp1(xi,H,xtmp);
    Hmid = interp1(xi,H,xmid);
    Wtmp = interp1(xi,W,xtmp);
    Wmid = interp1(xi,W,xmid);

    for kpt = 1:length(ytmp)
        
        switch kpt
            case 1
                xitmp = [ xtmp(1), xmid(1) ];
                Hitmp = [ Htmp(1), Hmid(1) ];
                Witmp = [ Wtmp(1), Wmid(1) ];
            case length(ytmp)
                xitmp = [xmid(end), xtmp(end) ];
                Hitmp = [ Hmid(end), Htmp(end) ];
                Witmp = [ Wmid(end), Wtmp(end) ];
            otherwise
                xitmp = [ xmid(kpt-1), xmid(kpt) ];
                Hitmp = [ Hmid(kpt-1), Hmid(kpt) ];
                Witmp = [ Wmid(kpt-1), Wmid(kpt) ];
        end

        finalCellBr{numitem,1} = Bridge('deport',rtmp.deport,'mass_density', ...
            rtmp.mass_density,'material',rtmp.material, ...
            'young_modulus',rtmp.young_modulus,'shear_coeff',rtmp.shear_coeff, ...
            'damping',rtmp.damping);
        finalCellBr{numitem,1}.thickness = mean(Hitmp);
        finalCellBr{numitem,1}.width = mean(Witmp);
        finalCellBr{numitem,1}.x_extended = xitmp;
        finalCellBr{numitem,1}.y_extended = repelem(ytmp(kpt),2);
        numitem = numitem+1;
        
    end   
end

if exist('finalCellBr','var')
    obj.bridge = finalCellBr;
end

end

function [ xo, yo ] = replacepoints( xi, yi, xp, yp, dim )
% replace points that are outside the soundboard to the soundboard
% boundaries

xo = xi;
    yo = yi;
    in = inpolygon(xi, yi, xp, yp);
    if ~all(in)
        if strcmp(dim, 'x')
            xitmp = repmat(xi(1), 1e3, 1);
            yitmp = linspace(yi(1), yi(end), 1e3).';
        elseif strcmp(dim, 'y')
            yitmp = repmat(yi(1), 1e3, 1);
            xitmp = linspace(xi(1), xi(end), 1e3).';
        end
        in = inpolygon(xitmp, yitmp, xp, yp);
        xitmp = xitmp(in);
        yitmp = yitmp(in);  
        xo = xitmp([1 end]);
        yo = yitmp([1 end]);
    end
end
