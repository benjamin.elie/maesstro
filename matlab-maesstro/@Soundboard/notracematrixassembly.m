function [ massMatrix,stiffMatrix ] = notracematrixassembly(obj, nbr_p, nbr_q)
%   generate the general matrix of the system

disp('Computation of the ribs matrix started')
tic;

%%%% RIBS DATA

Mbr_flex = 0;
Mbr_tor = 0;
Mribs_flex = 0;
Mribs_tor = 0;

Kbr_flex = 0;
Kbr_tor = 0;
Kribs_flex = 0;
Kribs_tor = 0;

for k = 1:length(obj.ribs)
   disp(['Computation of rib matrix ',num2str(k),'/',num2str(length(obj.ribs))])
   [ M_ribs_flexion, K_ribs_flexion, M_ribs_torsion, K_ribs_torsion ] = ...
       obj.ribs{k}.computeribsmatrix([obj.width obj.length],nbr_p, nbr_q);   
   Mribs_flex = Mribs_flex + M_ribs_flexion;
   Mribs_tor = Mribs_tor + M_ribs_torsion;
   Kribs_flex = Kribs_flex + K_ribs_flexion;
   Kribs_tor = Kribs_tor + K_ribs_torsion;
   clear M_ribs_flexion M_ribs_torsion 
   clear K_ribs_flexion K_ribs_torsion
   obj.ribs{k}.cleanmat;
   
end

massMatrix = obj.panel_mass_matrix + Mribs_flex + Mribs_tor;
clear Mribs_flex Mribs_tor
stiffMatrix = obj.extended_matrix + obj.panel_stiffness_matrix + ...
    Kribs_flex + Kribs_tor;
clear Kribs_flex Kribs_tor

obj.panel_mass_matrix = [];
obj.panel_stiffness_matrix = [];
obj.extended_matrix = [];

disp('Computation of the ribs matrix is done')    
be = toc;
disp(['Elapsed time to compute the ribs matrix: ',num2str(be),' seconds'])    
disp('Computation of the bridge matrix started')
tic;

%%%% Bridge DATA
for k = 1:length(obj.bridge)
    disp(['Computation of bridge matrix ',num2str(k),'/',num2str(length(obj.bridge))])
    [ M_bridge_flexion, K_bridge_flexion, M_bridge_torsion, K_bridge_torsion ] = ...
        obj.bridge{k}.computebridgematrix([obj.width obj.length],nbr_p, nbr_q);
   Mbr_flex = Mbr_flex + M_bridge_flexion;
   Mbr_tor = Mbr_tor + M_bridge_torsion;
   Kbr_flex = Kbr_flex + K_bridge_flexion;
   Kbr_tor = Kbr_tor + K_bridge_torsion;
   clear M_bridge_flexion M_bridge_torsion 
   clear K_bridge_flexion K_bridge_torsion
   obj.bridge{k}.cleanmat;
end

disp('Computation of the bridge matrix is done')    
be = toc;
disp(['Elapsed time to compute the bridge matrix: ',num2str(be),' seconds'])    

tic;
massMatrix = massMatrix + Mbr_flex + Mbr_tor;
stiffMatrix = stiffMatrix + Kbr_flex + Kbr_tor;

obj.mass_matrix = massMatrix;
obj.stiffness_matrix = stiffMatrix;

be = toc;
disp(['Elapsed time to compute the system matrix: ',num2str(be),' seconds'])    

end

