function readmaterials(obj, file)
% read material data from file

try
    matTab = readtable(file);
catch
    error('file is not a valid file');
end

mtrName = matTab.identifier; % Name of materials in the dictionary

%%% Soundboard
matsb = obj.material;
idx = find(cellfun(@(x)strcmpi(x,matsb), mtrName));
obj.mass_density = matTab.rho_kg_m_3_(idx);
obj.young_modulus = [ matTab.Ex_GPa_(idx),  matTab.Ey_GPa_(idx) ]*1e9;
obj.poisson_coeff = [ matTab.nuxy(idx), matTab.nuyx(idx) ];
obj.shear_coeff = [ matTab.Gxy_GPa_(idx), matTab.Gxz_GPa_(idx), matTab.Gyz_GPa_(idx) ]*1e9;
obj.damping = matTab.damping___(idx)/100;

%%% Ribs
for k = 1:length(obj.ribs)
    rtmp = obj.ribs{k};
    mattmp = rtmp.material;
    idx = find(cellfun(@(x)strcmpi(x,mattmp), mtrName));
    rtmp.mass_density = matTab.rho_kg_m_3_(idx);
    rtmp.young_modulus = [ matTab.Ex_GPa_(idx),  matTab.Ey_GPa_(idx) ]*1e9;
    rtmp.shear_coeff = [ matTab.Gxy_GPa_(idx), matTab.Gxz_GPa_(idx), matTab.Gyz_GPa_(idx) ]*1e9;
    rtmp.damping = matTab.damping___(idx)/100;
    rtmp.poisson_coeff = [ matTab.nuxy(idx), matTab.nuyx(idx) ];
end

%%% Bridges
for k = 1:length(obj.bridge)
    rtmp = obj.bridge{k};
    mattmp = rtmp.material;
    idx = find(cellfun(@(x)strcmpi(x,mattmp), mtrName));
    rtmp.mass_density = matTab.rho_kg_m_3_(idx);
    rtmp.young_modulus = [ matTab.Ex_GPa_(idx),  matTab.Ey_GPa_(idx) ]*1e9;
    rtmp.shear_coeff = [ matTab.Gxy_GPa_(idx), matTab.Gxz_GPa_(idx), matTab.Gyz_GPa_(idx) ]*1e9;
    rtmp.damping = matTab.damping___(idx)/100;
    rtmp.poisson_coeff = [ matTab.nuxy(idx), matTab.nuyx(idx) ];
end

Ex = obj.young_modulus(1);
Ey = obj.young_modulus(2);
nuxy = obj.poisson_coeff(1);
nuyx = obj.poisson_coeff(2);
h = mean(obj.thickness);

D1 = (Ex*h^3)/(12*(1-nuxy*nuyx));
D3 = (Ey*h^3)/(12*(1-nuxy*nuyx));
D2 = (Ex*nuyx*h^3)/(6*(1-nuxy*nuyx));
D4 = (obj.shear_coeff(1)*h^3)/3;
obj.rigidity = [ D1, D2, D3, D4 ];

end

