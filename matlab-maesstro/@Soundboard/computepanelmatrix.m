function [ M_panel, K_panel ] = computepanelmatrix( obj, xDim, yDim )
% generate the mass and stiffness matrix of the soundboard panel

% Input arguments:
% xdim: number of modes in the x direction
% ydim: number of modes in the y direction
%
% Output arguments:
% M_plate: mass matrix (square matrix of size (xDim*yDim)x(xDim*yDim))
% K_plate: stiffness matrix (square matrix of size (xDim*yDim)x(xDim*yDim))

if nargin < 2; xDim = 18; end
if nargin < 3; yDim = 16; end

h = mean(obj.thickness);
l = obj.width;
L = obj.length;

Mp = obj.mass_density*h*l*L/4;
D1 = obj.rigidity(1);
D3 = obj.rigidity(2);
D2 = obj.rigidity(3);
D4 = obj.rigidity(4);

M_panel = Mp*eye(xDim*yDim);
K_panel = zeros(xDim*yDim);

for q=1:yDim
   for p=1:xDim    
       K_panel(p+xDim*(q-1),p+xDim*(q-1)) = D1*(p*pi/l)^4+D3*(q*pi/L)^4+(D2+D4)*(p*pi/l)^2*(q*pi/L)^2;       
   end
end


K_panel=l*L/4*K_panel;

obj.mass = Mp*4;
obj.rigidity = [ D1 D2 D3 D4 ];
obj.panel_mass_matrix = M_panel;
obj.panel_stiffness_matrix = K_panel;
obj.basis_dim = [ xDim, yDim ];

end

