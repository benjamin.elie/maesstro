function [eigenmodes,eigenvectors] = modalbasis(obj, maxFreq)
% compute the modal basis of the soundboard

if nargin < 2; maxFreq = Inf; end
M = obj.mass_matrix;
K = obj.stiffness_matrix;
[ U, lbd ] = eig( M\K,'nobalance','vector');

[ lbd, s ] = sort(lbd);

eigenvectors = U(:,s);
eigenmodes = sqrt(lbd)/(2*pi);

mr = diag(eigenvectors.'*obj.mass_matrix*eigenvectors);
eigenvectors = eigenvectors*diag(sqrt(1./mr));

mr = diag(eigenvectors.'*M*eigenvectors);
kr = mr.*lbd;

cr = diag(eye(length(lbd))*obj.damping*sqrt(diag(lbd))*diag(mr));
nMode = find(eigenmodes<maxFreq, 1, 'last');

obj.eigen_frequencies = eigenmodes(1:nMode);
obj.eigen_vectors = eigenvectors(:,1:nMode);
obj.modal_mass = mr(1:nMode);
obj.modal_stiffness = kr(1:nMode);
obj.modal_damping = cr(1:nMode);

end

