function readh5file(obj, h5file)
% read SORTIES_MODES_TABLE Files


if exist(h5file,'file')
    mn = h5read(h5file,'/masses_modales');
    kn = h5read(h5file,'/raideurs_modales');
    P  = h5read(h5file,'/coefficients_deformees');
    Rn  = h5read(h5file,'/amortissements_modaux');
    basis_dim  = h5read(h5file,'/basis_dimension');
    sb_dim = h5read(h5file,'/soundboard_dimension');
else
    error('h5 file is not valid')
end

obj.modal_mass = mn(:);
obj.eigen_vectors = P;
obj.eigen_frequencies = sqrt(kn(:)./mn(:))/2/pi;
obj.modal_stiffness = kn(:);
obj.modal_damping = Rn(:);
obj.basis_dim = basis_dim;
obj.width = sb_dim(1);
obj.length = sb_dim(2);


end

