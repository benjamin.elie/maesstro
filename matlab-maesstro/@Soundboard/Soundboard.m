classdef Soundboard < handle
    % Soundboard is a class that allows simulations of piano soundboard dynamics
    
    properties
        
        % Geometry 
        x_normalized = [];
        y_normalized = [];
        x_extended = [];
        y_extended = [];
        origin = [];
        
        area = [];
        width = [];
        length = [];
        thickness = [];
        mesh = [];
        order = 0;   
        montjoiemesh = [];
                
        ribs = [];
        bridge = []; 
        
        % Mechanical properties
        material = [];
        mass_density = [];  
        young_modulus = [];
        poisson_coeff = [];
        shear_coeff = [];
        damping = [];    
        mass = [];
        rigidity = [];        
        extended_matrix = 0;
        panel_stiffness_matrix = [];
        panel_mass_matrix = [];        
        orthotropy_angle = [];
        block_stiffness = 2.5e6;
        stiffness_matrix = [];
        mass_matrix = [];
        eigen_frequencies = [];
        eigen_vectors = [];
        modal_mass = [];
        modal_stiffness = [];
        modal_damping = [];        
        mobility = [];        
        
        %%% Other
        basis_dim = [];
        solver = [];
        
        
    end
    
    methods
        
        % Constructor method
        function obj = Soundboard(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        % read plate date stored in the .xls file specified by file
        readmaterials(obj, file)
        
        % To generate the mass and stiffness matrix of the plate
        [ M_plate, K_plate ] = computepanelmatrix( obj, xDim, yDim )   
        
        % Generate mesh file
        readmesh(obj, file)
        
        % Generate soundboard matrix
        extMatrix = computesystemmatrix(obj, xDim,yDim)
        
        %   generate the general matrix of the system
        [ massMatrix,stiffMatrix ] = matrixassembly(obj)
        
        % compute the modal basis of the soundboard
        [eigenmodes,eigenvectors] = modalbasis(obj, maxFreq)
        
        convergencestudy( obj,fmax, red )
        
        % plot the mode shape
        plotmode(obj, Nmode, isSubplot, isRibs)
        
        extendedplate( obj )     
        extendedplate_v2( obj )
        
        Y = computemobility( obj, excPoint, obsPoint, freqVec )
        
        msh = createmesh( obj, fmax, isRegular )    
        
        % write output normalized in a hdf5 file
        exportnormalizeddata( obj, outputFile )
        
        plotsoundboard(SB, typedata)
        
        soundboard2json(obj, filename)

        [ massMatrix,stiffMatrix ] = notracematrixassembly(obj, nbr_p, nbr_q)
        
        geometry(obj)
        
        meshformontjoie(obj, fmax, isplot)
        
        Y = simpleplatemobility( obj, pos, fvec )
        
        plotmeshmontjoie(obj)
        
        geometryformesh(obj, fmax, isOrtho, lc, order)
        geometryformesh_v2(obj, fmax, isOrtho, N)
        
        initmontjoie(obj, centerFraq, nummodes, k_ord)
        
        montjoiemodalfrequency(obj, max_freq)
        montjoiemodalshape(obj)        
        
        readh5file(obj, h5file)
        
        modalDensity = computemodaldensity(obj, ord)
        
    end
    
end

