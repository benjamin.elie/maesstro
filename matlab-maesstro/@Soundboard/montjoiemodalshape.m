function montjoiemodalshape(obj)
% load modal shapes computed with montjoie to convert them into noralized
% data

fk = obj.eigen_frequencies;
nModes = length(fk);
lbd = (2*pi*fk).^2;

xDim = obj.basis_dim(1);
yDim = obj.basis_dim(2);

fileroot = './MontjoieOutput/SortieSoundboard2D';

disp('Recovering eigenvectors...')
tic
for k = 1:nModes
    file = [fileroot,num2str(k-1,'%.4d'),'_U2.dat'];
    [X,Y,~,~,V] = loadND(file);
    if k == 1
        yNode = size(Y,1);
        xNode = size(X,2);

        Xmin = min(X(:));
        Ymin = min(Y(:));
        Xmax = max(X(:));
        Ymax = max(Y(:));

        L = Ymax-Ymin;
        l = Xmax-Xmin;
        Vvec = zeros((yNode)*(xNode),nModes); 
        M = zeros((yNode)*(xNode),xDim*yDim); 
        cpt = 1;
        for q=1:yDim
            sinql = sin(q*pi.*(Y(:)-Ymin)/L);
            for p=1:xDim    
                M(:,cpt) = sin(p*pi.*(X(:)-Xmin)/l).*sinql(:);
                cpt = cpt + 1;
            end
        end        
    end

    Vvec(:,k) =  V(:);
end

Umat = M\Vvec;
toc
disp(['Eigenvectors recovered in ',num2str(toc), 'seconds'])

cr = diag(eye(nModes)*obj.damping*sqrt(diag(lbd))*eye(nModes));

obj.eigen_vectors = Umat;
obj.modal_mass = ones(nModes,1);
obj.modal_damping = cr;
obj.modal_stiffness = lbd;
obj.length = L;
obj.width = l;

end


