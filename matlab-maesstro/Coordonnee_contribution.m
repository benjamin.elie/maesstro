function [X,Y,coorx,coory] = Coordonnee_contribution(morceau_x,morceau_y,Lx,Ly)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

noeudx=morceau_x;
noeudy=morceau_y;

xv = (0:Lx/noeudx:Lx)-Lx/2;
yv = (0:Ly/noeudy:Ly)-Ly/2;

[ coorx, coory ] = meshgrid(xv, yv);

X = zeros(noeudy,noeudx);
Y = zeros(noeudy,noeudx);

for xxx = 1:morceau_x    
    X(:,xxx) = mean([coorx(1,xxx) coorx(1,xxx+1)]);
end

for yyy = 1:morceau_y    
    Y(yyy,:) = mean([coory(yyy,1) coory(yyy+1,1)]);
end

end

