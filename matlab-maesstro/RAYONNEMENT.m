%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fonction RAYONNEMENT : Calcul de la pression acoustique rayonnee par la
% table d'harmonie en un point
% Module inspiré de MODULE_TEMPO.m développé par Benjamin Trévisan
% Benjamin Cotte - version du 15/05/2018
%  
% fichiers en entree :
% - un fichier HDF5 fichier_modes avec les données relatives aux N modes
% calculés et conservés dans la bande de fréquences utile: masses mn,
% raideurs kn, amortissements Rn et déformées modales P stockées sous la
% forme des coefficients dans la base des modes de la plaque étendue avec
% appui simple (fonctions sinus) 
% - un fichier HDF5 fichier_mouvement avec les coordonnées modales qn(ti)
% pour Nm modes et les Ni iterations temporelles
% - un fichier .xls fichier_param avec les paramètres de calcul
%
% fichiers en sortie :
% - fichier HDF5 SORTIES_RAYONNEMENT.h5 avec les données normalisées dans
% le répertoire /pression: pression acoustique au récepteur pour les Nt itérations temporelles (vecteur de Nt réels)
% - fichier synthese_sonore.wav : fichier audio de la pression normalisée
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function RAYONNEMENT(fichier_param, fichier_modes, fichier_mouvement)

tic
disp('Chargement des fichiers : en cours...');
disp('Calcul de la pression rayonnee');

%%%%% Read parameters file
prm = jsondecode(fileread(fichier_param));
c0 = prm.sound_velocity;
rho0 = prm.air_density;
X0 = prm.x_ac;
Y0 = prm.y_ac;
Z0 = prm.z_ac;
max_freq = prm.max_freq;
clear prm

% 2) charger les parametres du calcul modal
if exist(fichier_modes,'file')
    mn = h5read(fichier_modes,'/masses_modales');
    kn = h5read(fichier_modes,'/raideurs_modales');
    P  = h5read(fichier_modes,'/coefficients_deformees');
    basis_dim  = h5read(fichier_modes,'/basis_dimension');
    sb_dim = h5read(fichier_modes,'/soundboard_dimension');
else
    error('le fichier HDF5 généré par MODES_TABLE est manquant')
end

nbr_p = basis_dim(1); 
nbr_q = basis_dim(2);
l_px = sb_dim(1);
l_py = sb_dim(2);

disp(['plaque etendue : ',num2str(l_px),'m x ',num2str(l_py),'m'])

disp(['Recepteur en x = ',num2str(X0),'m - y =  ',num2str(Y0),'m - z = ',num2str(Z0),'m'])

modemax = length(mn);
val_propres_ordo = kn./mn;
clear kn mn basis_dim sb_dim
frequences_propres = sqrt(val_propres_ordo)/(2*pi);

disp([num2str(modemax),' modes calcules entre ',num2str(frequences_propres(1)),'Hz et ',num2str(frequences_propres(end)),'Hz'])

% 3) charger les coordonnes modales
if exist(fichier_mouvement,'file')
    tps = h5read(fichier_mouvement,'/temps');
    bi = h5read(fichier_mouvement,'/coordonnees_modales');
else
    error('le fichier HDF5 généré par MOUVEMENT_TABLE est manquant')
end

dt = median(diff(tps)); % pas de temps du calcul de corde
fso = round(1/dt); % frequence d'echantillonnage associee a dt

% fso = round(2.1*max_freq);
% 
% bi = resample(bi.', fso, Fs, 30).';
% tps = (0:size(bi,2)-1)/fso;
npts = length(tps);
Fs = fso;
dt = 1/Fs;
clear tps

disp('Chargement des fichiers : termine.');
disp('Calcul de la pression rayonnee');

% 1) Calcul des accelerations modales
disp('Calcul des accelerations modales :  en cours...');
disp('Calcul de la pression rayonnee');

bi_2 = zeros(size(bi,1),size(bi,2)-1);

bi_2(:,1) = (bi(:,2)-2*bi(:,1))/dt^2;
bi_2(:,2:npts-1) = (bi(:,3:npts)-2*bi(:,2:npts-1)+bi(:,1:npts-2))/dt^2;
bi_2 = single(bi_2);
clear bi

disp('Calcul des accelerations modales :  termine.');
disp('Calcul de la pression rayonnee : initialisation') ;

% discretisation spatiale adaptee au champ lointain
morceau_x = round(l_px/(c0/4/frequences_propres(modemax)));
morceau_y = round(l_py/(c0/4/frequences_propres(modemax)));
clear frequences_propres

%%%% calcul des coordonnees des X et Y sur la table maillee %%%%%%%%%%%%%%%
[X,Y] = Coordonnee_contribution(morceau_x,morceau_y,l_px,l_py);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% calcul de la base modale (BM) correspondante %%%%%%%%%%%%%%%%%%%%%%%%%%
BM = zeros([size(X),nbr_p*nbr_q]);
cpt = 1;

for q = 1:nbr_q
    sinql = sin(q*pi.*(Y+l_py/2)/l_py);
    for p = 1:nbr_p   
        BM(:,:,cpt) = sin(p*pi.*(X+l_px/2)/l_px).*sinql;
        cpt = cpt + 1;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
aprod = single(P*bi_2(:,1:npts-1));
BM = single(reshape(BM,size(BM,1)*size(BM,2),[]));
clear P bi_2
acc = BM*aprod;
clear aprod BM

r = sqrt( (X-X0).^2 + (Y-Y0).^2 + Z0.^2);
clear X X0 Y Y0 Z0
r = r(:);

%%%%% calcul du retard des ondes provenant de chaque contribution
indice_temps = round(r/c0/dt);
nidx = length(indice_temps);

surf_rect = l_px/morceau_x*l_py/morceau_y;  % surface de chaque rectangle d'int�gration

cst = zeros(size(r));
for k = 1:nidx
    cst(k) = rho0*1/2/pi/r(k)*surf_rect;
end
clear r surf_rect

enumi = 0;
PMo = zeros(1,npts-1);
disp('Le calcul de la pression rayonnee a commence')   
%%%%%%%%%%%%%%%%% coeur du calcul
for ttt = 1:npts-1
    if ttt/(npts-1)*100 >= enumi
        disp(['Computation: ',num2str(enumi),'%'])
        enumi = enumi+10;
    end
    numDel = ttt-indice_temps;    
    if any(numDel >= 1)     
        idxneg = find(numDel < 1);
        numDel(idxneg) = 1;
        boolvec = ones(length(numDel),1);
        boolvec(idxneg) = 0;
        idx = sub2ind(size(acc),1:size(acc,1),numDel.');
        tmp = cst.*acc(idx(:)).*boolvec;
        PMo(ttt) = PMo(ttt)-sum(tmp);
    end
end
clear acc
cputime_Rayleigh = toc;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

disp('Calcul de la pression rayonnee : termine.');
audiowrite('synthese_sonore.wav',PMo/max(abs(PMo))*0.99,Fs)

if exist('SORTIES_RAYONNEMENT.h5','file')
    warning('existing file SORTIES_RAYONNEMENT.h5 deleted')
    delete('SORTIES_RAYONNEMENT.h5')
end

tps = (0:length(PMo)-1)/fso;
h5create('SORTIES_RAYONNEMENT.h5','/temps',size(tps))
h5create('SORTIES_RAYONNEMENT.h5','/pression',size(PMo))
h5write('SORTIES_RAYONNEMENT.h5', '/temps', tps)
h5write('SORTIES_RAYONNEMENT.h5', '/pression', PMo)

disp(['Module rayonnement fini sans erreur en ',num2str(cputime_Rayleigh,'%.2f'),' secondes'])

