function [ SB, Rcell, Bcell ] = readjson( fname )
% read json files of normalized data

id = jsondecode(fileread(fname));
sb = id.panel;
rb = id.ribs;
brg = id.bridges;
clear id

xsb = arrayfun(@(x)x.x, sb.contour)/1000; % sb.contour(1).x ];
ysb = arrayfun(@(x)x.y, sb.contour)/1000; %sb.contour(1).y ];

if (xsb(1) ~= xsb(end)) || ( ysb(1) ~= ysb(end) ) %%% Non-closed contour
    xsb = [ xsb; xsb(1) ];
    ysb = [ ysb; ysb(1) ];
end

thck = arrayfun(@(x)x.thickness, sb.contour)/1000;
meanThickness = mean(thck);

SB = Soundboard('x_normalized',xsb,'y_normalized',ysb,'thickness',thck, ...
    'material',sb.materialId,'orthotropy_angle',sb.orthotropicAngleDeg);
SB.area = polyarea(xsb, ysb);

Rcell = cell(length(rb),1);
or_angle = zeros(length(rb),1);

krib = 1;

for k = 1:length(rb)    
    xvec = [ rb(k).start.x; rb(k).end.x ]/1000;
    yvec = [ rb(k).start.y; rb(k).end.y ]/1000;    
    thrib = [ rb(k).start.height, rb(k).end.height ]/1000;
    wrib = [ rb(k).start.width, rb(k).end.width ]/1000;
    or_angle(k) = atand(diff(yvec)/diff(xvec));
        
    if isfield(rb(k),'intermediatePoints')
        posint = arrayfun(@(x)x.position, rb(k).intermediatePoints);
        xint = xvec(1) + posint*abs(diff(xvec));
        wint = arrayfun(@(x)x.width, rb(k).intermediatePoints)/1000;
        thint = arrayfun(@(x)x.height, rb(k).intermediatePoints)/1000;


        P = polyfit(xvec,yvec,1);
        yint = polyval(P,xint);
        xvecribs = [ xvec(1); xint(:); xvec(end) ];
        yvecribs = [ yvec(1); yint(:); yvec(end) ];
        wribtmp = [ wrib(1); wint(:); wrib(end) ];
        thribtmp = [ thrib(1); thint(:); thrib(end) ];

        [ xvec, isx ] = sort(xvecribs, 'ascend');
        yvec = yvecribs(isx);
        wrib = wribtmp(isx);
        thrib = thribtmp(isx);
    end
    
    for k1 = 1:length(xvec)-1 
        Rcell{krib} = Ribs('x_normalized',xvec(k1:k1+1),'y_normalized',yvec(k1:k1+1),'obj_angle',or_angle(k),...
            'number',1,'thickness', thrib(k1:k1+1),'width', ...
            wrib(k1:k1+1), 'deport',meanThickness/2,'material',rb(k).materialId);
        krib = krib + 1;
    end
end

Bcell = cell(length(brg),1);
or_angle = zeros(length(brg),1);

for k = 1:length(brg)   
    xsb = arrayfun(@(x)x.x, brg(k).medianLine)/1000;
    ysb = arrayfun(@(x)x.y, brg(k).medianLine)/1000;
    or_angle(k) = atand((ysb(end)-ysb(1))/(xsb(end)-xsb(1)));
    wbr = arrayfun(@(x)x.width, brg(k).medianLine)/1000;
    thbr = arrayfun(@(x)x.height, brg(k).medianLine)/1000;
    Bcell{k} = Bridge('x_normalized',xsb,'y_normalized',ysb,'obj_angle',or_angle(k),...
        'number',length(xsb)-1,'deport',meanThickness/2,'width',wbr,...
        'thickness',thbr,'material',brg(k).materialId);
end

SB.ribs = Rcell;
SB.bridge = Bcell;

end

