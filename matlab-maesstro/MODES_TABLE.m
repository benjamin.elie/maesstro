function SB = MODES_TABLE( jsonfile, param_file, mtrfile )

%%%%% Initialization
%%% Path and files

jsonfile2 = [jsonfile(1:end-5),'_simple.json'];

%%%%% Read parameters file
prm = jsondecode(fileread(param_file));
max_freq = prm.max_freq;
modalsolver = prm.solver;
nbr_p = prm.n_xsin;
nbr_q = prm.n_ysin;
k_block = prm.spring_stiffness;

SB = readjson( jsonfile );
SB.solver = lower(modalsolver);
SB.block_stiffness = k_block;
SB.basis_dim = [ nbr_p, nbr_q ];

tictot = tic;

switch lower(modalsolver)
    case 'lva'
        disp('Computation of the extended plate matrix started')
        tic;
        SB.extendedplate;
        
        SB.readmaterials(mtrfile);
        SB.createmesh(max_freq, true);
        SB.computepanelmatrix(nbr_p, nbr_q);
        if SB.order
            SB.computesystemmatrix(nbr_p, nbr_q);
        end
        disp('Computation of the extended plate matrix is done')    
        be = toc;
        disp(['Elapsed time to compute the extended plate matrix: ',num2str(be),' seconds'])    

        SB.notracematrixassembly(nbr_p, nbr_q);

        disp('Computation of the modal basis started')
        SB.modalbasis(max_freq);

        disp('Computation of the modal basis is done')
        be = toc;
        SB.soundboard2json(jsonfile2);    
        disp(['Elapsed time to compute the modal basis: ',num2str(be),' seconds'])   
    
    case 'montjoie'

        tic; 
        SB.geometry;
        
        SB.readmaterials(mtrfile);
        disp('Computation of the mesh for montjoie started')   
        
        %%% Convergence
        fktmp = 0;
        tic
        nerrtmp = inf;
        SB.geometryformesh(max_freq, false, 4);
        for k_ord = 1:3
            ordDisc = k_ord;
            
            SB.initmontjoie(max_freq-200, 5, k_ord);
            try
                SB.montjoiemodalfrequency(max_freq);
                fk = SB.eigen_frequencies;
                dfk = fk-fktmp;
                nerr = 100*mean(abs(dfk./fk));                
                fktmp = fk;
                if k_ord > 1
                    if nerr < 1e-4 || nerr > nerrtmp
                        ordDisc = k_ord-1;
                        break
                    end   
                    nerrtmp = nerr;
                end
            catch
                ordDisc = k_ord-1;
                break
            end
        end
        disp(['The discretization order is ',num2str(ordDisc)])
        toc
        SB.initmontjoie(10,1400);
        
        disp('Computation of the mesh is done')    
        be = toc;
        disp(['Elapsed time to compute the mesh: ',num2str(be),' seconds']) 
        SB.montjoiemodalfrequency(max_freq);
        SB.montjoiemodalshape
        
    otherwise
        error('Solver badly defined. Should be either lva or montjoie')        
end

SB.exportnormalizeddata;
betot = toc(tictot);

disp(['MODES TABLES returned without error. Total elapsed time to compute modes: ',num2str(betot),' seconds'])

end

