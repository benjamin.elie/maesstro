%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% fonction MOUVEMENT_TABLE : Calcul de la dynamique de la table d'harmonie 
% Module inspiré de MODULE_TEMPO.m développé par Benjamin Trévisan
% Benjamin Cotte - version du 15/05/2018
% 
% 
% fichiers en entree :
% - un fichier HDF5 fichier_modes avec les données relatives aux N modes
% calculés et conservés dans la bande de fréquences utile: masses mn,
% raideurs kn, amortissements Rn et déformées modales P stockées sous la
% forme des coefficients dans la base des modes de la plaque étendue avec
% appui simple (fonctions sinus) 
% - coordonnées des points de couplage le long du chevalet : fichier_couplage
% - fichier .txt de la force exercée par la corde au chevalet pour les Nt
% itérations temporelles (multiplicateur de Lagrange) : fichier_force
% - un fichier .xls fichier_param avec les paramètres de calcul
%
% fichier HDF5 SORTIES_MOUVEMENT_TABLE.h5 en sortie avec :
% - coordonnées modales qn(ti) pour Nm modes et les Ni iterations temporelles
% 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function MOUVEMENT_TABLE( fichier_modes, fichier_effort)

disp('Chargement des fichiers : en cours...');
tic

% 3) charger les parametres du calcul modal
if exist(fichier_modes,'file')
    mn = h5read(fichier_modes,'/masses_modales');
    kn = h5read(fichier_modes,'/raideurs_modales');
    Rn = h5read(fichier_modes,'/amortissements_modaux');
    P  = h5read(fichier_modes,'/coefficients_deformees');
    basis_dim  = h5read(fichier_modes,'/basis_dimension');
    sb_dim = h5read(fichier_modes,'/soundboard_dimension');
else
    error('le fichier HDF5 généré par MODES_TABLE est manquant')
end

nbr_p = basis_dim(1); 
nbr_q = basis_dim(2);
l_px = sb_dim(1);
l_py = sb_dim(2);

mn = mn(:);
kn = kn(:);

modemax = length(mn);
val_propres_ordo = kn./mn;
frequences_propres = sqrt(val_propres_ordo)/(2*pi);

disp([num2str(modemax),' modes calcules entre ',num2str(frequences_propres(1)),'Hz et ',num2str(frequences_propres(end)),'Hz'])
max_freq = 1000*ceil(frequences_propres(end)/1000);
fso = round(2.1*max_freq);

% 5) charger effort = multiplicateur de Lagrange LM a x=L dans le calcul Montjoie
if exist(fichier_effort,'file')
    idx = h5read(fichier_effort,'/note_index');
    nb_notes = length(idx);
    duration = h5read(fichier_effort,'/duration');
    for inote = 1:nb_notes
        data = h5read(fichier_effort,['/',num2str(inote)]);
        force_vert = data(2,:);
        tvec = data(1,:);
        clear data

        note(inote).force = force_vert;
        clear t_string force_vert
    end
    xe = h5read(fichier_effort,'/x_coupling');
    ye = h5read(fichier_effort,'/y_coupling');
    start_time = h5read(fichier_effort,'/start_time');
    dt = median(diff(tvec));
else
    error('Le fichier effort est manquant')
end

disp('Chargement des fichiers : termine.');
disp('Calcul du mouvement de la table');

% 6) calculer les amplitudes modales dans la base physique (table d'harmonie)
disp('Calcul des amplitudes modales dans la base physique : en cours...');

% matrices de masses et raideurs de la structure raidie tronquee dans la base raidie
Ms = diag(mn);
amort_plaque = Rn./mn./sqrt(val_propres_ordo); % amortissements modaux
cn = diag(sqrt(diag(val_propres_ordo))*Ms*diag(amort_plaque));
cn = cn(:);
clear Ms

Fs = round(1/dt);
 
F_gene = zeros(modemax,round(duration*Fs));
tps = (0:duration*Fs-1)*dt;
Phi_xe_ye = zeros(nbr_p*nbr_q, length(xe));
cpt = 1;

for q=1:nbr_q
    sinql = sin(q*pi.*ye/l_py);
    for p=1:nbr_p   
        Phi_xe_ye(cpt,:) = sin(p*pi.*xe/l_px).*sinql;
        cpt = cpt + 1;
    end
end

for inote = 1:nb_notes
    idx1 = floor(start_time(inote)*Fs)+1;
    idx2 = min(size(F_gene,2),idx1+length(note(inote).force)-1);
    idx = idx1:idx2;
    F_gene(:,idx1:idx2) = F_gene(:,idx1:idx2)+P'*Phi_xe_ye(:,inote)*note(inote).force(1:length(idx));
end

clear Phi_xe_ye

bi = zeros(modemax,length(tps)+1);
it = 1;
Mtmp = 1./(mn/(dt^2)+0.5*cn/dt);
bi(:,it+1) = F_gene(:,1).*Mtmp;

ntps = length(tps);
numiter = 0;

% instants superieurs ou egaux a it = 2 :
for it = 2:ntps %size(tps,2)
    if isnan(norm(bi(:,it)))
        warning('Divergence du calcul');      
        break
    else
        if it/ntps*100 > numiter
            disp(['Completion: ',num2str(numiter),'%'])
            numiter = numiter +10 ;
        end
        bi(:,it+1) = Mtmp.*(F_gene(:,it) -(kn-2*mn/(dt^2)).*bi(:,it) - (mn/(dt^2)-0.5*cn/dt).*bi(:,it-1));
    end
end

bi(isnan(bi)) = 0;

bi = resample(bi.', fso, Fs, 30).';
tps = (0:size(bi,2)-1)/fso;

cputime_amp_modales = toc;

disp('Calcul des amplitudes modales dans la base physique : termine.');

if exist('SORTIES_MOUVEMENT_TABLE.h5','file') == 2
    warning('existing file SORTIES_MOUVEMENT_TABLE.h5 deleted')
    delete('SORTIES_MOUVEMENT_TABLE.h5')
end

h5create('SORTIES_MOUVEMENT_TABLE.h5','/temps',size(tps))
h5create('SORTIES_MOUVEMENT_TABLE.h5','/coordonnees_modales',size(bi))
h5write('SORTIES_MOUVEMENT_TABLE.h5', '/temps', tps)
h5write('SORTIES_MOUVEMENT_TABLE.h5', '/coordonnees_modales', bi)

disp(['Module MOUVEMENT_TABLE termine sans erreur en ',num2str(cputime_amp_modales,'%.2f'),' secondes'])
