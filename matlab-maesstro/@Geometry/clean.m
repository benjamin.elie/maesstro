function clean(obj)
% clean points and index

obj.points = [];
obj.lines = [];
obj.physical_groups = [];
for k = 1:length(obj.elements)
    obj.elements{k}.point_index = [];
    obj.elements{k}.line_index = [];
end

end

