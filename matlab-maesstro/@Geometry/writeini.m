function writeini(obj, numModes, varargin)
%
if nargin < 2; numModes = 20; end

discOrder = obj.discretization_order;
centerFreq = obj.center_frequency;
typeSolver = obj.type_solver;
NLSolver = obj.nonlinear_solver;

%%%%%%%%%%%% header of .ini file
fidini = fopen('montjoie.ini','w+');
fprintf(fidini,'%s\n','TypeEquation = TIME_REISSNER_MINDLIN');

% %%%%%%%%%%%%%%%%%%%%%%% For string soundboard
% fprintf(fidini,'%s\n','TimeInterval = 0 0.0001'); 
% fprintf(fidini,'%s\n','TimeStep = 5e-5');     
% fprintf(fidini,'%s\n','DisplayFrequency = 1e-2'); 
% fprintf(fidini,'%s\n','String = 0.955 8.6590e-7 7850 759.4 2.02e+11 6.7528e-14 8.0e10 0.85 PHI_DAMPING 0.15 0.5 0.15 2.4e-9 1.0e-9 2.4e-9 1 50 1 2.481e9 4.570e5');
% fprintf(fidini,'%s\n','Hammer = YES 9.14e-3 0.079 1 2.4 0.02 2011 0.02 -0.02001  1e-10');
% fprintf(fidini,'%s\n','StringSolver = THETA_NL_QUARTER  0.25 0.25');                      
% fprintf(fidini,'%s\n','StringPermutations = PERMUT_ALL'); 
% fprintf(fidini,'%s\n','StringInitialData = NONE'); 
% fprintf(fidini,'%s\n','StringInterp = 20');                                               
% fprintf(fidini,'%s\n','StringInterpTimeGrid = 0 2.0 1.0e-2');                                               
% fprintf(fidini,'%s\n','StringSismo = 0.06');                                            
% fprintf(fidini,'%s\n','StringSismoTimeGrid = 0  2.0 5e-5');
% fprintf(fidini,'%s\n','FileOutputString = SismoLM_Soundboard.txt SismoHammerSoundboard.txt ForceMarteauSoundboard.txt EnergyNoteSoundboard.txt');  
% fprintf(fidini,'%s\n','FileOutputSismoString = SismoStringSoundboard_ SismoLastPointSoundboard_ SismoCrushSoundboard_ ValSoundboard_');  
% fprintf(fidini,'%s\n','Energy = 0 2.0 1e-4 100');   
% fprintf(fidini,'%s\n','Bridge = 2');
% fprintf(fidini,'%s\n','NewtonParam = 1e-9 10 1e-9');

fprintf(fidini,'%s\n','TypeElement = TRIANGLE_LOBATTO');
fprintf(fidini,'%s\n','');
fprintf(fidini,'%s\n','# Mesh files');
fprintf(fidini,'%s\n',['MeshPath = ',pwd,'/']);
fprintf(fidini,'%s\n','FileMesh = geofile.msh');
fprintf(fidini,'%s\n','');

numelem = length(obj.physical_groups);


for kelem = 1:numelem
    line2write = ['MateriauDielec = ',num2str(kelem), ...
        obj.elements{obj.physical_groups{kelem}(1)}.mechanical_properties ];
    fprintf(fidini,'%s\n', line2write);
end

% obj.lineboundary
% disp(obj.boundary_lines);
%%% .ini 
fprintf(fidini,'%s\n','');
fprintf(fidini,'%s\n', ['ConditionReference = ',num2str(1:length(obj.elements{1}.point_index)),' ',obj.boundary_conditions ]);
if strcmpi(obj.boundary_conditions,'supported')
    fprintf(fidini,'%s\n','SupportedComponents = 2');
end
fprintf(fidini,'%s\n','');
fprintf(fidini,'%s\n','#Element thickness');
for kelem = 1:numelem
    line2write = ['ThicknessPlate = VARIABLE ',num2str(kelem),' ',...
        num2str(obj.elements{obj.physical_groups{kelem}(1)}.thickness) ];
     fprintf(fidini,'%s\n', line2write);         
end
   
%%% .ini
fprintf(fidini,'%s\n','');
fprintf(fidini,'%s\n',['OrderDiscretization = ',num2str(discOrder)]);
fprintf(fidini,'%s\n','SaveEquivalentImpedance = Masses.txt Stiffnesses.txt Dissipations.txt Amplitudes.txt');
% fprintf(fidini,'%s\n','TypeMesh = QUAD' );
fprintf(fidini,'%s\n',['Eigenvalue = YES SHIFTED CENTERED ',num2str(centerFreq),' ',num2str(numModes)]);
if ~isempty(obj.eigenvalue_tolerance)
    fprintf(fidini,'%s\n',['EigenvalueTolerance = ',num2str(obj.eigenvalue_tolerance)]);
end

if ~isempty(obj.dircihlet_coef_eigenvalue)
    fprintf(fidini,'%s\n',['DirichletCoefEigenvalue = ',num2str(obj.dircihlet_coef_eigenvalue)]);
end
fprintf(fidini,'%s\n','SismoPlane = AUTO 131 131');
fprintf(fidini,'%s\n','FileOutputPlane = toto SortieSoundboard2D');
fprintf(fidini,'%s\n','DirectoryOutputString = ./MontjoieOutput/');
fprintf(fidini,'%s\n','OutputFormat = BINARY DOUBLE' );
fprintf(fidini,'%s\n','FileEigenvalue = ./MontjoieOutput/eigenval2D');
fprintf(fidini,'%s\n','ElectricOrMagnetic = -2');
fprintf(fidini,'%s\n',['TypeSolver = ',typeSolver]);
fprintf(fidini,'%s\n',['NonLinearSolver = ',NLSolver,' 1e-15 50']);
fprintf(fidini,'%s\n','PrintLevel = -1');
fprintf(fidini,'%s\n','DataNonRegressionTest = soundboard.x 20 1');


end

