classdef Geometry < handle
    % GEOMETRY is a class used to generate mesh files to be used by
    % Montjoie
    
    properties
        elements = [];
        characteristic_length = [];
        points = [];
        lines = [];
        physical_groups = [];
        boundary_lines = [];
        
        % FOR MONTJOIE SOLVER
        boundary_conditions = 'DIRICHLET'
        discretization_order = 3;
        center_frequency = 10;
        type_solver = 'AUTO';
        nonlinear_solver = 'NEWTON';
        dircihlet_coef_eigenvalue = 1e12;
        eigenvalue_tolerance = 1e-6;

    end
    
    methods
        % Constructor method
        function obj = Geometry(varargin)
            for k = 1:2:length(varargin)
                field = varargin{k};
                argvalue = varargin{k+1};
                if isprop(obj,field)
                    obj.(field) = argvalue;
                end % if isprop
            end % for k ...
        end % end function
        
        % write .msh file from geometry object
        writemeshfile(obj)
        
        % write .ini file for montjoie
        writeini(obj, NumModes)

        % rearrange lines to avoid intersections
        rearrangelines(obj, tol)
        
        % rearrange physical groups
        rearrangegroups(obj)
        
        numLines = lineboundary(obj)
        
        % clean points and index
        clean(obj)
        
    end
    
    
end

