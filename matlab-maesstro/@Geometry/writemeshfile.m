function writemeshfile(obj)
% write .msh file from geometry object

lc = obj.characteristic_length;
numelem = length(obj.elements);
npt = size(obj.points,1);
nlines = length(obj.lines);

fid = fopen('geofile.geo','w+');

%%%%%%%%%%%% header of .geo file
fprintf(fid,'%s\n','// Soundboard mesh');
fprintf(fid,'%s\n','');
% fprintf(fid,'%s\n','Geometry.AutoCoherence = 0; // Should all duplicate entities be automatically removed?');
fprintf(fid,'%s\n','');
fprintf(fid,'%s\n','// Generate points');

for kpt = 1:npt
    xtmp = obj.points(kpt,1);
    ytmp = obj.points(kpt,2);   

    line2write = ['Point(',num2str(kpt),') = {',num2str(xtmp),',',num2str(ytmp),',0,',num2str(lc),'};'];    
    fprintf(fid,'%s\n',line2write);

end
fprintf(fid,'%s\n','');

fprintf(fid,'%s\n','// Generate lines');

for kline = 1:nlines
    kx = num2str(obj.lines(kline,1));
    ky = num2str(obj.lines(kline,2));   
    line2write = ['Line(',num2str(kline),') = {',kx,',',ky,'};'];
    fprintf(fid,'%s\n',line2write); 
end
fprintf(fid,'%s\n','');

fprintf(fid,'%s\n','// Generate Physical lines');

for kline = 1:nlines
    line2write = ['Physical Line(',num2str(kline),') = {',num2str(kline),'};'];
    fprintf(fid,'%s\n',line2write); 
end
fprintf(fid,'%s\n','');

fprintf(fid,'%s\n','// Generate contours');

cellpanel = 'Plane Surface(1) = {';
for kelem = 1:numelem
    idxLine = obj.elements{kelem}.line_index;
    cellwrite = ['Line Loop(',num2str(kelem),') = {'];
    for kidx = 1:length(idxLine)
        celltmp = [num2str(idxLine(kidx)),','];
        cellwrite = [cellwrite,celltmp];
    end
    cellwrite = [cellwrite(1:end-1),'};'];
    fprintf(fid,'%s\n',cellwrite);
        
    celltmp = [ num2str(kelem),',' ];
    cellpanel = [ cellpanel, celltmp ];
    
end

fprintf(fid,'%s\n','');
fprintf(fid,'%s\n','// Generate surfaces');
cellpanel = [cellpanel(1:end-1),'};'];
% fprintf(fid, '%s\n', 'Plane Surface(1) = {1};')
fprintf(fid,'%s\n',cellpanel);

for kelem = 2:numelem    
    fprintf(fid,'%s\n',['Plane Surface(',num2str(kelem),') = {',num2str(kelem),'};']);
end

for ksurf = 1:length(obj.physical_groups)
    cellphys = ['Physical Surface(',num2str(ksurf),') = {'];
    for k1 = 1:length(obj.physical_groups{ksurf})
        cellphys = [ cellphys,num2str(obj.physical_groups{ksurf}(k1)),',' ];
    end
    cellphys = [cellphys(1:end-1),'};'];

    fprintf(fid,'%s\n',cellphys);
end

% fprintf(fid,'%s\n','Recombine;');
fprintf(fid,'%s\n','Coherence;');



