function rearrangegroups(obj)
% Rearrange physical groups

ngroup = 1;
elem2remove = [];
for k1 = 1:length(obj.elements)
    ref = obj.elements{k1};
    if ~any(elem2remove == k1)
        obj.physical_groups{ngroup} = k1;
        for k2 = k1:length(obj.elements)
            if k1~=k2 && ~any(elem2remove == k2)
                comp = obj.elements{k2};
                if (strcmp(comp.mechanical_properties,ref.mechanical_properties)) ...
                        && (comp.thickness == ref.thickness)
                    obj.physical_groups{ngroup} = [ obj.physical_groups{ngroup}, k2 ];
                    elem2remove = [ elem2remove, k2 ];
                end
            end
        end
        ngroup = ngroup+1;
    else
        
    end
 
end

% obj.physical_groups{1}(obj.physical_groups{1} == 1) = [];

