function rearrangelines(obj, tol)
% rearrange lines to avoid intersections


if nargin < 2; tol = 1e-4; end 

% clean points and index
obj.clean;

kpt = 1;
kline = 1;

for kelem = 1:length(obj.elements)

    xtmp = obj.elements{kelem}.x_coordinates;
    ytmp = obj.elements{kelem}.y_coordinates;
    
    for k = 1:length(xtmp)
        pttmp = [ xtmp(k),  ytmp(k) ];
        isM = false;
        if ~isempty(obj.points)
            [ isM, c ] = ismembertol(pttmp, obj.points(:,1:2),tol,'ByRows',true);            
            c = c(1);            
        end
        if ~all(isM)
            obj.points = [ obj.points ; pttmp, kelem];
            obj.elements{kelem}.point_index = ...
                [ obj.elements{kelem}.point_index ; kpt ];
            kpt = kpt + 1;       
        else
            obj.elements{kelem}.point_index = ...
                [ obj.elements{kelem}.point_index ; c ];
        end
    end

    for k = 1:length(obj.elements{kelem}.point_index)
        if k < length(obj.elements{kelem}.point_index)
            linetmp = [ obj.elements{kelem}.point_index(k), obj.elements{kelem}.point_index(k+1) ];
        else
            linetmp = [ obj.elements{kelem}.point_index(k), obj.elements{kelem}.point_index(1) ];
        end
        c = [];
        if ~isempty(obj.lines)
            [ ~, ~, c ] = intersect(sort(linetmp), sort(obj.lines(:,1:2),2),'rows');            
        end
        if isempty(c)
            obj.lines = [ obj.lines; ...
                linetmp, kelem ];
            obj.elements{kelem}.line_index = [ obj.elements{kelem}.line_index; kline ];
            kline = kline + 1;
        else
            if obj.lines(c,1:2) ~= linetmp
                c = -c;
            end
                
            obj.elements{kelem}.line_index = ...
                [ obj.elements{kelem}.line_index ; c ];
        end     
    end
end

end

