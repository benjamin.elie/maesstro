function numLines = lineboundary(obj)
% Find the lines that are at the boundaries of the soundboard

fullLine = obj.elements{1}.line_index;

xsb = obj.elements{1}.x_coordinates;
ysb = obj.elements{1}.y_coordinates;
sbpoly = polyshape(xsb, ysb);
[ xsb, ysb ] = sbpoly.boundary;

numLines = fullLine(:);

Lines = obj.lines(:,1:2);
Pts = obj.points(:,1:2);
nLines = size(Lines,1);
load all
close all
figure;
plot(xsb, ysb);
hold on;
save all
for k = 1:nLines
    if ~any(fullLine == k)        
        xtmp = Pts(Lines(k,:),1);
        ytmp = Pts(Lines(k,:),2);        
        [xi,yi] = polyxpoly(xtmp,ytmp, xsb, ysb, 'unique');
        
        plot(xtmp, ytmp, 'r','linewidth',2)
        pause
        
        if ~isempty(xi) && length(xi) == 2
            disp('2 points')
            if xi == xtmp && yi == ytmp
                disp('Yeah, good')
                numLines = [ numLines; k ];
            end        
        end
    end
end
    
obj.boundary_lines = numLines.';

end

