# Audio examples synthesized by MAESSTRO

## Presentation

You will find here examples of piano tones or piano pieces synthesized by MAESSTRO. RP, MP1, MP2, MP3 correspond to 4 different virtual pianos. They consist in a reference piano (RP), derived from a real piano, which is subject to several modifications to evaluate their mechanical and acoustical impacts. The chosen modifications include the increase of the panel thickness (MP1), the removal of half the ribs (MP2), and the removal of all the ribs (MP3). These modifications correspond to change in stiffness, namely a global increase for MP1, a local decrease for MP2, and both a global and local stiffness decreases for MP3.

The audio samples correspond to:
* `RP_C3_28.wav`: note C3 from the virtual reference piano RP
* `MP1_C3_28.wav`: note C3 from the virtual modified piano MP1
* `MP2_C3_28.wav`: note C3 from the virtual modified piano MP2
* `MP3_C3_28.wav`: note C3 from the virtual modified piano MP3
* `RP_Hammerklavier_3rd.wav`: beginning of the 3rd movement of Beethoven's piano sonata No. 29 op.106 (Hammerklavier)



