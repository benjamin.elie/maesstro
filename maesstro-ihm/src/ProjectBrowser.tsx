import {List} from "immutable";
import * as React from "react";

import {BridgeNode} from "./BridgeNode";
import {ImageNode} from "./ImageNode";
import {PanelNode} from "./PanelNode";
import {Project} from "./Project";
import {ProjectNode} from "./ProjectNode";
import {RibNode} from "./RibNode";

interface IdNodePair {
    id: number;
    node: ProjectNode;
}

interface Categories {
    images: IdNodePair[];
    panels: IdNodePair[];
    bridges: IdNodePair[];
    ribs: IdNodePair[];
}

function groupNodesByCategory(nodes: List<ProjectNode>): Categories {
    const categories: Categories = {
        images: [],
        panels: [],
        bridges: [],
        ribs: [],
    };

    nodes.map((node, id) => {
        const idAndNode = {id, node};
        if (node instanceof ImageNode) {
            categories.images.push(idAndNode);
        } else if (node instanceof PanelNode) {
            categories.panels.push(idAndNode);
        } else if (node instanceof BridgeNode) {
            categories.bridges.push(idAndNode);
        } else if (node instanceof RibNode) {
            categories.ribs.push(idAndNode);
        }
    });
    return categories;
}

interface NodeFolderProps extends Object {
    name: string;
    addTitle: string;
    nodes: Array<{id: number, node: ProjectNode}>;
    factory: () => Promise<ProjectNode>;
    project: Project;
    onProjectUpdated: (project: Project) => void;
}

class NodeFolder extends React.Component<NodeFolderProps, {folded: boolean}> {
    constructor(props: NodeFolderProps) {
        super(props);
        this.state = {
            folded: false,
        };
    }

    public render() {
        const {name, nodes, addTitle} = this.props;
        const {folded} = this.state;
        const nodeClass = " folder-node";
        const headerClass = " folder-header";
        const caret = (
            this.state.folded
            ? <i className="folder-header__caret fas fa-caret-right "></i>
            : <i className="folder-header__caret fas fa-caret-down "></i>
        );
        return (
            <div>
                <div
                    className={headerClass +
                        (folded ? headerClass + "--folded" : "")}
                    onClick={this.toggleFold}>
                    {caret}
                    <div
                        className="folder-header__title">{name} - {nodes.length}</div>
                    <button
                        title={addTitle}
                        className="btn btn-sm btn-primary add-button"
                        onClick={this.handleAddClicked} >
                        Add
                    </button>
                </div>
                {!this.state.folded &&
                    <div className="folder-body">
                        {nodes.map(({id, node}) => (
                            <div
                                className={nodeClass +
                                    (node.selected ? nodeClass + "--selected" : "")
                                }
                                key={id}
                                onClick={() => this.handleNodeClicked({id, node})}>
                            <div className="folder-node__title">{node.name}</div>
                            {node.selected &&
                                <button
                                    className="delete"
                                    onClick={this.handleDeleteClicked}>
                                     <span aria-hidden="true">&times;</span>
                                </button>
                            }
                            </div>
                        ))}
                    </div>
                }
            </div>
        );
    }

    private handleDeleteClicked = (event: React.MouseEvent) => {
        const {project, onProjectUpdated} = this.props;
        event.stopPropagation();
        onProjectUpdated(project.deleteSelectedNode());
    }

    private handleNodeClicked = ({id, node}: IdNodePair)  => {
        const {project,  onProjectUpdated} = this.props;
        onProjectUpdated(project.selectNode(node.selected ? null : id));
    }

    private handleAddClicked = (event: React.MouseEvent) => {
        const {factory,  project, onProjectUpdated} = this.props;
        event.stopPropagation();
        factory().then((node) => onProjectUpdated(project.addNode(node)));
    }

    private toggleFold = () => {
        this.setState({folded: !this.state.folded});
    }
}

interface ProjectBrowserProps {
    project: Project;
    onProjectUpdated: (project: Project) => void;
    onImportImage: () => Promise<HTMLImageElement>;
}

function ProjectBrowser({project,   onProjectUpdated, onImportImage}: ProjectBrowserProps) {
    const {nodes} = project;
    const categories = groupNodesByCategory(nodes);
    return (
        <div className="project_browser">
            <div className="project_browser__nodes">
                <NodeFolder
                    name="Images"
                    addTitle="Import Image"
                    project={project}
                    onProjectUpdated={onProjectUpdated}
                    factory={() => onImportImage().then((image) => new ImageNode({image}))}
                    nodes={categories.images} />
                <NodeFolder
                    name="Panels"
                    addTitle="Add Panel"
                    project={project}
                    onProjectUpdated={onProjectUpdated}
                    factory={() => Promise.resolve(new PanelNode())}
                    nodes={categories.panels} />
                <NodeFolder
                    name="Bridges"
                    addTitle="Add Bridge"
                    project={project}
                    onProjectUpdated={onProjectUpdated}
                    factory={() => Promise.resolve(new BridgeNode())}
                    nodes={categories.bridges} />
                <NodeFolder
                    name="Ribs"
                    addTitle="Add Rib"
                    project={project}
                    onProjectUpdated={onProjectUpdated}
                    factory={() =>  Promise.resolve(new RibNode())}
                    nodes={categories.ribs} />
            </div>
        </div>
    );
}

export {ProjectBrowser};
