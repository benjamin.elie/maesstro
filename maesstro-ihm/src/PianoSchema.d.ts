import {JSONSchema6Definition} from "json-schema";

declare module "PianoSchema.json" {
    const value: JSONSchema6Definition;
    export default value;
}
