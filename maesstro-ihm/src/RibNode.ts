import {Record} from "immutable";

import {ProjectNode} from "./ProjectNode";
import {Rib} from "./Rib";
import {Transform} from "./Transform";

interface RibNodeProps {
    rib: Rib;
    transform: Transform;
    name: string;
    selected: boolean;
}

const defaultRibNodeProps: RibNodeProps = {
    rib: new Rib(),
    transform: new Transform(),
    name: "Rib",
    selected: false,
};

class RibNode extends Record<RibNodeProps>(defaultRibNodeProps) implements ProjectNode {

    get type() {
        return "RIB_NODE";
    }
}

export {RibNode};
