import * as React from "react";

import {Field} from "formik";
import {MaterialRegistry} from "./MaterialRegistry";

interface MaterialSelectFieldProps {
    name: string;
    materialRegistry: MaterialRegistry;
}

function MaterialOption({identifier, materialRegistry}: { identifier: string, materialRegistry: MaterialRegistry }) {
    return (
        <option
            value={identifier}
        >
            {identifier}
        </option>
    );
}

export class MaterialSelectField extends React.Component<MaterialSelectFieldProps> {
    public render() {
        const {name, materialRegistry} = this.props;
        const materialsIds = materialRegistry.getAllMaterialsIds();
        return (
            <Field className="form-control" component="select" name={name}>
                {materialsIds.map( (identifier) =>
                    <MaterialOption
                        key={identifier}
                        identifier={identifier}
                        materialRegistry={materialRegistry}
                    />,
                )}
            </Field>
        );
    }
}
