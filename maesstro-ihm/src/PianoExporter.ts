import {List} from "immutable";

import {BridgeNode} from "./BridgeNode";
import {PanelNode} from "./PanelNode";
import {Piano} from "./Piano";
import {Project} from "./Project";
import {ProjectNode} from "./ProjectNode";
import {RibNode} from "./RibNode";

class PianoExporter {

    public async export(project: Project): Promise<Blob> {
        const jsonPiano = this.pianoFromProjectNodes(project.nodes).toJSON();
        return this.jsonToBlob(jsonPiano);
    }

    private jsonToBlob(json: any) {
        return new Blob(
            [JSON.stringify(json, null, 2)],
            {type: "application/json"});
    }

    private pianoFromProjectNodes(projectNodes: List<ProjectNode>) {
        return new Piano().withMutations((piano) => {
            projectNodes.forEach((node) => {
                if (node.type === "PANEL_NODE") {
                    piano.set("panel", (node as PanelNode).panel);
                } else if (node.type === "BRIDGE_NODE") {
                    piano.set("bridges", piano.bridges.push((node as BridgeNode).bridge));
                } else if (node.type === "RIB_NODE") {
                    piano.set("ribs", piano.ribs.push((node as RibNode).rib));
                }
            });
        });
    }
}

export {PianoExporter};
