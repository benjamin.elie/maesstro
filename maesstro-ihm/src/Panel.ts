import {List, Record} from "immutable";
import {ThickPoint} from "./Point";

interface PanelProps {
    contour: List<ThickPoint>;
    orthotropicAngleDeg: number;
    materialId: string;
}

const PanelDefaultProps = {
    contour: List<ThickPoint>(),
    orthotropicAngleDeg: 0,
    materialId: "Norway Spruce 1",
};

class Panel extends Record<PanelProps>(PanelDefaultProps) {}

function fromJS(obj: any): Panel {
    const panel = new Panel().asMutable();
    if (obj.contour) {
        panel.set("contour", List(obj.contour));
    }
    if (obj.orthotropicAngleDeg != null) {
        panel.set("orthotropicAngleDeg", obj.orthotropicAngleDeg);
    }
    if (obj.materialId != null) {
        panel.set("materialId", obj.materialId);
    }
    return panel.asImmutable();
}

export {Panel, fromJS};
