import {Record} from "immutable";

import {Point} from "./Point";
import {ProjectNode} from "./ProjectNode";
import {Transform} from "./Transform";

const DEFAULT_IMAGE = document.createElement("img");

interface ImageNodeProps {
    image: HTMLImageElement;
    name: string;
    selected: boolean;
    origin: Point;
    ref1: Point;
    ref2: Point;
    refLength: number;
    angleDeg: number;
}

const defaultImageNodeProps: ImageNodeProps = {
    image: DEFAULT_IMAGE,
    name: "Image",
    selected: false,
    origin: {x: 0, y: 0},
    ref1: {x: 1000, y: 0},
    ref2: {x: 0, y: 1000},
    refLength: 1000,
    angleDeg: 0,
};

class ImageNode extends Record<ImageNodeProps>(defaultImageNodeProps) implements ProjectNode {

    get type() {
        return "IMAGE_NODE";
    }

    get transform() {
        // Coordinates of the space origin in the image space (in px)
        const ox = this.origin.x;
        const oy = this.origin.y;

        // Vector from the space origin in the image space to the point mapped to (1000, 0)
        const dx = this.ref1.x - this.ref2.x;
        const dy = this.ref1.y - this.ref2.y;

        // Angle in radian between the image space and the canvas space
        const rotRad = this.angleDeg / 180 * Math.PI;

        const cos = Math.cos(rotRad);
        const sin = Math.sin(rotRad);

        // Scale to apply to the image space to map it to the canvas space
        const scale = this.refLength / Math.sqrt(dx * dx + dy * dy);

        // Translation to apply to the image space to map it to the canvas space
        const x = (- ox * cos + oy * sin) * scale;
        const y = (- ox * sin - oy * cos) * scale;

        return new Transform({
            position: {x, y},
            rotation: rotRad * 180 / Math.PI,
            scale ,
        });
    }
}

export {ImageNode};
