import {Field, Form, Formik} from "formik";
import * as React from "react";

import {ImageNode} from "./ImageNode";
import {ProjectNode} from "./ProjectNode";

interface ImagePropertyEditorProps {
    node: ImageNode;
    onNodeUpdated: (node: ProjectNode) => void;
}

class ImagePropertyEditor extends React.Component<ImagePropertyEditorProps> {
    public render() {
        const {node, onNodeUpdated} = this.props;
        const initialValues = node.toJSON();
        return (
            <Formik
                enableReinitialize={true}
                onSubmit={() => null}
                validate={
                    (values) => {
                        onNodeUpdated(new ImageNode(values));
                        return {};
                    }
                }
                initialValues={initialValues}
                render={(handleSubmit) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="ref1.y">Origin (px)</label>
                            <div className="input-group mt-1">
                                <Field className="form-control" type="number" lang="en" name="origin.x" />
                                <Field className="form-control" type="number" lang="en" name="origin.y" />
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="angleDef">Angle (°)</label>
                            <Field className="form-control" type="number" name="angleDeg" />
                        </div>
                        <div className="form-group">
                            <div>Reference segment</div>
                            <label htmlFor="refLength">Length (mm)</label>
                            <Field className="form-control" type="number" lang="en" name="refLength" />
                            <div>Ends</div>
                            <div className="input-group mt-1">
                                <div className="input-group-prepend">
                                    <label className="input-group-text" htmlFor="ref1.y">P1 (px)</label>
                                </div>
                                <Field className="form-control" type="number" lang="en" name="ref1.x" />
                                <Field className="form-control" type="number" lang="en" name="ref1.y" />
                            </div>
                            <div className="input-group mt-1">
                                <div className="input-group-prepend">
                                    <label className="input-group-text" htmlFor="ref2.y">P2 (px)</label>
                                </div>
                                <Field className="form-control" type="number" lang="en" name="ref2.x" />
                                <Field className="form-control" type="number" lang="en" name="ref2.y" />
                            </div>
                        </div>
                    </Form>
                )}
            />
        );
    }
}

export {ImagePropertyEditor};
