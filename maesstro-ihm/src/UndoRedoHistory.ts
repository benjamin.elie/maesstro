import {Stack} from "immutable";

class UndoRedoHistory<T> {

    public readonly currentValue: T;

    constructor(initialValue: T, private undoStack: Stack<T> = Stack(), private redoStack: Stack<T> = Stack() ) {
        this.currentValue = initialValue;
    }

    public push(value: T) {
        return new UndoRedoHistory<T>(value, this.undoStack.push(this.currentValue), Stack());
    }

    public canRedo() {
        return !this.redoStack.isEmpty();
    }

    public redo() {
        if (!this.canRedo()) {
            throw Error("Nothing to redo.");
        }
        return new UndoRedoHistory<T>(
            this.redoStack.first(),
            this.undoStack.push(this.currentValue),
            this.redoStack.pop(),
        );
    }

    public canUndo() {
        return !this.undoStack.isEmpty();
    }

    public undo() {
        if (!this.canUndo()) {
            throw Error("Nothing to undo");
        }
        return new UndoRedoHistory<T>(
            this.undoStack.first(),
            this.undoStack.pop(),
            this.redoStack.push(this.currentValue),
        );
    }
}

export {UndoRedoHistory};
