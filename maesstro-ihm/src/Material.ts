export interface Material {
    readonly identifier: string;
    readonly rho: number;
    readonly Ex: number;
    readonly Ey: number;
    readonly Gxy: number;
    readonly Gxz: number;
    readonly Gyz: number;
    readonly nuxy: number;
    readonly nuyx: number;
    readonly damping: number;
}

export function isMaterial(material: any): material is Material {
    const {
        identifier,
        rho,
        Ex,
        Ey,
        Gxy,
        Gxz,
        Gyz,
        nuxy,
        nuyx,
        damping,
    } = material;
    return (
        typeof identifier === "string" &&
        [ rho, Ex, Ey, Gxy, Gxz, Gyz, nuxy, nuyx, damping ].every(
            (v) => typeof v === "number",
        )
    );
}
