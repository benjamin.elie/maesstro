import {
    ArrayHelpers,
    Field,
    FieldArray,
    Form,
    Formik,
    FormikErrors,
    getIn,
} from "formik";
import * as React from "react";
import Button from "react-bootstrap/es/Button";
import Modal from "react-bootstrap/es/Modal";

import {FileService} from "./FileService";
import {Material} from "./Material";
import {MaterialRegistry} from "./MaterialRegistry";
import {MaterialRegistryExporter} from "./MaterialRegistryExporter";
import {MaterialRegistryImporter} from "./MaterialRegistryImporter";
interface FormValues {
    materials: Material[];
}

interface MaterialRegistryEditorProps {
    show: boolean;
    onHide: () => void;
    materialRegistry: MaterialRegistry;
    onSave: (materialRegistry: MaterialRegistry) => void;
    fileService: FileService;
}

function validateNumberField(value: string | number): string | undefined {
    value = value.toString();
    const valueWithoutSpace = value.replace(/\s/, "");
    if (value.length !== valueWithoutSpace.length) {
        return "Extra space character in field";
    } else if (Number.isNaN(Number(value))) {
        return `Invalid number ${value}`;
    }
}

class MaterialRegistryEditor extends React.Component<MaterialRegistryEditorProps> {

    public render() {
        const {materialRegistry} = this.props;
        return (
            <Formik
                enableReinitialize={true}
                initialValues={{
                    materials: materialRegistry.getAllMaterials(),
                }
                }
                onSubmit={this.handleSubmit}
                render={(props) => this.renderForm(props)}
            />
        );
    }

    private renderForm({values,  errors}: {values: FormValues, errors: FormikErrors<{materials: Material[]}>}) {
        const {show, onHide} = this.props;
        return (
            <Modal
                show={show}
                onHide={onHide}
                dialogClassName="material-registry-editor-dialog"
            >
                <Form>
                    <Modal.Header closeButton>
                        <Modal.Title>Edit Materials</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        {this.renderHeader()}
                        <FieldArray
                            name="materials"
                            render={(arrayHelpers) => (
                                <div>
                                    {values.materials.map((material, index) =>
                                        this.renderMaterialRow(arrayHelpers, material, index, errors),
                                    )}
                                </div>
                            )}
                        />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            variant="secondary"
                            onClick={this.handleImportClick}
                        >
                            <i className="fas fa-file-import mr-1"></i>
                            Import
                        </Button>
                        <Button
                            variant="secondary"
                            onClick={this.handleExportClick}
                        >
                            <i className="fas fa-file-export mr-1"></i>
                            Export
                        </Button>
                        <Button
                            className="ml-auto"
                            type="submit"
                            variant="primary"
                        >
                            Save
                        </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        );
    }

    private renderHeader() {
        return (
            <div className="form-row">
                {["identifier", "rho (kg/m^3)", "Ex (GPa)", "Ey (GPa)", "Gxy (GPa)",
                "Gxz (GPa)", "Gyz (GPa)", "nuxy", "nuyx", "damping"].map((property) => (
                    <div className="col">
                        {property}
                    </div>
                ))}
            </div>
        );
    }

    private renderMaterialRow(
        arrayHelpers: ArrayHelpers,
        material: Material,
        index: number,
        errors: FormikErrors<{materials: Material[]}>,
    ) {
        return (
            <div key={index} className="form-row">
                <div className="col">
                    <Field
                        className="form-control"
                        name={`materials.${index}.identifier`}
                    />
                </div>
                {["rho", "Ex", "Ey", "Gxy", "Gxz", "Gyz", "nuxy", "nuyx", "damping"].map((property) => (
                    this.renderNumberField(`materials.${index}.${property}`, errors)
                ))}
            </div>
        );
    }

    private renderNumberField(name: string, errors: FormikErrors<{materials: Material[]}>) {
        const error = getIn(errors, name);
        return (
            <div key={name} className="col">
                <Field
                    validate={validateNumberField}
                    className={"form-control" + (error ? " is-invalid" : "")}
                    title={error || null}
                    name={name}
                />
            </div>
        );
    }

    private handleSubmit = (values: FormValues) => {
        const {onSave} = this.props;

        onSave(new MaterialRegistry(values.materials));
    }

    private handleImportClick = () => {
        const importer = new MaterialRegistryImporter();
        this.props.fileService.openFile().then((file) =>
            importer.import(file),
        ).then(this.props.onSave).catch( () =>
            console.error("Could not load file"),
        ).then(this.props.onHide);
    }

    private handleExportClick = () => {
        const exporter = new MaterialRegistryExporter();
        exporter.export(this.props.materialRegistry).then((blob) =>
            this.props.fileService.saveFile(blob, "Materials.csv"),
        ).catch(() => console.error("Could not save materials"));
    }
}

export {MaterialRegistryEditor};
