import {parse as parseCSV, ParseConfig, ParseResult} from "papaparse";

import {isMaterial} from "./Material";
import {MaterialRegistry} from "./MaterialRegistry";

export class MaterialRegistryImporter {

    public import(blob: Blob): Promise<MaterialRegistry> {
        return this.parseCSVFileToJSON(blob).then((data: any[]) => {
            if (data.every(isMaterial)) {
                return new MaterialRegistry(data);
            } else {
                throw Error("Invalid CSV file");
            }
        });
    }

    private parseCSVFileToJSON(blob: Blob): Promise<any[]> {
        return new Promise((resolve, reject) => {
            function completeCb(result: ParseResult) {
                if (result.errors.length > 0) {
                    reject(result.errors);
                }
                resolve(result.data);
            }
            parseCSV(blob as File, {
                transformHeader: (header: string) => header.split(" ")[0],
                complete: completeCb,
                header: true,
                skipEmptyLines: "greedy",
                dynamicTyping: true,
            } as ParseConfig);
        });
    }
}
