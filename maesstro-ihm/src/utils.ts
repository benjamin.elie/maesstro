import * as Konva from "konva";

import {Point} from "./Point";

function round2dPoint(point: Point, decimals = 2) {
    const mult = 10 ** decimals;
    const pt = {...point};
    pt.x = Math.round(pt.x * mult) / mult;
    pt.y = Math.round(pt.y * mult) / mult;
    return pt;
}

function isNotNull<TValue>(value: TValue | null): value is TValue {
        return value !== null;
}

function pointerPosition(event: Konva.KonvaEventObject<MouseEvent>) {
    const stage =  event.target.getStage();
    const domPointerPosition = stage.getPointerPosition();
    const x = (domPointerPosition.x / stage.scaleX()) + stage.offsetX();
    const y = (domPointerPosition.y / stage.scaleY()) + stage.offsetY();
    return {x, y};
}

function imageFromBlob(imageBlob: Blob): Promise<HTMLImageElement> {
    return new Promise((resolve, reject) => {
        const img = document.createElement("img");
        img.src = URL.createObjectURL(imageBlob);
        img.onload = () => resolve(img);
    });
}

function JSONFromBlob(blob: Blob): Promise<any> {
    const reader = new FileReader();
    return new Promise((resolve, reject) => {
        reader.addEventListener("loadend", (e) => {
            resolve(JSON.parse(reader.result as string));
        });
        reader.readAsText(blob);
    });
}

function distanceBetweenPoints(pt1: Point, pt2: Point) {
    const dx = pt2.x - pt1.x;
    const dy = pt2.y - pt1.y;
    return Math.sqrt(dx * dx + dy * dy);
}

export {
    isNotNull,
    pointerPosition,
    imageFromBlob,
    JSONFromBlob,
    round2dPoint,
    distanceBetweenPoints,
};
