import {List} from "immutable";
import * as Konva from "konva";
import * as React from "react";
import {Line} from "react-konva";

import {BridgeNode} from "../BridgeNode";
import {AngleGraphic} from "../Graphics/Angle";
import {BridgeGraphic} from "../Graphics/Bridge";
import {ImageGraphic} from "../Graphics/Image";
import {PanelGraphic} from "../Graphics/Panel";
import {RibGraphic} from "../Graphics/Rib";
import {ImageNode} from "../ImageNode";
import {PanelNode} from "../PanelNode";
import {Point} from "../Point";
import {ProjectNode} from "../ProjectNode";
import {RibNode} from "../RibNode";
import {Transform} from "../Transform";
import {
    distanceBetweenPoints,
    pointerPosition,
    round2dPoint,
} from "../utils";
import {Manipulator} from "./Manipulator";
import {Tool, ToolProps} from "./Tool";

class EditTool extends Tool {

    constructor(props: ToolProps) {
        super(props);
    }

    public render() {
        return this.props.children({
            viewportHandlers: {
                onClick: this.handleClick,
            },
            viewportChildren: this.renderViewportChildren(),
        });
    }

    private renderViewportChildren() {
        const {node} = this.props;
        if (node instanceof PanelNode) {
            return this.renderPanelEditor(node);
        } else if (node instanceof BridgeNode) {
            return this.renderBridgeEditor(node);
        } else if (node instanceof ImageNode) {
            return this.renderImageEditor(node);
        } else if (node instanceof RibNode) {
            return this.renderRibEditor(node);
        } else {
            return null;
        }
    }

    private renderImageEditor(imageNode: ImageNode) {
        const {onNodeUpdated} = this.props;
        const {ref1, ref2} = imageNode;
        return (
            <>
                <ImageGraphic
                    key={"image"}
                    image={imageNode.image}
                    transform={new Transform()}
                    opacity={1}
                />
                <Manipulator
                    key={"origin"}
                    {...imageNode.origin}
                    onMove={(p) =>
                        onNodeUpdated(imageNode.set("origin", round2dPoint(p, 0)))
                    }
                />
                <Line ref={"refSegment"}
                    points={[ref1.x, ref1.y, ref2.x, ref2.y]}
                    stroke={"#722074"}
                />
                <Manipulator
                    key={"ref1"}
                    {...ref1}
                    onMove={(p) => onNodeUpdated(imageNode.set("ref1", round2dPoint(p, 0)))}
                />
                <Manipulator
                    key={"ref2"}
                    {...ref2}
                    onMove={(p) => onNodeUpdated(imageNode.set("ref2", round2dPoint(p, 0)))}
                />
            </>
        );
    }

    private renderLineNode(node: ProjectNode, pointsPath: string[]) {
        const {onNodeUpdated} = this.props;
        const line = node.getIn(pointsPath).toArray();
        const pointUpdater = (i: number, p: Point) => onNodeUpdated(node.setIn([...pointsPath, i], round2dPoint(p)));
        return (
            <>
                {line.map((point: Point, i: number) =>
                    <Manipulator
                        key={i}
                        {...point}
                        onMove={(p) =>
                            pointUpdater(i, {...point, ...p})
                        }
                    />,
                )}
            </>
        );
    }

    private renderPanelEditor(panelNode: PanelNode) {
        return (
            <>
                <PanelGraphic panel={panelNode.panel} selected={true}/>
                <AngleGraphic origin={{x: 0, y: 0}} angle={panelNode.panel.orthotropicAngleDeg}/>
                {this.renderLineNode(panelNode, ["panel", "contour"])}
            </>
        );
    }

    private renderBridgeEditor(bridgeNode: BridgeNode) {
        return (
            <>
                <BridgeGraphic bridge={bridgeNode.bridge} />
                {this.renderLineNode(bridgeNode, ["bridge", "medianLine"])}
            </>
        );
    }

    private renderRibEditor(ribNode: RibNode) {
        const {onNodeUpdated} = this.props;
        const {start,  end} = ribNode.rib;
        const startUpdater = (point: Point) =>
            onNodeUpdated(ribNode.setIn(["rib", "start"], {...start,  ...round2dPoint(point)}));
        const endUpdater = (point: Point) =>
            onNodeUpdated(ribNode.setIn(["rib", "end"], {...end,  ...round2dPoint(point)}));
        return (
            <>
                <RibGraphic rib={ribNode.rib} />
                <Manipulator {...start} onMove={startUpdater}/>
                <Manipulator {...end} onMove={endUpdater}/>
            </>
        );
    }

    private bridgePointFromPosition(position: Point) {
        return {...position,  width: 20, height: 20, isReference: false};
    }

    private thickPointFromPosition(position: Point) {
        return {...position,  thickness: 8};
    }

    private handleClick = (event: Konva.KonvaEventObject<MouseEvent>) => {
        if (event.evt.button !== 0) {
            return;
        }
        const position = round2dPoint(pointerPosition(event));
        if (event.evt.shiftKey) {
            this.deletePoint(position);
        } else if (event.evt.ctrlKey) {
            this.insertPoint(position);
        } else {
            this.addPoint(position);
        }
    }

    private addPoint(position: Point) {
        const {node,  onNodeUpdated} = this.props;
        if (node instanceof PanelNode) {
            const point = this.thickPointFromPosition(position);
            const line = node.panel.contour;
            onNodeUpdated(node.setIn(["panel", "contour"], line.push(point)));
        } else if (node instanceof BridgeNode) {
            const point = this.bridgePointFromPosition(position);
            const line = node.bridge.medianLine;
            onNodeUpdated(node.setIn(["bridge", "medianLine"], line.push(point)));
        }
    }

    private deletePoint(position: Point) {
        const {node, onNodeUpdated} = this.props;
        let linePath;
        if (node instanceof PanelNode) {
            linePath = ["panel", "contour"];
        } else if (node instanceof BridgeNode) {
            linePath = ["bridge", "medianLine"];
        } else {
            return;
        }
        const line = node.getIn(linePath);
        const nearest = this.getNearestPoint(line, position);
        if (nearest !== undefined) {
            const {index, dist} = nearest;
            if (dist < 10) {
                onNodeUpdated(node.setIn(linePath, line.delete(index)));
            }
        }
    }

    private insertPoint(position: Point) {
        const {node, onNodeUpdated} = this.props;
        let update: (point: Point, index: number) => void;
        let line: List<Point>;
        if (node instanceof PanelNode) {
            line = node.panel.contour;
            const first = line.get(0);
            if (first !== undefined) {
                line = line.push(first);
            }
            update = (point, index) => {
                const thickPoint = this.thickPointFromPosition(point);
                onNodeUpdated(node.setIn(["panel", "contour"], node.panel.contour.insert(index, thickPoint)));
            };

        } else if (node instanceof BridgeNode) {
            line = node.bridge.medianLine;
            update = (point, index) => {
                const bridgePoint = this.bridgePointFromPosition(point);
                onNodeUpdated(node.setIn(["bridge", "medianLine"], line.insert(index, bridgePoint)));
            };
        } else {
            return;
        }

        const projection = this.projectPointOnLine(line, position);
        if (projection !== undefined) {
            const {dist, point, index} = projection;
            if (dist < 10) {
                update(round2dPoint(point), index);
            }
        }
    }

    private getNearestPoint(line: List<Point>, position: Point) {
        let best: undefined | {dist: number, point: Point, index: number};
        line.forEach((point, index) => {
            const dist = distanceBetweenPoints(point, position);
            if (best === undefined ||  best.dist > dist) {
                best = {
                    dist,
                    point,
                    index,
                };
            }
        });
        return best;
    }

    private projectPointOnLine(line: List<Point>, point: Point) {
        const segments = line.zip(line.shift());
        let best: undefined | {dist: number, point: Point, index: number};
        segments.forEach(([A, B], index) => {
            const AP = {x: point.x - A.x, y: point.y - A.y};
            const AB = {x: B.x - A.x, y: B.y - A.y};
            const dotAPAB = AP.x * AB.x + AP.y * AB.y;
            const dotABAB = AB.x * AB.x + AB.y * AB.y;
            const projection = {
                x: A.x + dotAPAB / dotABAB * AB.x,
                y: A.y + dotAPAB / dotABAB * AB.y,
            };
            const dist = distanceBetweenPoints(projection, point);
            if (best === undefined ||  best.dist > dist) {
                best = {
                    dist,
                    index: index + 1,
                    point: projection,
                };
            }
        });
        return best;
    }
}

export {EditTool};
