import * as React from "react";
import {KonvaNodeProps} from "react-konva";

import {ProjectNode} from "../ProjectNode";

interface PropsProvidedByTool {
    viewportHandlers: KonvaNodeProps;
    viewportChildren: React.ReactElement<any> | null;
}
interface ToolProps {
    node: ProjectNode | null;
    onNodeUpdated: (node: ProjectNode) => void;
    children: (props: PropsProvidedByTool) => React.ReactElement<any>;
}

abstract class Tool extends React.Component<ToolProps> {

    public abstract render(): React.ReactElement<any>;
}

export {Tool, ToolProps};
