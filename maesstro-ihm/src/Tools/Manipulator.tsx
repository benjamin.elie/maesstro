import * as React from "react";
import {Circle, Group, Line} from "react-konva";

interface ManipulatorProps {
    x: number;
    y: number;
    onMove: (position: {x: number, y: number}) => void;
}

class Manipulator extends React.Component<ManipulatorProps, {dragging: boolean}> {
    constructor(props: ManipulatorProps) {
        super(props);
        this.state = {
            dragging: false,
        };
    }

    public setDragging = (dragging: boolean) => {
        this.setState({dragging});
    }

    public render() {
        const {dragging} = this.state;
        const color = dragging ? "rgba(0, 0, 0, 0)" : "rgba(0, 0, 0, 0.1)";
        return (
            <Group
                draggable={true}
                onDragMove={(e) => this.props.onMove({x: e.target.x(), y: e.target.y()})}
                x={this.props.x}
                y={this.props.y}
                onDragStart={() => this.setDragging(true)}
                onDragEnd={() => this.setDragging(false)}
            >
                <Circle
                    strokeWidth={1}
                    stroke={"black"}
                    radius={8}
                    fill={color}
                />
                    {dragging && (
                        <>
                            <Line
                                strokeWidth={1}
                                stroke={"black"}
                                points={[-2, -2, 2, 2]}
                            />
                            <Line
                                strokeWidth={1}
                                stroke={"black"}
                                points={[-2, 2, 2, -2]}
                            />
                        </>
                    )}
            </Group>
        );
    }

}

export {Manipulator};
