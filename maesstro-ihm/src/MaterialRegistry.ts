import {Material} from "./Material";

interface MaterialMap {
    [identifier: string]: Material;
}
import * as defaultMaterials from "./DefaultMaterials.json";

export class MaterialRegistry {
    private materials: MaterialMap = {};
    private materialsIds: string[] = [];

    constructor(materials: Material[]) {
        if (materials.length === 0) {
            throw Error("Not enough materials");
        }
        for (const material of materials) {
            this.materials[material.identifier] = material;
            this.materialsIds.push(material.identifier);
        }
    }

    public hasMaterial(identifier: string) {
        return this.materials[identifier] != null;
    }

    public getAllMaterialsIds(): Readonly<string[]> {
        return this.materialsIds;
    }

    public getMaterial(identifier: string): Material {
        return this.materials[identifier] || null;
    }

    public getAllMaterials(): Readonly<Material[]> {
        return this.materialsIds.map((id) => this.getMaterial(id));
    }
}

export const DEFAULT_MATERIAL_REGISTRY = new MaterialRegistry(defaultMaterials);
