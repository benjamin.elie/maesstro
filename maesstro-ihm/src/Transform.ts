import {Record} from "immutable";

import {Point} from "./Point";

interface TransformProps {
    position: Point;
    rotation: number;
    scale: number;
}
const defaultTransformProps = {
    position: {x: 0, y: 0},
    rotation: 0,
    scale: 1,
};
class Transform extends Record<TransformProps>(defaultTransformProps) {}

export {Transform};
