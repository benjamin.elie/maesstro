import * as React from "react";

import {BridgeNode} from "./BridgeNode";
import {BridgePropertyEditor} from "./BridgePropertyEditor";
import {ImageNode} from "./ImageNode";
import {ImagePropertyEditor} from "./ImagePropertyEditor";
import {MaterialRegistry} from "./MaterialRegistry";
import {PanelNode} from "./PanelNode";
import {PanelPropertyEditor} from "./PanelPropertyEditor";
import {ProjectNode} from "./ProjectNode";
import {RibNode} from "./RibNode";
import {RibPropertyEditor} from "./RibPropertyEditor";

interface NodePropertyEditorProps {
    materialRegistry: MaterialRegistry;
    node: ProjectNode;
    onNodeUpdated: (node: ProjectNode) => void;
}

class NodePropertyEditor extends React.Component<NodePropertyEditorProps> {

    public render() {
        const {name} = this.props.node;
        return (
            <div className="property-editor">
                <div className="form-group">
                    <label htmlFor="name" >Name</label>
                    <input
                        className="form-control"
                        name="name"
                        onChange={this.handleNameInputChange}
                        onBlur={this.handleNameInputBlur}
                        value={name}
                    ></input>
                </div>
                {this.renderSpecificForm()}
            </div>
        );
    }

    private renderSpecificForm() {
        const {materialRegistry, node, onNodeUpdated} = this.props;
        if (node instanceof ImageNode) {
            return (
                <ImagePropertyEditor
                    node={node}
                    onNodeUpdated={onNodeUpdated}
                />);
        } else if (node instanceof PanelNode) {
            return (
                <PanelPropertyEditor
                    materialRegistry={materialRegistry}
                    node={node}
                    onNodeUpdated={onNodeUpdated}
                />);
        } else if (node instanceof BridgeNode) {
            return (
                <BridgePropertyEditor
                    materialRegistry={materialRegistry}
                    node={node}
                    onNodeUpdated={onNodeUpdated}
                />);
        } else if (node instanceof RibNode) {
            return (
                <RibPropertyEditor
                    materialRegistry={materialRegistry}
                    node={node}
                    onNodeUpdated={onNodeUpdated}
                />);
        } else {
            return null;
        }
    }

    private handleNameInputChange = (event: {target: HTMLInputElement}) => {
        const {node} = this.props;
        this.props.onNodeUpdated(node.set("name", event.target.value));
    }

    private handleNameInputBlur = (event: {target: HTMLInputElement}) => {
        const name = event.target.value || "Unnamed";
        const {node} = this.props;
        this.props.onNodeUpdated(node.set("name", name));
    }
}

export {NodePropertyEditor};
