import {unparse as toCSV} from "papaparse";

import {Material} from "./Material";
import {MaterialRegistry} from "./MaterialRegistry";

export class MaterialRegistryExporter {

    public async export(materialRegistry: MaterialRegistry): Promise<Blob> {
        const csv = this.toCSV(materialRegistry.getAllMaterials());
        return new Blob([csv], {type: "text/csv"});
    }

    public toCSV(materials: Material[]) {
        const fields = ["identifier", "rho (kg/m^3)", "Ex (GPa)", "Ey (GPa)", "Gxy (GPa)",
        "Gxz (GPa)", "Gyz (GPa)", "nuxy", "nuyx", "damping (%)"];
        const data = materials.map((material) => Object.keys(material).map((key) => (material as any)[key]),
);
        return toCSV({fields, data});
    }

}
