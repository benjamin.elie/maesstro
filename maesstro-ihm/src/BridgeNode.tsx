import {Record} from "immutable";

import {Bridge} from "./Bridge";
import {ProjectNode} from "./ProjectNode";
import {Transform} from "./Transform";

interface BridgeNodeProps {
    bridge: Bridge;
    transform: Transform;
    name: string;
    selected: boolean;
}

const defaultBridgeNodeProps: BridgeNodeProps = {
    bridge: new Bridge(),
    transform: new Transform(),
    name: "Bridge",
    selected: false,
};

class BridgeNode extends Record<BridgeNodeProps>(defaultBridgeNodeProps) implements ProjectNode {
    get type() {
        return "BRIDGE_NODE";
    }
}

export {BridgeNode};
