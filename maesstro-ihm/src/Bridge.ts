import {List, Record} from "immutable";
import {BridgePoint} from "./Point";

interface BridgeProps {
    medianLine: List<BridgePoint>;
    materialId: string;
}

const BridgeDefaultProps = {
    medianLine: List<BridgePoint>(),
    materialId: "Norway Spruce 1",
};

class Bridge extends Record<BridgeProps>(BridgeDefaultProps) {}

function fromJS(obj: any): Bridge {
    const bridge = new Bridge().asMutable();
    if (obj.medianLine) {
        bridge.set("medianLine", List(obj.medianLine));
    }
    if (obj.materialId != null) {
        bridge.set("materialId", obj.materialId);
    }
    return bridge.asImmutable();
}

export {Bridge, fromJS};
