import {List} from "immutable";
import * as JSZip from "jszip";

import {ImageNode} from "./ImageNode";
import {Project} from "./Project";
import {ProjectNode} from "./ProjectNode";

class ProjectExporter {
    private static IMAGE_NAME_PREFIX = "Image";
    private imageIndex = 0;
    public export(project: Project) {
        const zip = JSZip();
        const jsonPromise = this.projectToJSON(project, zip);
        return jsonPromise.then((json: any) => {
            zip.file("project.json", JSON.stringify(json, null, 2));
            return zip.generateAsync({type: "blob"});
        });
    }

    private projectToJSON(project: Project, zip: JSZip) {
        return this.nodesToJSON(project.nodes, zip).then(
            (nodes) => {
                const materials = project.materialRegistry.getAllMaterials();
                return {nodes, materials};
            },
        );
    }

    private nodesToJSON(nodes: List<ProjectNode>, zip: JSZip) {
        const jsonPromises = nodes.map((node) => {
            return this.nodeToJSON(node, zip).then((jsonNode) => {
                jsonNode.type = node.type;
                return jsonNode;
            });
        });
        return Promise.all(jsonPromises);
    }

    private nodeToJSON(node: ProjectNode, zip: JSZip) {
            if (node instanceof ImageNode) {
                const imageName = ProjectExporter.IMAGE_NAME_PREFIX + (this.imageIndex++);
                return this.extractImageFromImageNode(imageName, node, zip);
            }
            return Promise.resolve(node.toJSON());
    }

    private extractImageFromImageNode(imageName: string, node: ImageNode, zip: JSZip) {
        return this.resolveReference(node.image.src).then((blob) => {
            zip.file(imageName, blob);
            const json: any = node.toJSON();
            delete json.image;
            json.imageFileName = imageName;
            return json;
        });
    }

    private resolveReference(url: string): Promise <Blob> {
        return fetch(url).then((r) => r.blob());
    }
}

export {ProjectExporter};
