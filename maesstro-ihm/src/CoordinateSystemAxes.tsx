import * as React from "react";
import {Line} from "react-konva";

class CoordinateSystemAxes extends React.Component {

    public render() {
        const axisProps = {
            stroke: "grey",
            strokeWidth: 1,
        };
        return (
            <>
                <Line
                    points={[0, -2000, 0, 2000]}
                    {...axisProps}
                />
                <Line
                    points={[-2000, 0, 2000, 0]}
                    {...axisProps}
                />
            </>
        );
    }
}

export {CoordinateSystemAxes};
