interface Point {
    x: number;
    y: number;
}

interface RibPoint extends Point {
    width: number;
    height: number;
}

interface ThickPoint extends Point {
    thickness: number;
}

interface BridgePoint extends Point {
    width: number;
    height: number;
    isReference: boolean;
}

export {Point, RibPoint, BridgePoint, ThickPoint};
