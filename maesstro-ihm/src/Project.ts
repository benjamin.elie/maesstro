import {List} from "immutable";
import {MaterialRegistry} from "./MaterialRegistry";
import {ProjectNode} from "./ProjectNode";

class Project {

    constructor(
        public readonly nodes: List<ProjectNode>,
        public readonly  materialRegistry: MaterialRegistry,
    ) {}

    public updateSelectedNode(node: ProjectNode) {
        const selectedNodeIdx = this.selectedNodeIdx();
        if (selectedNodeIdx !== -1) {
            return this.withNodes(this.nodes.set(selectedNodeIdx, node));
        }
        throw Error("Can't modify selectedNode when no node is selected");
    }

    public get selectedNode() {
        return this.nodes.find((n) => n.selected) || null;
    }

    public addNode(node: ProjectNode) {
        const nodes = this.nodes.map((n) => n.set("selected", false)).push(
            node.set("selected", true) .set("name", this.nextAvailableName(node.name)),
        );
        return this.withNodes(nodes);
    }

    public selectNode(nodeIdx: number|null) {
        if (nodeIdx == null) {
            return this.deselectAll();
        }
        const nodes = this.nodes.map((node: ProjectNode, idx: number) =>
            node.set("selected", (nodeIdx === idx)),
        );
        return this.withNodes(nodes);
    }

    public deselectAll() {
        const nodes = this.nodes.map((node) => node.set("selected", false));
        return this.withNodes(nodes);
    }

    public deleteSelectedNode() {
        const selectedNodeIdx = this.selectedNodeIdx();
        const nodesCount = this.nodes.size;
        const nextSelectedNode = nodesCount > 1 ? Math.min(selectedNodeIdx, nodesCount - 2 ) : null;
        const nodes = this.nodes.delete(selectedNodeIdx);
        return this.withNodes(nodes).selectNode(nextSelectedNode);
    }

    public withMaterialRegistry(materialRegistry: MaterialRegistry) {
        return new Project(this.nodes, materialRegistry);
    }

    private withNodes(nodes: List<ProjectNode>) {
        return new Project(nodes, this.materialRegistry);
    }

    private nextAvailableName(prefix: string) {
        let i = 0;
        let name = prefix;
        while (this.nodes.find((node) => node.name === name)) {
            name = `${prefix}${i++}`;
        }
        return name;
    }

    private selectedNodeIdx() {
        return this.nodes.findIndex((n) => n.selected);
    }
}

export {Project};
