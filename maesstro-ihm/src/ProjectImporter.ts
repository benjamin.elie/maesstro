import {List} from "immutable";
import * as JSZip from "jszip";

import {fromJS as bridgeFromJS} from "./Bridge";
import {BridgeNode} from "./BridgeNode";
import {ImageNode} from "./ImageNode";
import {MaterialRegistry} from "./MaterialRegistry";
import {fromJS as panelFromJS} from "./Panel";
import {PanelNode} from "./PanelNode";
import {Project} from "./Project";
import {ProjectNode} from "./ProjectNode";
import {fromJS as ribFromJS} from "./Rib";
import {RibNode} from "./RibNode";
import {Transform} from "./Transform";
import {imageFromBlob, isNotNull} from "./utils";

interface ProjectFiles {
    json: any;
    images: {[key: string]: Blob};
}

class ProjectImporter {
    constructor(private defaultMaterialRegistry: MaterialRegistry) {}

    public async import(blob: Blob): Promise<Project> {
        const projectFiles = await this.getProjectFiles(blob);
        return this.projectFromProjectFiles(projectFiles);
    }

    private async getProjectFiles(blob: Blob): Promise<ProjectFiles> {
        const zip = await JSZip.loadAsync(blob);
        const json = await zip.file("project.json").async("text").then(JSON.parse);
        const images: ProjectFiles["images"] = {};
        for (const fileObject of zip.file(/^Image/)) {
            images[fileObject.name] = await fileObject.async("blob");
        }
        return {json, images};
    }

    private async projectFromProjectFiles(projectFiles: ProjectFiles): Promise<Project> {
        const {json, images} = projectFiles;
        const jsonNodes: any[] = json.nodes;
        const nodesPromises = jsonNodes.map((jsonNode) => this.nodeFromJSONNode(jsonNode, images));
        const nodesArray = await Promise.all(nodesPromises);
        const nodes = List<ProjectNode>(nodesArray.filter(isNotNull));
        let materialRegistry = this.defaultMaterialRegistry;
        try {
            materialRegistry = new MaterialRegistry(json.materials);
        } catch {
            console.warn("Materials not found in project. Fallback to using default materials.");
        }
        return new Project(nodes, materialRegistry);
    }

    private async nodeFromJSONNode(jsonNode: any, images: ProjectFiles["images"]): Promise<ProjectNode|null> {
        const nodeType = jsonNode.type;
        delete jsonNode.type;
        if (nodeType === "IMAGE_NODE") {
            const {imageFileName, ...nodeProps} = jsonNode;
            return imageFromBlob(images[jsonNode.imageFileName]).then((image) => {
                nodeProps.image = image;
                return new ImageNode(nodeProps);
            });
        } else if (nodeType === "PANEL_NODE") {
            const {transform: transfromProps, panel: jsonPanel, ...nodeProps} = jsonNode;
            nodeProps.transform = new Transform(transfromProps);
            nodeProps.panel = panelFromJS(jsonPanel);
            return Promise.resolve(new PanelNode(nodeProps));
        } else if (nodeType === "BRIDGE_NODE") {
            const {transform: transfromProps, bridge: jsonBridge, ...nodeProps} = jsonNode;
            nodeProps.transform = new Transform(transfromProps);
            nodeProps.bridge = bridgeFromJS(jsonBridge);
            return new BridgeNode(nodeProps);
        } else if (nodeType === "RIB_NODE") {
            const {transform: transfromProps, rib: jsonRib, ...nodeProps} = jsonNode;
            nodeProps.transform = new Transform(transfromProps);
            nodeProps.rib = ribFromJS(jsonRib);
            return new RibNode(nodeProps);
        }
        return null;
    }

}

export {ProjectImporter};
