interface HTMLFileInputElement extends HTMLInputElement {
    type: "file";
    files: FileList;
}

class FileService {

    public openFile(): Promise<File> {
        const input = this.createFileInput();
        return new Promise<File>((resolve, reject) => {
            function handleChange() {
                resolve(input.files[0]);
            }
            input.addEventListener("change", handleChange, {once: true});
            this.clickOn(input);
        });
    }

    public saveFile(blob: Blob, filename: string) {
        const url = URL.createObjectURL(blob);
        const link = document.createElement("a");
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        this.clickOn(link);
    }

    private createFileInput() {
        const input = document.createElement("input");
        input.type = "file";
        return input as HTMLFileInputElement;
    }

    private clickOn(element: HTMLElement): void {
        element.dispatchEvent(new MouseEvent(
            "click", {
                bubbles: false,
                cancelable: false,
                view: window,
            }),
        );

    }
}

export {FileService};
