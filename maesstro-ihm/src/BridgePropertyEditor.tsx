import {Field, Form, Formik} from "formik";
import {List} from "immutable";
import * as React from "react";

import {Bridge} from "./Bridge";
import {BridgeNode} from "./BridgeNode";
import {MaterialRegistry} from "./MaterialRegistry";
import {MaterialSelectField} from "./MaterialSelectField";
import {BridgePoint} from "./Point";
import {ProjectNode} from "./ProjectNode";

interface ImagePropertyEditorProps {
    materialRegistry: MaterialRegistry;
    node: BridgeNode;
    onNodeUpdated: (node: ProjectNode) => void;
}

interface FormValues {
    referencePointIdx: string;
    medianLine: BridgePoint[];
    materialId: string;
}

class BridgePropertyEditor extends React.Component<ImagePropertyEditorProps> {

    public render() {
        const {materialRegistry, node, onNodeUpdated} = this.props;
        const initialValues: FormValues = this.bridgeToFormValues(node.bridge);
        return (
            <Formik
                initialValues = {initialValues}
                enableReinitialize={true}
                onSubmit={() => null}
                validate={(values: FormValues) => {
                        const bridge = this.updateBridge(node.bridge, values);
                        onNodeUpdated(node.set("bridge", bridge));
                        return {};
                    }
                }
                render ={({handleSubmit, values}) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="materialId">Material</label>
                            <MaterialSelectField
                                name="materialId"
                                materialRegistry={materialRegistry}
                            />
                        </div>
                        <div className="form-group">
                            <span>Median Line (mm)</span>
                            <div className="row">
                                <div className="input-group">
                                    <div className="col text-center">X</div>
                                    <div className="col text-center">Y</div>
                                    <div className="col text-center">Width</div>
                                    <div className="col text-center">Height</div>
                                    <div className="col-1 text-center"></div>
                                </div>
                            </div>
                            {values.medianLine.map((point: BridgePoint, i: number) => {
                                const strIdx = i.toString();
                                return (
                                    <div key={`bridge-${i}`}className="input-group mt-1">
                                        <Field className="form-control" step={0.01} type="number" lang="en"
                                            name={`medianLine.${i}.x`}/>
                                        <Field className="form-control" step={0.01} type="number" lang="en"
                                            name={`medianLine.${i}.y`}/>
                                        <Field className="form-control" step={0.01} type="number" lang="en"
                                            name={`medianLine.${i}.width`}/>
                                        <Field className="form-control" step={0.01} type="number" lang="en"
                                            name={`medianLine.${i}.height`}/>
                                        <Field
                                            type="radio"
                                            value={strIdx}
                                            name="referencePointIdx" checked={values.referencePointIdx === strIdx}
                                        />
                                    </div>
                                );
                            })}
                        </div>
                    </Form>)
                }
            />);
    }

    private updateBridge(bridge: Bridge, { medianLine, materialId, referencePointIdx: strRefIdx}: FormValues) {
        const referencePointIdx = parseInt(strRefIdx, 10);
        medianLine = medianLine.map(
            (point, index) => {
                point.isReference = (index === referencePointIdx);
                return point;
            });
        return bridge.set(
            "medianLine", List(medianLine),
        ).set(
            "materialId", materialId,
        );

    }

    private bridgeToFormValues(bridge: Bridge): FormValues  {
        const values = {
            referencePointIdx: bridge.medianLine.findIndex(
                (point) => point.isReference,
            ).toString(),
            ...bridge.toJS(),
        };
        return values;

    }
}

export {BridgePropertyEditor};
