import {BridgeNode} from "./BridgeNode";
import {PanelNode} from "./PanelNode";
import {fromJS as PianoFromJS} from "./Piano";
import {Project} from "./Project";
import {RibNode} from "./RibNode";
import {JSONFromBlob} from "./utils";

class PianoImporter {

    public import(project: Project, blob: Blob): Promise<Project> {
        return (
            JSONFromBlob(blob).then((json) => {
                return this.projectFromJSON(project, json);
            })
        );
    }

    public projectFromJSON(project: Project, json: any): Project {
        // XXX validate the json
        const piano = PianoFromJS(json);
        let newProject = project;
        if (piano) {
            newProject = newProject.addNode(new PanelNode({panel: piano.panel}));
            if (piano.bridges) {
                for (const bridge of piano.bridges) {
                    newProject = newProject.addNode( new BridgeNode({bridge}));
                }
            }
            if (piano.ribs) {
                for (const rib of piano.ribs) {
                    newProject = newProject.addNode( new RibNode({rib}));
                }
            }
        }
        return newProject;
    }
}

export {PianoImporter};
