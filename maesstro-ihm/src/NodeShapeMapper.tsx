import * as React from "react";

import {BridgeNode} from "./BridgeNode";
import {BridgeGraphic} from "./Graphics/Bridge";
import {ImageGraphic} from "./Graphics/Image";
import {PanelGraphic} from "./Graphics/Panel";
import {RibGraphic} from "./Graphics/Rib";
import {ImageNode} from "./ImageNode";
import {PanelNode} from "./PanelNode";
import {ProjectNode} from "./ProjectNode";
import {RibNode} from "./RibNode";

interface NodeShapeMapperProps {
    node: ProjectNode;
}

class NodeShapeMapper extends React.Component<NodeShapeMapperProps> {
    public render() {
        const {node} = this.props;
        if (node instanceof ImageNode) {
            return (
                <ImageGraphic
                    image={node.image}
                    transform={node.transform}
                    opacity={0.5}
                />
            );
        } else if (node instanceof PanelNode ) {
            return (
                <PanelGraphic panel={node.panel} selected={node.selected}/>
            );
        } else if (node instanceof BridgeNode) {
            return <BridgeGraphic bridge={node.bridge} />;
        } else if (node instanceof RibNode) {
            return <RibGraphic rib={node.rib} />;

        }
    }
}

export {NodeShapeMapper};
