import {List} from "immutable";
import * as Konva from "konva";
import * as React from "react";
import {KonvaNodeProps, Layer, Stage} from "react-konva";
import Measure from "react-measure";

import {CoordinateSystemAxes} from "./CoordinateSystemAxes";
import {NodeShapeMapper} from "./NodeShapeMapper";
import {Point} from "./Point";
import {ProjectNode} from "./ProjectNode";

interface ViewportProps {
    nodes: List<ProjectNode>;
    handlers: KonvaNodeProps;
}

interface ViewportState {
    dragState: {mouseStart: Point, offsetStart: Point} | null;
    offset: Point;
    scale: number;
}

class Viewport extends React.Component<ViewportProps, ViewportState> {

    constructor(props: ViewportProps) {
        super(props);
        this.state = {
            dragState: null,
            offset: {x: 0, y: -100},
            scale: 1,
        };
    }
    public render() {
        return (
            <Measure bounds>
            {({measureRef, contentRect}) => (
                <div ref={measureRef} className="main-area__viewport">
                {contentRect.bounds &&
                    this.renderStage(contentRect.bounds)
                }
                </div>
            )}
            </Measure>
        );
    }

    private renderStage({width, height}: {width: number, height: number}) {
        const {scale, offset} = this.state;
        return (
            <Stage
                {...this.props.handlers}
                scaleX={scale}
                scaleY={-1 * scale}
                offsetX={offset.x - width / scale / 2}
                offsetY={offset.y + height / scale}
                width={width}
                height={height}
                onMouseDown={this.onMouseDown}
                onMouseMove={this.onMouseMove}
                onMouseUp={this.onMouseUp}
                onWheel={this.onScroll}
                onContentContextMenu={(e) => {e.evt.preventDefault(); }}>
                <Layer>
                    <CoordinateSystemAxes/>
                    {this.props.nodes.map((node, i) => (
                        node.selected ? null : <NodeShapeMapper node={node} key={i} />
                    ))}
                    {this.props.children}
                </Layer>
            </Stage>
            );
    }

    private onScroll = (event: Konva.KonvaEventObject<WheelEvent>) => {
        const scaleBy = 1.08;
        this.setState({
            scale:  this.state.scale * (event.evt.deltaY < 0 ?  scaleBy : 1 / scaleBy),
        });
    }

    private onMouseDown = (event: Konva.KonvaEventObject<MouseEvent>) => {
        if  (event.evt.button !== 1) {
            if  (this.props.handlers.onMouseDown) {
                this.props.handlers.onMouseDown(event);
            }
            return;
        }
        this.setState({
            dragState:  {
                mouseStart:  event.target.getStage().getPointerPosition(),
                offsetStart: this.state.offset,
            },
        });
        (event.evt.target as HTMLElement).setPointerCapture(0);
    }

    private onMouseMove = (event: Konva.KonvaEventObject<MouseEvent>) => {
        const dragState = this.state.dragState;
        if (dragState === null) {
            if  (this.props.handlers.onMouseMove) {
                this.props.handlers.onMouseMove(event);
            }
            return;
        }
        const {x,  y} = event.target.getStage().getPointerPosition();
        const dx = (x - dragState.mouseStart.x) / this.state.scale;
        const dy = (y - dragState.mouseStart.y) / this.state.scale;
        const {offsetStart} = dragState;
        const offset = {
            x:  offsetStart.x - dx,
            y: offsetStart.y + dy,
        };
        this.setState({offset});
    }

    private onMouseUp = (event: Konva.KonvaEventObject<MouseEvent>) => {
        const dragState = this.state.dragState;
        if (dragState === null) {
            if  (this.props.handlers.onMouseUp) {
                this.props.handlers.onMouseUp(event);
            }
            return;
        }
        this.setState({dragState:  null});
    }
}

export {Viewport};
