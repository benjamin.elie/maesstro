import {List, Record} from "immutable";
import {Bridge, fromJS as BridgeFromJS} from "./Bridge";
import {fromJS as PanelFromJS, Panel} from "./Panel";
import {Point} from "./Point";
import {fromJS as RibFromJS, Rib} from "./Rib";

const PianoDefaultProps = {
    panel: new Panel(),
    bridges: List<Bridge>(),
    ribs: List<Rib>(),
};

interface PianoProps {
    panel: Panel;
    bridges: List<Bridge>;
    ribs: List<Rib>;
}

class Piano extends Record<PianoProps>(PianoDefaultProps) {}

function fromJS(obj: any): Piano {
    const piano = new Piano().asMutable();
    if (obj.panel) {
        piano.set("panel", PanelFromJS(obj.panel));
    }
    if (obj.bridges) {
        const bridges = List<Bridge>(obj.bridges.map(BridgeFromJS));
        piano.set("bridges", bridges);
    }
    if (obj.ribs) {
        const ribs = List<Rib>(obj.ribs.map(RibFromJS));
        piano.set("ribs", ribs);
    }
    return piano.asImmutable();
}

export {Piano, Point, fromJS};
