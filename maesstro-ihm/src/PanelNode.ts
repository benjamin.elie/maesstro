import {Record} from "immutable";

import {Panel} from "./Panel";
import {ProjectNode} from "./ProjectNode";
import {Transform} from "./Transform";

interface PanelNodeProps {
    panel: Panel;
    transform: Transform;
    name: string;
    selected: boolean;
}

const defaultPanelNodeProps: PanelNodeProps = {
    panel: new Panel(),
    transform: new Transform(),
    name: "Panel",
    selected: false,
};

class PanelNode extends Record<PanelNodeProps>(defaultPanelNodeProps) implements ProjectNode {

    get type() {
        return "PANEL_NODE";
    }
}

export {PanelNode};
