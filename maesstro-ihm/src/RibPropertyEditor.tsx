import {Field, Form, Formik} from "formik";
import * as React from "react";

import {MaterialRegistry} from "./MaterialRegistry";
import {MaterialSelectField} from "./MaterialSelectField";
import {RibPoint} from "./Point";
import {ProjectNode} from "./ProjectNode";
import {RibNode} from "./RibNode";

interface ImagePropertyEditorProps {
    materialRegistry: MaterialRegistry;
    node: RibNode;
    onNodeUpdated: (node: ProjectNode) => void;
}

class RibPropertyEditor extends React.Component<ImagePropertyEditorProps> {
    public render() {
        const {materialRegistry, node, onNodeUpdated} = this.props;
        const initialValues = node.rib.toJS();
        return (
            <Formik
                initialValues = {initialValues}
                enableReinitialize={true}
                onSubmit={() => null}
                validate={
                    ({start, end, materialId}: {start: RibPoint, end: RibPoint, materialId: string}) => {
                        const rib = node.rib.set(
                            "start", start,
                        ).set(
                            "end", end,
                        ).set(
                            "materialId", materialId,
                        );
                        onNodeUpdated(node.set("rib", rib));
                        return {};
                    }
                }
                render ={({handleSubmit, values}) => (
                    <Form>
                        <div className="form-group">
                            <label htmlFor="materialId">Material</label>
                            <MaterialSelectField
                                name="materialId"
                                materialRegistry={materialRegistry}
                            />
                        </div>
                        <div className="form-group">
                            <span>Ends (mm)</span>
                            <div className="row">
                                <div className="input-group">
                                    <div className="col text-center">X</div>
                                    <div className="col text-center">Y</div>
                                    <div className="col text-center">Width</div>
                                    <div className="col text-center">Height</div>
                                </div>
                            </div>
                            {["start", "end"].map((ptName: string, i: number) =>
                                <div key={`rib-${i}`}className="input-group mt-1">
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`${ptName}.x`}/>
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`${ptName}.y`}/>
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`${ptName}.width`}/>
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`${ptName}.height`}/>
                                </div>)
                            }
                        </div>
                    </Form>)
                }
            />);
    }
}

export {RibPropertyEditor};
