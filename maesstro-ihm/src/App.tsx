import {List} from "immutable";
import * as React from "react";
import Card from "react-bootstrap/es/Card";

import {FileService} from "./FileService";
import {
    DEFAULT_MATERIAL_REGISTRY,
} from "./MaterialRegistry";
import {MaterialRegistry} from "./MaterialRegistry";
import {MaterialRegistryEditor} from "./MaterialRegistryEditor";
import {NodePropertyEditor} from "./NodePropertyEditor";
import {Piano} from "./Piano";
import {PianoExporter} from "./PianoExporter";
import {PianoImporter} from "./PianoImporter";
import {Project} from "./Project";
import {ProjectBrowser} from "./ProjectBrowser";
import {ProjectExporter} from "./ProjectExporter";
import {ProjectImporter} from "./ProjectImporter";
import {ProjectNode} from "./ProjectNode";
import {EditTool} from "./Tools/EditTool";
import {UndoRedoHistory} from "./UndoRedoHistory";
import {imageFromBlob} from "./utils";
import {Viewport} from "./Viewport";

interface AppState {
    piano: Piano;
    showEditor: boolean;
    saved: boolean;
    projectHistory: UndoRedoHistory<Project>;
}

class App extends React.Component<{}, AppState> {

    private fileService = new FileService();
    private projectExporter = new ProjectExporter();
    private projectImporter: ProjectImporter;
    private pianoExporter = new PianoExporter();
    private pianoImporter = new PianoImporter();

    constructor(props: {}) {
        super(props);
        this.projectImporter = new ProjectImporter(DEFAULT_MATERIAL_REGISTRY);
        const piano = new Piano();
        const nodes = List();
        const projectHistory = new UndoRedoHistory(new Project(nodes, DEFAULT_MATERIAL_REGISTRY));
        this.state = {piano, projectHistory, showEditor: false, saved: true};
    }

    public componentWillMount() {
        document.addEventListener("keydown", this.handleKeyDown);
        window.addEventListener("beforeunload", this.handleBeforeUnload);
    }

    public componentWillUnmount() {
        document.removeEventListener("keydown", this.handleKeyDown);
        document.removeEventListener("beforeunload", this.handleBeforeUnload);
    }

    public render() {
        const {nodes, selectedNode} = this.project;
        const {projectHistory, showEditor, saved} = this.state;
        return (
            <div className="app">
                <MaterialRegistryEditor
                    show={showEditor}
                    onHide={this.handleHideMaterialRegistryEditor}
                    onSave={this.handleMaterialRegistryChange}
                    materialRegistry={this.project.materialRegistry}
                    fileService={this.fileService}
                />
                <EditTool node={selectedNode} onNodeUpdated={this.handleSelectedNodeUpdated}>
                { ({viewportHandlers, viewportChildren}) => (
                    <div className="main-area">
                        <div className="main-area__left-pane pane">
                            <div className="toolbar">
                                <button
                                    onClick={this.handleProjectSelected}
                                    title="Open a project">
                                    <i className="fas fa-folder-open"></i>
                                </button>
                                <button
                                    onClick={this.handleSaveProject}
                                    title="Save the project" >
                                    <i className="fas fa-save"></i>
                                </button>
                                <button
                                    onClick={this.handlePianoSelected}
                                    title="Import a Piano file">
                                    <i className="fas fa-file-import"></i>
                                </button>
                                <button
                                    onClick={this.handleExportPiano}
                                    title="Export as Piano file">
                                    <i className="fas fa-file-export"></i>
                                </button>
                                <div className="flex-fill"></div>
                                <button
                                    disabled={!projectHistory.canUndo()}
                                    onClick={this.handleUndoClicked}
                                    title="Undo last action (Ctrl-Z)">
                                    <i className="fas fa-undo"></i>
                                </button>
                                <button
                                    disabled={!projectHistory.canRedo()}
                                    onClick={this.handleRedoClicked}
                                    title="Redo last action (Ctrl-Shift-Z)">
                                    <i className="fas fa-redo"></i>
                                </button>
                            </div>
                            <div className="panels">
                                <Card className="project-browser-panel">
                                    <Card.Header as="h5" className="d-flex align-items-center">
                                        Project {saved ? "" : "*"}
                                        <button
                                            className="btn btn-success btn-sm material-registry-btn"
                                            onClick={this.handleShowMaterialRegistryEditor}
                                            title="materials"
                                        >
                                            <i className="fas fa-tree"></i>
                                        </button>

                                    </Card.Header>
                                    <ProjectBrowser
                                        project={this.project}
                                        onProjectUpdated={this.updateProject}
                                        onImportImage={this.importImage}
                                    />
                                </Card>
                                {selectedNode &&
                                    <Card className="node-properties-panel">
                                        <Card.Header as="h5">Properties</Card.Header>
                                        <Card.Body>
                                            <NodePropertyEditor
                                                materialRegistry={this.project.materialRegistry}
                                                node={selectedNode}
                                                onNodeUpdated={this.handleSelectedNodeUpdated}
                                            />
                                        </Card.Body>
                                    </Card>
                                }
                            </div>
                        </div>
                        <Viewport
                            handlers={viewportHandlers}
                            nodes={nodes}>
                            {viewportChildren}
                        </Viewport>
                    </div>
                )}
                </EditTool>
            </div >
        );
    }

    private get project() {
        return this.state.projectHistory.currentValue;
    }

    private updateProject = (project: Project) => {
        const projectHistory = this.state.projectHistory.push(project);
        this.setState({projectHistory, saved: false});
    }

    private handleSelectedNodeUpdated = (node: ProjectNode) => {
        this.updateProject(this.project.updateSelectedNode(node));
    }

    private handleProjectSelected = () => {
        this.fileService.openFile().then((file) =>
            this.projectImporter.import(file),
        ).then((project) =>
            this.updateProject(project.deselectAll()),
        );
    }

    private handlePianoSelected = () => {
        this.fileService.openFile().then((file) =>
            this.pianoImporter.import(this.project, file),
        ).then((project) => this.updateProject(project));
    }

    private importImage = () => {
        return this.fileService.openFile().then(imageFromBlob);
    }

    private handleSaveProject = () => {
        this.projectExporter.export(this.project).then((blob) =>
            this.fileService.saveFile(blob, "PianoProject.mprj"),
        );
        this.setState({saved: true});

    }

    private handleExportPiano = () => {
        this.pianoExporter.export(this.project).then((blob) =>
            this.fileService.saveFile(blob, "piano.json"),
        );
    }

    private handleUndoClicked = () => {
        this.setState({saved: false, projectHistory: this.state.projectHistory.undo()});
    }

    private handleRedoClicked = () => {
        this.setState({saved: false, projectHistory: this.state.projectHistory.redo()});
    }

    private handleKeyDown = (event: KeyboardEvent) => {
        if (event.which === 90 && event.ctrlKey) {
            if (event.shiftKey) {
                this.setState({saved: false, projectHistory: this.state.projectHistory.redo()});
                event.preventDefault();
            } else {
                this.setState({saved: false, projectHistory: this.state.projectHistory.undo()});
                event.preventDefault();
            }
        }
    }

    private handleBeforeUnload = (event: Event) => {
        if (!this.state.saved) {
            event.returnValue = true;
        }
    }

    private handleShowMaterialRegistryEditor = () => {
        this.setState({showEditor: true});
    }

    private handleHideMaterialRegistryEditor = () => {
        this.setState({showEditor: false});
    }

    private handleMaterialRegistryChange = (materialRegistry: MaterialRegistry) => {
        this.updateProject(this.project.withMaterialRegistry(materialRegistry));
        this.setState({showEditor: false});
    }
}

export {App};
