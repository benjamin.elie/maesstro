import * as React from "react";
import {Image} from "react-konva";
import {Transform} from "../Transform";

interface ImageGraphicProps {
    image: HTMLImageElement;
    transform: Transform;
    opacity: number;
}

class ImageGraphic extends React.Component<ImageGraphicProps> {

    public render() {
        const {position, rotation, scale} = this.props.transform;
        return (
            <Image
                {...position}
                rotation={rotation}
                offsetY={this.props.image.height}
                scale={{x: scale, y: -scale}}
                image={this.props.image}
                opacity={0.5}
            />
        );
    }
}

export {ImageGraphic};
