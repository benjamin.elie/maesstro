import * as React from "react";
import {Circle, Group, Line} from "react-konva";

import {Bridge} from "../Bridge";
import {bridgeColor} from "../Theme";
import {pointsToOpenLine} from "./utils";

interface BridgeGraphicProps {
    bridge: Bridge;
}

class BridgeGraphic extends React.Component<BridgeGraphicProps> {

    public render() {
        const {bridge} = this.props;
        const line = pointsToOpenLine(bridge.medianLine);
        return (
            <Group>
                <Line
                    x={0}
                    y={0}
                    points={line}
                    stroke={bridgeColor}
                />
                {
                    this.props.bridge.medianLine.filter(
                        (point) => point.isReference,
                    ).map(
                    (point, idx) =>
                        <Circle
                            key={idx}
                            x={point.x}
                            y={point.y}
                            radius={10}
                            fill={"red"}
                        />,
                    )
                }
            </Group>
        );
    }
}

export {BridgeGraphic};
