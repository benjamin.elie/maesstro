import * as React from "react";
import {Line} from "react-konva";

import {Panel} from "../Panel";
import {panelColor} from "../Theme";
import {pointsToClosedLine} from "./utils";

interface PanelGraphicProps {
    selected: boolean;
    panel: Panel;
}

class PanelGraphic extends React.Component<PanelGraphicProps> {

    public render() {
        const {panel} = this.props;
        const line = pointsToClosedLine(panel.contour);
        return (
            <Line
                x={0}
                y={0}
                points={line}
                stroke={panelColor} />
        );
    }
}

export {PanelGraphic};
