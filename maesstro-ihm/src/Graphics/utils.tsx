import {List} from "immutable";
import {Point} from "../Point";

function pointsToOpenLine(points: List<Point>): number[] {
    if (points.size === 0) {
        return [];
    }
    const line = points.reduce((l: number[], p: Point) => {
        return l.concat([p.x, p.y]);
    }, [],
    );
    return line;
}

function pointsToClosedLine(points: List<Point>): number[] {
    const firstPoint = points.get(0);
    if (firstPoint == null) {
        return [];
    }
    const openLine = pointsToOpenLine(points);
    return openLine.concat([firstPoint.x, firstPoint.y]);
}

export {pointsToOpenLine, pointsToClosedLine};
