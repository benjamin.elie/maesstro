import {List} from "immutable";
import * as React from "react";
import {Line} from "react-konva";

import {Rib} from "../Rib";
import {ribColor} from "../Theme";
import {pointsToOpenLine} from "./utils";

interface RibGraphicProps {
    rib: Rib;
}

class RibGraphic extends React.Component<RibGraphicProps> {

    public render() {
        const {start, end} = this.props.rib;
        const line = pointsToOpenLine(List([start, end]));
        return (
            <Line
                x={0}
                y={0}
                strokeWidth={3}
                points={line}
                stroke={ribColor} />
        );
    }
}

export {RibGraphic};
