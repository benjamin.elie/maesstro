import * as React from "react";
import {Arc, Line} from "react-konva";

import {Point} from "../Point";

interface AngleProps {
    origin: Point;
    angle: number;
}

export class AngleGraphic extends React.Component<AngleProps> {
    public render() {
        const {angle, origin} = this.props;

        const angleRad = angle * Math.PI / 180;
        const lineLength = 1000; // 1m
        const vector = {
            x: Math.cos(angleRad) * lineLength,
            y: Math.sin(angleRad) * lineLength,
        };
        return (
            <>
                <Arc
                    outerRadius={300}
                    innerRadius={0}
                    fill={"rgba(111, 185, 71, 0.15)"}
                    angle={angle}
                    x={origin.x}
                    y={origin.y}
                />
                <Line
                    x={origin.x}
                    y={origin.y}
                    stroke="rgba(255, 0, 0, 0.5)"
                    points={
                        [
                            -vector.x,
                            -vector.y,
                            vector.x,
                            vector.y,
                        ]
                    }
                />
            </>
        );
    }
}
