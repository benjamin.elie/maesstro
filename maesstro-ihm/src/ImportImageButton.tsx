import * as React from "react";
import {FileService} from "./FileService";

interface ImportImageButtonProps {
    onImageImported: (file: File) => void;
}

class ImportImageButton extends React.Component<ImportImageButtonProps> {
    public render() {
        return (
            <button onClick={this.handleClick}>Import Image</button>
        );
    }

    private handleClick = () => {
        const fileService = new FileService();
        fileService.openFile().then(this.props.onImageImported);
    }

}

export {ImportImageButton};
