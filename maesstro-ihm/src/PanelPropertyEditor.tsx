import {Field, Form, Formik} from "formik";
import {List} from "immutable";
import * as React from "react";

import {MaterialRegistry} from "./MaterialRegistry";
import {MaterialSelectField} from "./MaterialSelectField";
import {PanelNode} from "./PanelNode";
import {ThickPoint} from "./Point";
import {ProjectNode} from "./ProjectNode";

interface ImagePropertyEditorProps {
    materialRegistry: MaterialRegistry;
    node: PanelNode;
    onNodeUpdated: (node: ProjectNode) => void;
}

class PanelPropertyEditor extends React.Component<ImagePropertyEditorProps> {
    public render() {
        const {materialRegistry, node, onNodeUpdated} = this.props;
        const panel = node.panel;
        const initialValues = panel.toJS();
        return (
            <Formik
                initialValues = {initialValues}
                enableReinitialize={true}
                onSubmit={() => null}
                validate={
                    ({contour, orthotropicAngleDeg, materialId}:
                        {
                            contour: ThickPoint[],
                            orthotropicAngleDeg: number,
                            materialId: string,
                        },
                    ) => {
                        const updatedPanel = node.panel.set(
                            "contour", List(contour),
                        ).set(
                            "orthotropicAngleDeg", orthotropicAngleDeg,
                        ).set(
                            "materialId", materialId,
                        );
                        onNodeUpdated(node.set("panel", updatedPanel));
                        return {};
                    }
                }
                render ={({handleSubmit, values}) => (
                    <Form>
                        <div className="form-group">
                            <div className="form-group">
                                <label htmlFor="orthotropicAngleDeg">Angle of fiber orientation (°)</label>
                                <Field
                                    className="form-control"
                                    step={0.01} type="number"
                                    name="orthotropicAngleDeg"
                                />
                            </div>
                            <div className="form-group">
                                <label htmlFor="materialId">Material</label>
                                <MaterialSelectField
                                    name="materialId"
                                    materialRegistry={materialRegistry}
                                />
                            </div>
                            <span>Contour (mm)</span>
                            <div className="d-flex">
                                <div className="text-center flex-grow-1">X</div>
                                <div className="text-center flex-grow-1">Y</div>
                                <div className="text-center flex-grow-1">Thickness</div>
                            </div>
                            {values.contour.map((point: ThickPoint, i: number) =>
                                <div key={`panel-${i}`}className="input-group mt-1">
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`contour.${i}.x`}/>
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`contour.${i}.y`}/>
                                    <Field className="form-control" step={0.01} type="number" lang="en"
                                        name={`contour.${i}.thickness`}/>
                                </div>)
                            }
                        </div>
                    </Form>)
                }
            />);
    }
}

export {PanelPropertyEditor};
