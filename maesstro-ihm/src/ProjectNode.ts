import {Record} from "immutable";

interface ProjectNode extends Record<any> {
    readonly type: string;
    selected: boolean;
    name: string;
}

export {ProjectNode};
