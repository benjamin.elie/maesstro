import {Record} from "immutable";
import {RibPoint} from "./Point";

interface RibProps {
    start: RibPoint;
    end: RibPoint;
    materialId: string;
}

const RibDefaultProps = {
    start: {x: 0, y: 0, width: 20, height: 20},
    end: {x: 1000, y: 0, width: 20, height: 20},
    materialId: "Norway Spruce 1",
};

class Rib extends Record<RibProps>(RibDefaultProps) {}

function fromJS(obj: any): Rib {
    const rib = new Rib().asMutable();
    if (obj.start) {
        rib.set("start", obj.start);
    }
    if (obj.end) {
        rib.set("end", obj.end);
    }
    if (obj.materialId != null) {
        rib.set("materialId", obj.materialId);
    }
    return rib.asImmutable();
}

export {Rib, fromJS};
