# MAESSTRO software

## Presentation

MAESSTRO is a software for sound synthesis of piano tones based on a complete vibroacoustic modeling of the piano. The main goal is to provide piano makers a set of tools that allow them to predict the acoustic impacts of structural modifications of piano soundboards on the resulting sounds. The interest of the physical modeling that drives the numerical computations embedded in the software is to be able to reproduce the vibroacoustics of the piano in a realistic fashion: the acoustic variations between tones produced by different pianos faithfully reflect their structural differences. For that purpose, MAESSTRO gathers and coordinates several numerical and physical models that numerically simulate all of the physical phenomena involved in the production of piano tones, from the hammer impulsion upto the acoustic radiation.

MAESSTRO includes several functionalities which are the following
* entering the geometry and the materials of the virtual soundboard thanks to a Graphical User Interface (GUI), 
* feeding MAESSTRO with MIDI files to be synthesized,
* simulating numerically the physical phenomena involved in the production of piano tones, 
* post-processing the software outputs, 
* creating audio files of synthesized piano tones.

The package contains three subpackages :
* maesstro-ihm contains the source codes of the GUI, implemented in TypeScript
* maesstro-matlab is a Matlab toolbox that contains the source codes for the computation of the modal basis of the soundboard, including an automatic mesh generator using GMSH. Matlab is only needed to modify the source files and generate updated executables. In order to launch the executables, only Matlab Runtime is needed
* maesstro-python contains the python modules which is used to call the different modules via a command line interface

## Installation
Step 1. Get the source codes by cloning this repo with git
```
git clone https://gitlab.com/benjamin.elie/maesstro.git

```
or by downloading its content.

Step 2. To install the maesstro python module via pip

```
python3 -m pip install path_to_maesstro/python-maesstro -e
```

'path_to_python-maesstro' is the path to the folder containing the installation file 'setup.py'


Step 3. If you want the Matlab toolbox to be automatically added tou your MATLAB path when you launch MATLAB, just add the following line in your `pathdef.m` file (usually in `$MATLABROOT/toolbox/local/`)
```
'/path_to_maesstro/matlab-maesstro:', ...

```
Add this line somewhere between `%%% BEGIN ENTRIES %%%` and `%%% END ENTRIES %%%`. If you want MAESSTRO to be in your MATLAB workspace only when asked to, add the folloing line to your scripts

```
addpath(genpath('/path_to_maesstro/matlab-maesstro'));
```

* WARNING: Please, do not modify anything in the maesstro folder, including demo files, unless you want to modify the repository.

## Dependencies

MAESSTRO may require to use external libraries that should be installed independently. These dependencies include:
* [Montjoie](http://montjoie.gforge.inria.fr/), a C++ library used to compute the modal basis of the soundboard using finite-element models
* [GMSH](https://gmsh.info/), a 2-D and 3-D free mesh generator

Visit the webpages to know how to install these libraries. 

## Matlab Runtime

Some modules need to execute compiled Matlab codes using the Matlab Runtime Compiler (MRC), which is freely available. The version that is used is 9.4 (R2018a), that you can download at :
```
https://fr.mathworks.com/products/compiler/matlab-runtime.html
```

Once you downloaded the right version of MRC, unzip the file and move the files in a temporary folder. Then, from a terminal, inside the temp folder, execute the following line to launch the installation in silent mode
```
./install -mode silent -agreeToLicense yes
```

It is necessary to change the environment variable 'LD_LIBRARY_PATH' so that the proper MATLAB libraries are used when the codes are launched. For that purpose, you should add the following lines to your .bashrc file:

```
export MCR_PATH=/usr/local/MATLAB/MATLAB_Runtime/v94
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$MCR_PATH/runtime/glnxa64::$MCR_PATH/bin/glnxa64::$MCR_PATH/sys/os/glnxa64:
```

In order to compile the MATLAB codes using the MATLAB Runtime Compiler, you can simply execute the file 'compile_module.m' located at the root of the maesstro-matlab folder. The executable files are then stored in the folder named 'matlab-builds', which will be created if it does not exist yet.

You should also configure the path to the different sources by editing the file named 'config.ini', which is located in '/home/user/.config/maesstro/'. If you do not find this file, you can create it using this command line in a terminal
```
maesstro init-config
```

Several paths are required:
* 'modes-table': path to the MATLAB executable file that computes the modal basis of the soundboard
* 'mouvement-table': path to the MATLAB executable file that computes the motion of the soundboard
* 'rayonnement': path to the MATLAB executable file that computes the azcoustic radiation of the soundboard
* 'runtime-path': path to the MRC compiler
* 'path': path to the Montjoie library

Usually, the path to the MATLAB executables files look like the following:
```
$MATLAB_EXE/matlab-builds/<name_of_the_module_linux>,
```
with '$MATLAB_EXE' being the path to the 'matlab-builds' folder

## Using MAESSTRO in command line

The general syntax if MAESSTRO in command line is
```
maesstro -option1 command -option2 <name_of_the_module> <name_of_the_ini_file.ini>
```

The options are the following:
* 'option1' is empty or is either '-version' or '-help', independently of the following command,
* 'command' is the name of the command, to be chosen among:
1. 'init-config' creates a text file that contains the path to the computations codes, the MATLAB and Montjoie executable files, and to the MRC library
2. 'new' creates the file 'piano.ini' that contains the path to the input data files
3. 'run' launches the module specified by '-option2' and '<name_of_the_module>'
* 'option2' is empty of is either '-help' or:
1. '-m' to launch a specific module (defined by '<name_of_the_module>')
2. '-f' to launch a sequence of modules from a specific module (defined by '<name_of_the_module>'), up to the 'rayonnement' module
* '<name_of_the_module>' is the name of the module (only works with 'run -m' or 'run -f'), to be chosen among:
1. 'modes-table', to launch the computation of the soundboard's modal basis
2. 'marteau-corde', to launch the computation of the string dynamics
3. 'mouvement-table', to launch the computation of the modal coordinates of the soundboard as a function of time
4. 'rayonnement', to launch the commputation of the acoustic radiation
* '<name_of_the_ini_file.ini>' is the name of the configuration file .ini, which contains the path to input data files.

## Using MAESSTRO online

The GUI is available [here](https://maesstro.demo.logilab.fr/) in any web browser. You can download a user's guide (in French) [here](https://www.maesstro.cnrs.fr/wp-content/uploads/2019/12/MAESSTRO_Mode_d_emploi.pdf)

Some simulations may take a large amount of memory. You can run MAESSTRO online, via the [Simulagora platform](https://www.simulagora.com/).
Ask benjamin.cotte@ensta-paris.fr to create your account.


## Updates

MAESSTRO is in its development stage, so that you may expect regular available updates. Simply run the following command in the maesstro folder
```
git pull origin master
```

## Contact

For questions and remarks, please contact me at bnjmn.elie (at) gmail.com

## Papers to cite

If you publish results based on MAESSTRO and if you :
* used the finite-element computation of the soundboard modal basis, please cite [1] and [2],
* used the semi-analytical model for the computation of the modal basis, please cite [3],

In any case, please cite the original paper describing the software [4]

## References:
[1] J. Chabassier, A. Chaigne, P. Joly, Modeling and simulation of a grand piano, The Journal of the Acoustical Society of America 134(1), pp. 648-655, 2013

[2] C. Geuzaine and J.-F. Remacle. Gmsh: a three-dimensional finite element mesh generator with built-in pre- and post-processing facilities. International Journal for Numerical Methods in Engineering 79(11), pp. 1309-1331, 2009

[3] B. Trévisan, K. Ege, B. Laulagnet, A modal approach to piano soundboard vibroacoustic behavior, Journal of the Acoustical Society of America 141(2), pp. 690–709, 2017

[4] B. Elie, X. Boutillon, J. Chabassier, K. Ege, B. Laulagnet, B. Trévisan, B. Cotté, N. Chauvat, MAESSTRO: A sound synthesis framework for Computer-Aided Design of piano soundboards, in ISMA 2019, Detmold, Germany, 2019.

Please visit the website of the MAESSTRO project for further information
```
https://www.maesstro.cnrs.fr/
https://www.maesstro.cnrs.fr/cao-sonore/
```

