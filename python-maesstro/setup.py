"""Maesstro setup.py"""

from setuptools import setup, find_packages
from os import path

here = path.abspath(path.dirname(__file__))

setup(
    name='maesstro',
    version='1.0.0',
    description='MAESSTRO run chain',
    author='Logilab SA',
    author_email='bnjmn.elie@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    packages=find_packages(exclude=['contrib', 'docs', 'tests']),
    python_requires='>= 3.5',
    include_package_data=True,
    install_requires=[
        'click',
        'jsonschema',
        'h5py',
        'pandas>=0.20.0',
        'xdg',
        'xlrd',
        'mido',
        'sklearn',
    ],
    extras_require={
    },
    package_data={
    },
    entry_points={
        'console_scripts': [
            'maesstro=maesstro.cli:cli',
        ],
    },
)
