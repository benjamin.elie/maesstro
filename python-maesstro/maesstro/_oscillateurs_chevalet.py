"""
Calcul des oscillateurs représentant la mobilité au chevalet vue par la corde

 fichiers en entree :
 - un fichier HDF5 fichier_modes avec les données relatives aux N modes
 calculés et conservés dans la bande de fréquences utile: masses mn,
 raideurs kn, amortissements Rn et déformées modales P stockées sous la
 forme des coefficients dans la base des modes de la plaque étendue avec
 appui simple (fonctions sinus)
 - coordonnées des points de couplage le long du chevalet : fichier_couplage
 - un fichier .json avec les paramètres de calcul

 fichiers en sortie :
 - fichier HDF5 SORTIES_OSCILLATEURS_CHEVALET.h5 avec les déformées modales
 au niveau des Nc points de couplage (matrice de Nm x Nc réels)
 - 4 fichiers texte correspondant aux masses modales, amortissements modaux,
 raideurs modales et amplitudes modales (déformées modales au point de
 couplage pour le calcul de corde
"""
import h5py
import numpy as np
import pandas
from pathlib import Path
from sklearn.neighbors import NearestNeighbors
from scipy import interpolate

from . import (
    utils
)

from ._soundboard import Soundboard
from .jsontools import readjson


def adjust_coupling_positions(xe: np.ndarray,
                              ye: np.ndarray,
                              geometryFile: str) -> np.ndarray:

    SB = readjson(geometryFile)
    xmin = min(SB.x_normalized)
    ymin = min(SB.y_normalized)
    xref, yref = SB.ref_point[0]

    aX = xref - xe[-1]
    aY = yref - ye[-1]

    xe = xe + aX
    ye = ye + aY

    xint = np.zeros(0)
    yint = np.zeros(0)
    idxbr = np.zeros(0)

    for kbr in range(len(SB.bridge)):
        xbr = SB.bridge[kbr].x_normalized
        ybr = SB.bridge[kbr].y_normalized

        xtmp = np.linspace(min(xbr), max(xbr), 10000)
        f = interpolate.interp1d(xbr, ybr)
        ytmp = f(xtmp)

        xint = np.concatenate((xint.reshape(-1), xtmp.reshape(-1)))
        yint = np.concatenate((yint.reshape(-1), ytmp.reshape(-1)))
        idxbr = np.concatenate((idxbr.reshape(-1),
                                kbr * np.ones(len(xtmp)).reshape(-1)))

    neigh = NearestNeighbors(n_neighbors=1)

    Y = [[xe[k], ye[k]] for k in range(len(xe))]
    X = [[xint[k], yint[k]] for k in range(len(xint))]

    neigh.fit(X)
    dist, IdxTmp = neigh.kneighbors(Y)
    IdxTmp = np.sort(IdxTmp.squeeze())
    xc_adjust = np.array([X[k][0] for k in IdxTmp])-xmin
    yc_adjust = np.array([X[k][1] for k in IdxTmp])-ymin
    idxbrpt = np.array([int(idxbr[k]) for k in IdxTmp])

    return xc_adjust, yc_adjust, idxbrpt


def extended_coupling_positions(xe: np.ndarray,
                                ye: np.ndarray,
                                idx: np.ndarray,
                                geometryFile: str) -> np.ndarray:

    SB = readjson(geometryFile)
    SBext = readjson(geometryFile.replace('.json', '_simple.json'))

    ycext = np.zeros(0)
    xcext = np.zeros(0)

    for k in range(len(SB.bridge)):

        sxc = []
        sxbrc = []
        xtmp = [xe[n] for n in range(len(idx)) if idx[n] == k]
        ytmp = [ye[n] for n in range(len(idx)) if idx[n] == k]

        xbr = SB.bridge[k].x_normalized
        ybr = SB.bridge[k].y_normalized
        sxbrc.append(xbr[0])
        for kx in range(1, len(xbr)):
            sxbrc.append(sxbrc[kx - 1] + np.sqrt((xbr[kx] - xbr[kx - 1])**2 +
                         (ybr[kx] - ybr[kx - 1])**2))

        oxbr = min(sxbrc)
        maxbr = max(sxbrc)
        lbr = maxbr - oxbr

        sxc.append(xtmp[0])
        for kx in range(1, len(xtmp)):
            sxc.append(sxc[kx - 1] + np.sqrt((xtmp[kx] - xtmp[kx - 1])**2 +
                       (ytmp[kx] - ytmp[kx - 1])**2))

        sxo = np.array(sxc) - oxbr
        sxoadim = sxo / lbr

        oxbrext = min(SBext.bridge[k].x_normalized)
        maxbrext = max(SBext.bridge[k].x_normalized)
        xcext = np.concatenate((xcext.reshape(-1),
                                sxoadim.reshape(-1) * (maxbrext - oxbrext) +
                                oxbrext))
        ycext = np.concatenate((ycext.reshape(-1),
                                np.mean(SBext.bridge[k].y_normalized) *
                                np.ones(len(sxc))))

    return xcext, ycext


def coupling_positions(fpath: str) -> np.ndarray:
    """Read coupling positions."""

    df = pandas.read_excel(fpath.as_posix(), index_col=1, skiprows=[1],
                           dtypes={'Nc': 'int'})
    xe = np.array(df.x_c)
    ye = np.array(df.y_c)
    return xe, ye


def modal_basis(nbr_p: int, nbr_q: int, l: float, L: float,
                xe: np.ndarray, ye: np.ndarray) -> np.ndarray:
    """creation de la base modale au niveau des points de couplage (xe, ye)"""
    basis = np.zeros((nbr_p * nbr_q, len(xe)))
    cpt = 0
    for q in range(1, nbr_q + 1):
        for p in range(1, nbr_p + 1):
            basis[cpt, :] = \
                np.sin(p * np.pi * xe / l) * np.sin(q * np.pi * ye / L)
            cpt = cpt + 1
    return basis


def write_results(modes: np.ndarray, npoints: int,
                  mn: np.ndarray, kn: np.ndarray, Rn: np.ndarray,
                  P: np.ndarray, xe: np.ndarray, ye: np.ndarray) -> None:
    """Write data to text files alongside MONTJOIE input files."""
    shapes_bridge = np.matmul(P, modes)

    montjoie_basedir = utils.montjoie_workspace()
    # generer les fichiers d'entree pour le code Montjoie
    # parametres des oscillateurs au point de couplage au chevalet de la note
    # consideree
    xy_pts = np.concatenate((xe.reshape(-1, 1), ye.reshape(-1, 1)), axis=1)
    np.savetxt((montjoie_basedir / 'Mass.txt').as_posix(),
               np.transpose(mn), fmt='%.4e')
    np.savetxt((montjoie_basedir / 'Stiffness.txt').as_posix(),
               np.transpose(kn), fmt='%.4e')
    np.savetxt((montjoie_basedir / 'Damping.txt').as_posix(),
               np.transpose(Rn), fmt='%.4e')
    np.savetxt((montjoie_basedir / 'Coupling.txt').as_posix(),
               xy_pts, fmt='%.4e')
    for ii in range(npoints):
        An = shapes_bridge[:, ii]  # modal amplitude for one bridge location
        np.savetxt(
            (utils.montjoie_workspace(ii + 1) / 'Amplitude.txt').as_posix(),
            np.transpose(An), fmt='%.4e',
        )


def run(h5_modes_tables: str,
        solver: str,
        couplage: str,
        geometryFile: str) -> None:
    xe, ye = coupling_positions(Path(couplage))
    xe, ye, idx = adjust_coupling_positions(xe, ye, geometryFile)

    if solver == 'lva':
        xe, ye = extended_coupling_positions(xe, ye, idx, geometryFile)
    npoints = len(xe)

    with h5py.File(h5_modes_tables, 'r') as f:
        mn = f['masses_modales']
        kn = f['raideurs_modales']
        Rn = f['amortissements_modaux']
        P = f['coefficients_deformees']
        nbr_p, nbr_q = f['basis_dimension']
        lx, ly = f['soundboard_dimension']
        modes = modal_basis(int(nbr_p), int(nbr_q), lx, ly, xe, ye)
        write_results(modes, npoints, mn, kn, Rn, P, xe, ye)
