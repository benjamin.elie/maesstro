#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 17:47:45 2019

@author: benjamin
"""
import json
import numpy as np
from ._soundboard import Soundboard, Superstructure


def polyarea(x, y):
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))


def readjson(fname):
    # read json files of normalized data

    with open(fname, 'r') as myfile:
        jstr = myfile.read().replace('\n', '')

    idjson = json.loads(jstr)
    sb = idjson['panel']
    rb = idjson['ribs']
    brg = idjson['bridges']

    sb = idjson['panel']
    contour = sb['contour']
    xsb = [contour[x]['x'] / 1000 for x in range(len(contour))]
    ysb = [contour[x]['y'] / 1000 for x in range(len(contour))]
    thck = [contour[x]['thickness'] / 1000 for x in range(len(contour))]

    if (xsb[0] != xsb[-1]) or (ysb[0] != ysb[-1]):
        xsb = np.append(xsb, contour[0]['x'] / 1000)
        ysb = np.append(ysb, contour[0]['y'] / 1000)

    meanThickness = np.mean(thck)
    SB = Soundboard()
    SB.x_normalized = np.array(xsb)
    SB.y_normalized = np.array(ysb)
    SB.area = polyarea(xsb, ysb)
    SB.material = sb['materialId']
    SB.thickness = np.array(thck)
    SB.orthotropy_angle = sb['orthotropicAngleDeg']

    rb = idjson['ribs']
    Rcell = []
    for k in range(len(rb)):
        rbtmp = rb[k]
        xvec = np.array([rbtmp['start']['x'], rbtmp['end']['x']]) / 1000
        yvec = np.array([rbtmp['start']['y'], rbtmp['end']['y']]) / 1000
        thrib = np.array([rbtmp['start']['height'],
                          rbtmp['end']['height']]) / 1000
        wrib = np.array([rbtmp['start']['width'],
                         rbtmp['end']['width']]) / 1000
        or_angle = np.arctan(np.diff(yvec) / np.diff(xvec)) * 180 / np.pi

        if 'intermediatePoints' in rbtmp:
            varTmp = rbtmp['intermediatePoints']
            nVar = range(len(varTmp))
            posint = np.array([varTmp[x]['position'] for x in nVar])
            xint = xvec[0] + posint * abs(np.diff(xvec))
            wint = np.array([varTmp[x]['width'] / 1000 for x in nVar])
            thint = np.array([varTmp[x]['height'] / 1000 for x in nVar])

            slope = (yvec[-1] - yvec[0]) / (xvec[-1] - xvec[0])
            intercept = yvec[0] - slope * xvec[0]
            yint = slope * xint + intercept

            xvecribs = xint.tolist()
            xvecribs.append(xvec[-1])
            xvecribs.insert(0, xvec[0])
            xvecribs = np.array(xvecribs)

            yvecribs = yint.tolist()
            yvecribs.append(yvec[-1])
            yvecribs.insert(0, yvec[0])
            yvecribs = np.array(yvecribs)

            wribtmp = wint.tolist()
            wribtmp.append(wrib[-1])
            wribtmp.insert(0, wrib[0])
            wribtmp = np.array(wribtmp)

            thribtmp = thint.tolist()
            thribtmp.append(thrib[-1])
            thribtmp.insert(0, thrib[0])
            thribtmp = np.array(thribtmp)

            isx = np.argsort(xvecribs)
            xvec = xvecribs[isx]
            yvec = yvecribs[isx]
            wrib = wribtmp[isx]
            thrib = thribtmp[isx]

        for k1 in range(len(xvec) - 1):
            rr = Superstructure()
            rr.x_normalized = xvec[k1:k1 + 2]
            rr.y_normalized = yvec[k1:k1 + 2]
            rr.deport = meanThickness / 2
            rr.thickness = thrib[k1:k1 + 1]
            rr.width = wrib[k1:k1 + 1]
            rr.material = rbtmp['materialId']
            rr.number = 1
            rr.obj_angle = or_angle
            Rcell.append(rr)

    Bcell = []
    brg = idjson['bridges']
    for k in range(len(brg)):
        brtmp = brg[k]['medianLine']
        xsb = [brtmp[x]['x'] / 1000 for x in range(len(brtmp))]
        ysb = [brtmp[x]['y'] / 1000 for x in range(len(brtmp))]
        thck = [brtmp[x]['height'] / 1000 for x in range(len(brtmp))]
        wrr = [brtmp[x]['width'] / 1000 for x in range(len(brtmp))]
        ref = [brtmp[x]['isReference'] for x in range(len(brtmp))]
        xref = [xsb[x] for x in range(len(xsb)) if ref[x]]
        yref = [ysb[x] for x in range(len(xsb)) if ref[x]]
        if len(xref) > 0:
            SB.ref_point.append([xref[0], yref[0]])
        angleInRad = np.arctan((ysb[-1] - ysb[0]) / (xsb[-1] - xsb[0]))
        or_angle = angleInRad * 180 / np.pi

        brr = Superstructure()
        brr.x_normalized = np.array(xsb)
        brr.y_normalized = np.array(ysb)
        brr.material = brg[k]['materialId']
        brr.width = np.array(wrr)
        brr.thickness = np.array(thck)
        brr.deport = meanThickness / 2
        brr.number = len(xsb) - 1
        brr.obj_angle = or_angle
        Bcell.append(brr)

    SB.ribs = Rcell
    SB.bridge = Bcell

    return SB


def readaudiojson(fname):
    with open(fname, 'r') as myfile:
        jstr = myfile.read().replace('\n', '')

    idjson = json.loads(jstr)
    notes = idjson['notes']
    numNotes = len(notes)
    idxNote = []
    startNote = []
    endNote = []
    for k in range(numNotes):
        idxNote.append(notes[k]['index'])
        startNote.append(notes[k]['start_time'])
        endNote.append(notes[k]['stop_time'])

    return idxNote, startNote, endNote
