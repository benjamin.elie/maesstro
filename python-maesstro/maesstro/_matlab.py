from configparser import ConfigParser
from pathlib import Path
import subprocess
from typing import Any

from .utils import LOGGER


def run(script_path: Path, runtime_path: Path, montjoie_lib: Path,
        *args: Any) -> subprocess.CompletedProcess:
    """Run a standalone MATLAB program from `script_path` (a .sh file under
    Linux) using MATLAB Runtime path specified as `runtime_path` argument.
    Extra arguments are passed to the script.
    """
    script_path = Path(script_path)
    if not script_path.exists():
        raise OSError('MATLAB script {} not found'.format(script_path))
    runtime_path = Path(runtime_path)
    montjoie_lib = Path(montjoie_lib / Path('MONTJOIE') / Path('lib'))
    if not runtime_path.is_dir():
        raise OSError('MATLAB Runtime path {} should be a directory'.format(
            runtime_path))
    strtmp = [str(script_path), str(montjoie_lib) + ':' + str(runtime_path)]
    cmd = strtmp + list(args)
    LOGGER.info('starting a subprocess with command: %s',
                ' '.join(cmd))
    return subprocess.run(cmd, check=True)


def runtime_path(global_config: ConfigParser) -> Path:
    return Path(global_config['matlab']['runtime-path'])


def montjoie_lib_path(global_config: ConfigParser) -> Path:
    return Path(global_config['montjoie']['path'])


def script_path(basepath: Path, name: str) -> Path:
    script_path = Path(basepath) / 'for_redistribution_files_only' / name
    if not script_path.exists():
        raise OSError('{} not found'.format(script_path))
    return script_path
