import abc
from configparser import ConfigParser, SectionProxy
from pathlib import Path
from typing import (  # noqa: F401
    List,
    Tuple,
    Union,
)

import h5py

from . import (
    _io,
    _matlab,
)


def script_name(module_name: str) -> str:
    return 'run_' + module_name.upper().replace('-', '_') + '.sh'


def script_path(global_config: ConfigParser, module_name: str) -> Path:
    basepath = Path(global_config['lva'][module_name])
    return _matlab.script_path(basepath, script_name(module_name))


def check_output(workspace: Path, module_name: str) -> Tuple[str, ...]:
    expected = _io.OUTPUTS[module_name]  # type: Tuple[str, ...]
    missing = [fname for fname in expected
               if not (workspace / fname).exists()]
    if missing:
        raise OSError('output file(s) {} not found in {}'.format(
            ', '.join(missing), str(workspace))
        )
    return expected


class _MatlabRunner(metaclass=abc.ABCMeta):
    """Abstract base class for MATLAB executable runner."""

    @abc.abstractproperty
    def module_name(self) -> str:
        """Name of MATLAB executable."""

    @abc.abstractmethod
    def inputs(self) -> List[Path]:
        """Sequence input arguments for the MATLAB executable."""

    def __init__(self, global_config: ConfigParser,
                 settings: SectionProxy) -> None:
        self.config = global_config
        self.settings = settings

    def _input_for(self, option: str) -> Path:
        fpath = self.settings[option]
        return Path(fpath).absolute()

    def run_matlab(self) -> Tuple[str, ...]:
        script = script_path(self.config, self.module_name)
        workspace = Path.cwd()
        args = []
        for fpath in self.inputs():
            if not fpath.exists():
                raise OSError('input file {} not found'.format(fpath))
            args.append(fpath.as_posix())
        matlab_runtime = _matlab.runtime_path(self.config)
        montjoie_lib = _matlab.montjoie_lib_path(self.config)
        _matlab.run(script, matlab_runtime, montjoie_lib, *args)
        return check_output(workspace, self.module_name)

    def run_module(self) -> h5py.File:
        output_files = self.run_matlab()
        h5files = [fname for fname in output_files if fname.endswith('.h5')]
        assert len(h5files) == 1, 'expecting exactly one .h5 output file'
        return h5py.File(h5files[0])


class ModesTableRunner(_MatlabRunner):
    module_name = 'modes-table'

    def inputs(self) -> List[Path]:
        return [
            self._input_for(option)
            for option in (
                'geometry',
                'parameters',
                'material'
            )
        ]


class MouvementTableRunner(_MatlabRunner):
    module_name = 'mouvement-table'

    def inputs(self) -> List[Path]:
        return [
            Path(_io.OUTPUTS['modes-table'][0]).absolute(),
            Path(_io.OUTPUTS['marteau-corde'][0]).absolute(),
        ]


class RayonnementRunner(_MatlabRunner):
    module_name = 'rayonnement'

    def inputs(self) -> List[Path]:
        return [
            self._input_for('parameters'),
            Path(_io.OUTPUTS['modes-table'][0]).absolute(),
            Path(_io.OUTPUTS['mouvement-table'][0]).absolute(),
        ]
