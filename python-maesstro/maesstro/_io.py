from typing import Dict, Tuple  # noqa: F401


OUTPUTS = {
    'modes-table': (
        'SORTIES_MODES_TABLE.h5',
    ),
    'marteau-corde': (
        'SORTIES_CORDE.h5',
    ),
    'mouvement-table': (
        'SORTIES_MOUVEMENT_TABLE.h5',
    ),
    'rayonnement': (
        'SORTIES_RAYONNEMENT.h5',
        'synthese_sonore.wav',
    ),
}  # type: Dict[str, Tuple[str, ...]]
