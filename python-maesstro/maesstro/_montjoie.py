from configparser import ConfigParser, SectionProxy
import json
import math
import os
from pathlib import Path
import shutil
import subprocess
from typing import (
    Any,
    Dict,
    IO,
    Text,
)
import warnings

import h5py
import numpy as np
import pandas
import time

from . import (
    _config,
    _io,
    _oscillateurs_chevalet,
    utils,
)

from ._midi import midi2json
from .inharmoCoeff import CallInharmoCoeff
from ._pianotone import mufflingtone

NOTE_ROW_LABEL = 'note'
NOTE_INDICE_ROW_LABEL = 'indice de la note'
STRING_DATA_ROW_LABEL = 'données corde'
HAMMER_DATA_ROW_LABEL = 'données marteau'
COLUMNS_DESC_ROW_LABEL = 'grandeur'
COLUMNS_DESC_ROW_NOTATION = 'notation'


def parse_plan_cordes(fpath: Path) -> pandas.DataFrame:
    df = pandas.read_excel(fpath.as_posix(), index_col=1, skiprows=[1],
                           dtypes={'Nc': 'int'})
#    instance = json.loads(df.to_json(orient='table'))['data']
#    utils.jsonschema_validate(instance, 'plan-cordes')
    return df


def parse_plan_cordes_materiaux(fpath: Path) -> pandas.DataFrame:
    df = pandas.read_excel(fpath.as_posix(),
                           sheet_name=1,
                           index_col=0,
                           skiprows=[1],
                           dtypes={'Nc': 'int'})
#    instance = json.loads(df.to_json(orient='table'))['data']
#    utils.jsonschema_validate(instance, 'plan-cordes')
    return df


def write_montjoie_inifile(notes: pandas.DataFrame,
                           materiaux: pandas.DataFrame,
                           note_playing: Dict[str, Any],
                           parameters: ConfigParser, workspace: Path,
                           output_directory: Path, fobj: IO[Text]) -> None:

    display_frequency = 0.1
    note_idx = int(note_playing['index'])
    props = dict(notes.loc[note_idx].items())
    B = np.array(CallInharmoCoeff())[note_idx - 1]
    bridge = props['alpha']

    Ru = '{:.4g}'.format(max(5e-3 * note_idx - 0.015, 0))
    Rv = 0.5
    Rphi = Ru
    etau = '{:.4g}'.format(2.78e-11 * note_idx + 1.5274e-9)
    etav = 1e-9
    etaphi = etau

    # Computation parameters
    time_step = parameters['time_step']
    time_step = 1e-6
    nsteps = parameters['nsteps']
    freq_max = parameters['max_freq']
    dt_new = 1 / (10 * freq_max)
    dt_new = max(5e-6, dt_new)

    # parameters is no longer present in plan-cordes file or previously in
    # parameters_module3.xls file
    # Hammer properties
    Mh = '{:.4g}'.format(-0.000062348 * note_idx + 0.0112)
    Kh = 10**(5.3097 * 0.01 * note_idx + 7.6425)
    ph = '{:.2f}'.format(2.4295E-4 * note_idx**2 - 0.007703 * note_idx + 2.337)
    Rh = '{:.2f}'.format(Kh * (10**(-0.04366 * note_idx - 2.294)))
    Kh = '{:.2f}'.format(Kh)
    is_struck = True
    kprime = 0.85
    el_H = -0.02001
    delta_d = 0.02
    e_M = 0.02
    hammer_slope = 2001

    theta = 0.25
    f0 = props["F_c"]
    
    nsteps = int(np.ceil(2.2 * freq_max / f0))
    if nsteps < 20:
        nsteps = 20
    if nsteps > 150:
        nsteps = 150
    curMtxAme = props['MatAme']
    curMtxFilInt = props['MatFilInt']
    curMtxFilExt = props['MatFilExt']

    matNames = list(materiaux.Name.get_values())
    idxMatAme = matNames.index(curMtxAme) + 1
    idxMatFilInt = matNames.index(curMtxFilInt) + 1
    idxMatFilExt = matNames.index(curMtxFilExt) + 1

    rhoAme = materiaux.rho.get_value(idxMatAme)
    youngAme = materiaux.E.get_value(idxMatAme)
    shearAme = materiaux.G.get_value(idxMatAme)

    rhoFilInt = materiaux.rho.get_value(idxMatFilInt)
    rhoFilExt = materiaux.rho.get_value(idxMatFilExt)

    mu = rhoAme * np.pi * (props['D0'] / 1000 / 2)**2 + \
        rhoFilInt * (np.pi / 4) * np.pi * (props['D1'] / 1000 / 2)**2 + \
        rhoFilExt * (np.pi / 4) * np.pi * (props['D2'] / 1000 / 2)**2

    Leq = props["Lstr"]
    T0 = mu * (2 * Leq * f0)**2

    if props["D1"] > 0 or props["D2"] > 0:
        deq = (64 * B * T0 * Leq**2 / (np.pi**3 * youngAme))**0.25
    else:
        deq = props["D0"] / 1000

    seceq = (math.pi * deq**2 / 4)
    epsilon = 1e-10
    locimpact = '{:.4g}'.format(props['lambda_H'] * Leq)
    # detuning parameter has no "notation" column to be used as a key, thus
    # it's not retrieved
    detuning = 1
    string = ' '.join(map(str, [
        Leq,
        '{:.4g}'.format(seceq),
        '{:.2f}'.format(mu / seceq),
        '{:.2f}'.format(T0),
        youngAme,
        '{:4g}'.format(math.pi * deq**4 / 64),
        shearAme,
        kprime,
        'PHI_DAMPING',
        Ru,
        Rv,
        Rphi,
        etau,
        etav,
        etaphi,
        detuning,  
        nsteps,
        int(is_struck),        
        Kh,
        Rh,
    ]))
    hammer = ' '.join(map(str, [
        'YES',
        Mh,
        locimpact,
        note_playing['initial_velocity'],
        ph,
        e_M,
        hammer_slope,
        delta_d,
        el_H,
        epsilon,
    ]))
    impedance = ' '.join(
        (workspace / fname).as_posix() for fname in [
            Path('Mass.txt'), Path('Stiffness.txt'), Path('Damping.txt'),
            Path(str(note_idx)) / 'Amplitude.txt',
        ])
    start_time = note_playing['start_time']
    stop_time = note_playing['stop_time']
    fobj.write('TimeInterval = {} {}\n'.format(start_time, stop_time))
    fobj.write('TimeStep = {}\n'.format(time_step))
    fobj.write('DisplayFrequency = {}\n'.format(display_frequency))
    nstrings = props['Nc']
    fobj.write('String = {}\n'.format(string) * nstrings)
    fobj.write('Hammer = {}\n'.format(hammer))
    fobj.write('Bridge = {}\n'.format(
        ' '.join([str(bridge)] * nstrings))
    )
    fobj.write('Impedance = {}\n'.format(impedance))
    fobj.write('StringSolver = THETA_NL_QUARTER  {:.4g} 0.25\n'.format(theta))
    fobj.write('StringPermutations = PERMUT_ALL\n')
    fobj.write('StringInitialData = NONE\n')
    fobj.write('StringSismoTimeGrid = {} {} {:.4g}\n'.format(
        start_time, stop_time, dt_new)
    )
    # Trailing / is needed as MONTJOIE does not join paths properly.
    fobj.write('DirectoryOutputString = {}\n'.format(
        output_directory.as_posix() + '/')
    )
    fobj.write('FileOutputString = SismoLM_Impedance-{idx}.txt '
               'SismoHammerImpedance-{idx}.txt '
               'ForceMarteauImpedance-{idx}.txt '
               'EnergyNoteImpedance-{idx}.txt\n'.format(idx=note_idx))
    fobj.write('FileOutputSismoString = SismoStringImpedance_ '
               'SismoLastPointImpedance_ SismoCrushImpedance_ ValImpedance_\n')
    fobj.write('Energy = {} {} 1e-4 1000\n'.format(start_time, stop_time))
    fobj.write('NewtonParam = 1e-9 20 1e-9\n')
    fobj.write('DataNonRegressionTest = StringImpedanceStiffNL.x 0 1\n')


def run(global_config: ConfigParser, settings: SectionProxy) -> None:
    start_montjoie = time.time()
    parameters = _config.read_json_config(settings['parameters'])
    solver = parameters['solver'].lower()
    _oscillateurs_chevalet.run(_io.OUTPUTS['modes-table'][0],
                               solver,
                               settings['plan-cordes'],
                               settings['geometry'])
    montjoie_basedir = Path(global_config['montjoie']['path']) / 'MONTJOIE'
    ld_library_path = montjoie_basedir / 'lib'
    executable = 'StringImpedanceStiffNL.x'
    exec_path = montjoie_basedir / executable
    if not exec_path.exists():
        raise OSError("executable {} not found in {}".format(
            executable, montjoie_basedir)
        )
    env = os.environ.copy()
    env["LD_LIBRARY_PATH"] = ld_library_path.as_posix()
    workspace = utils.workspace()
    notes = parse_plan_cordes(Path(settings['plan-cordes']))
    materiaux = parse_plan_cordes_materiaux(Path(settings['plan-cordes']))
    if Path(settings['jeu']).suffix == '.mid':
        jsonPath = midi2json(Path(settings['jeu']))
        with jsonPath.open() as f:
            notes_playing = json.load(f)
    elif Path(settings['jeu']).suffix == '.json':
        with Path(settings['jeu']).open() as f:
            notes_playing = json.load(f)
    utils.jsonschema_validate(notes_playing, 'jeu')
    (workspace / 'montjoie').mkdir(exist_ok=True)
    impedance_file = workspace / 'SismoLM_Impedance.txt'
    h5_result = workspace / _io.OUTPUTS['marteau-corde'][0]
    if h5_result.exists():
        warnings.warn('existing file {} deleted'.format(h5_result))
        h5_result.unlink()
    montjoie_wspace = utils.montjoie_workspace()
    xplayed = []
    yplayed = []

    xy_pts = np.loadtxt(workspace / 'montjoie' / 'Coupling.txt')
    index_notes = []
    start_time = []

    currNote = 1
    for note_playing in notes_playing['notes']:
        idx = note_playing['index']
        if idx < 71:
            isMuffler = True
        else:
            isMuffler = False
        velocity = note_playing['initial_velocity']
        props = dict(notes.loc[idx].items())
        f0 = props["F_c"]
        numString = props["Nc"]
        outdir = utils.montjoie_workspace(idx)
        inputfile = workspace / ('string-impedance-{}.ini'.format(idx))
        xplayed.append(xy_pts[int(idx - 1), 0])
        yplayed.append(xy_pts[int(idx - 1), 1])
        index_notes.append(idx)
        start_time.append(note_playing['start_time'])
        with inputfile.open('w') as f:
            write_montjoie_inifile(notes,
                                   materiaux,
                                   note_playing, parameters,
                                   montjoie_wspace, outdir, f)
        cmd = [exec_path.as_posix(), inputfile.as_posix()]
        subprocess.run(cmd, env=env, check=True)
        current_result = outdir / 'SismoLM_Impedance-{}.txt'.format(idx)
        if not current_result.exists():
            raise RuntimeError(
                "impedance result file {} not produced".format(
                    current_result)
            )
        shutil.copy(current_result.resolve().as_posix(),
                    workspace.as_posix())
        duration = 0
        with h5py.File(h5_result.as_posix(), 'a') as h5f, \
                current_result.open() as csvf, \
                impedance_file.open('a') as txtf:
            data = np.loadtxt(csvf)
            tps = data[:, 0]
            dt = np.median(np.diff(tps))
            force = 0
            for kch in range(numString):
                force += data[:, 1+kch]
            fs = int(1/dt)

            forceNew = mufflingtone(force, fs, f0, velocity, isMuffler)
            tpsNew = np.arange(len(forceNew)) / fs + tps[0]
            duration = max(duration, tps[-1])
            data = np.concatenate((tpsNew.reshape(-1, 1),
                                   forceNew.reshape(-1, 1)), axis=1)

            # Only save the second column, which corresponds to the vertical
            # force applied to the sound board.
            h5f.create_dataset(str(currNote), data=data[:, :2])
            csvf.seek(0)
            txtf.write(csvf.read())

        currNote += 1

    # Save the coupling point coordinates
    with h5py.File(h5_result.as_posix(), 'a') as h5f:
        h5f.create_dataset('x_coupling', data=xplayed)
        h5f.create_dataset('y_coupling', data=yplayed)
        h5f.create_dataset('note_index', data=index_notes)
        h5f.create_dataset('start_time', data=start_time)
        h5f.create_dataset('duration', data=duration+0.25)

    eltime = time.time() - start_montjoie
    print('Marteau-corde returned without error in ' +
          '%.2f' % eltime + ' seconds')
