#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Mon May 13 15:48:57 2019

@author: qizhang

"""
import json
import mido
import numpy as np
from pathlib import Path


def midi2json(midifile):

    # mid est l'objet contenant l'ensemble des informations du fichier midi
    mid = mido.MidiFile(str(midifile))

    # informations générales
    tracks = mid.tracks
    tpb = mid.ticks_per_beat

    # numTracks est le nombre de pistes
    numtracks = len(tracks)

    # Initialisations
    submit = 'tmp.json'
    result = []
    tempo = []
    tempoChange = []
    saveTempo = []
    saveTempoChange = []
    duration = 0

    # On boucle sur les pistes
    for i in range(numtracks):

        track = tracks[i]
        # Inialisations
        time_vec = []
        noteNum = []
        vcty = []
        valueToAdd = 0
        currentTime = 0
        currentTickDuration = 0

        # si au moins deuxieme piste, on repart du premier tempo
        if saveTempo != []:
            currentTickDuration = saveTempo[0] / tpb / 1e6
            currentTempo = saveTempo[0]
            tempo = [x for x in saveTempo]
            tempoChange = [x for x in saveTempoChange]

        # On boucle sur les messages
        for m in range(len(track)):
            msg_d = track[m]
            # S'il y a un message de temps
            if hasattr(msg_d, 'time') and not hasattr(msg_d, 'note'):
                addTime = True
                valueToAdd += msg_d.time * currentTickDuration

            currentTime += msg_d.time * currentTickDuration

            if hasattr(msg_d, 'tempo'):
                currentTempo = msg_d.tempo
                tempo.append(msg_d.tempo)
                tempoChange.append(currentTime)
                saveTempo.append(msg_d.tempo)
                saveTempoChange.append(currentTime)
                currentTickDuration = currentTempo / tpb / 1e6

            if hasattr(msg_d, 'note'):
                vcty.append(msg_d.velocity)
                eventTime = msg_d.time * currentTickDuration

                # Si la note suit directement après le changement de tempo
                if addTime:
                    # On ajoute le temps associé de changement de tempo
                    eventTime += valueToAdd
                    addTime = False
                    valueToAdd = 0
                time_vec.append(eventTime)
                noteNum.append(msg_d.note)

            # Si on repasse sur un autre tempo
            if i > 0 and tempo != []:
                if currentTime >= tempoChange[0]:
                    currentTempo = tempo[0]
                    tempo.pop(0)
                    tempoChange.pop(0)
                currentTickDuration = currentTempo / tpb / 1e6
        # Passage de velocite MIDI a vitesse du marteau
        vcty_ms = list(128 / (4.5 * (134.3 - np.array(vcty))))
        th_sum_time = list(np.cumsum(time_vec))

        iterVar = 0
        while iterVar < len(noteNum):

            a = noteNum[iterVar]
            b = noteNum.index(a, iterVar + 1, len(noteNum))

            if th_sum_time[iterVar] < 25:
                prop_dict = {}
                prop_dict['index'] = a - 20
                prop_dict['start_time'] = float('%.2f' % th_sum_time[iterVar])
                prop_dict['stop_time'] = float('%.2f' % th_sum_time[b])
                prop_dict['initial_velocity'] = float('%.2f' % vcty_ms[iterVar])  
                result.append(prop_dict)
                duration = max(duration, th_sum_time[b])
                
            del(noteNum[b])
            del(th_sum_time[b])
            del(vcty_ms[b])
            iterVar += 1

    sup_dict = {}
    sup_dict['duration'] = float('%.2f' % duration)
    sup_dict['notes'] = result

    with open(submit, 'w') as f:
        json.dump(sup_dict, f)

    return Path(submit)
