from collections import OrderedDict

import click

from ._config import (
    GlobalConfig,
    initialize_global_config,
    read_config,
    initialize_settings,
)
from . import (
    _lva,
    _montjoie,
)


@click.version_option()
@click.group()
def cli():
    pass


@cli.command(
    name='init-config',
    help='initialize global configuration',
)
@click.option(
    '--force', '-f', is_flag=True, default=False,
    help='force overridding of an already existing configuration file',
)
def init_config(force):
    config_fpath = GlobalConfig.file_path()
    if config_fpath.exists() and not force:
        raise click.ClickException(
            'file {} already exists, use --force to override'.format(
                config_fpath)
        )
    initialize_global_config(config_fpath)
    click.echo(
        "configuration initiliazed at {}; "
        "now edit this file to complete the process".format(
            config_fpath)
    )


@cli.command(
    'new',
    help='initialize a project from a "settings" file',
)
@click.argument(
    'settings', type=click.Path(exists=False),
)
def init_settings(settings):
    initialize_settings(settings)


_modules = OrderedDict()


def _register(name):
    def decorator(func):
        _modules[name] = func
        return func
    return decorator


@_register('modes-table')
def cmd_modes_table(config, settings):
    _lva.ModesTableRunner(config, settings).run_matlab()


@_register('marteau-corde')
def cmd_marteau_corde(config, settings):
    _montjoie.run(config, settings)


@_register('mouvement-table')
def cmd_mouvement_table(config, settings):
    _lva.MouvementTableRunner(config, settings).run_matlab()


@_register('rayonnement')
def cmd_rayonnement(config, settings):
    _lva.RayonnementRunner(config, settings).run_matlab()


@cli.command(
    name='run',
    help='run one or all module(s)',
)
@click.argument(
    'settings', type=click.Path(exists=True),
)
@click.option(
    '--module', '-m',
    type=click.Choice(list(_modules)),
)
@click.option(
    '--from-module', '-f',
    type=click.Choice(list(_modules)),
    help='run simulation from a given module',
)
def run(settings, module, from_module):
    config = GlobalConfig()
    config.load()
    settings = read_config(settings)
    if module is not None:
        if from_module is not None:
            raise click.BadParameter(
                "--module and --from-module option are mutually exclusive"
            )
        _modules[module](config, settings[module])
    else:
        for module, cmd in _modules.items():
            if from_module is not None:
                if module != from_module:
                    continue
                from_module = None
            click.echo('\n* running module {}'.format(module))
            cmd(config, settings[module])
