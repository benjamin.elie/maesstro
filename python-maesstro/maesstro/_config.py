import configparser
from pathlib import Path
from typing import Any
import json


class GlobalConfig(configparser.ConfigParser):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        kwargs['strict'] = True
        super().__init__(*args, **kwargs)

    @classmethod
    def file_path(cls) -> Path:
        import xdg  # inline import to allow mocking os.environ in tests.
        return Path(xdg.XDG_CONFIG_HOME) / 'maesstro' / 'config.ini'

    def load(self) -> None:
        """Load options from user configuration file, failing if it does not
        exist.
        """
        with self.file_path().open() as f:
            self.read_file(f)


def initialize_global_config(fpath: Path) -> None:
    config = configparser.ConfigParser(strict=True)
    config['lva'] = {
        'modes-table': '',
        'mouvement-table': '',
        'rayonnement': '',
    }
    config['matlab'] = {
        'runtime-path': '/usr/local/MATLAB/MATLAB_Runtime/v94/',
    }
    config['montjoie'] = {
        'path': str(Path.home() / 'montjoie'),
    }
    config_dir = fpath.parent
    if not config_dir.exists():
        config_dir.mkdir(parents=True)
    with fpath.open('w') as f:
        config.write(f)


def read_config(fpath: str) -> configparser.ConfigParser:
    """Parse `fpath` as a .INI file and return a ConfigParser instance."""
    config = configparser.ConfigParser(strict=True)
    with open(fpath) as f:
        config.read_file(f)
    return config


def read_json_config(fpath: str) -> dict:
    """Parse `fpath` as a .json file and return a dict instance."""
    with Path(fpath).open() as f:
        return json.load(f)


def initialize_settings(path: str) -> None:
    fpath = Path(path).absolute()
    projectdir = fpath.parent

    def filepath(name: str) -> str:
        return (projectdir / name).as_posix()

    config = configparser.ConfigParser(strict=True)
    config['modes-table'] = {
        'parameters': filepath('generalparameters.json'),
        'geometry': filepath('piano.json'),
        'material': filepath('materiau.csv'),
    }
    config['marteau-corde'] = {
        'parameters': filepath('generalparameters.json'),
        'plan-cordes': filepath('plan_cordes.xls'),
        'jeu': filepath('jeu.mid'),
        'geometry': filepath('piano.json')
    }
    config['mouvement-table'] = {
        'parameters': filepath('generalparameters.json'),
    }
    config['rayonnement'] = {
        'parameters': filepath('generalparameters.json'),
    }
    with fpath.open('w') as f:
        config.write(f)
