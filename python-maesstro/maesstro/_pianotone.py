#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 14:31:23 2019

@author: benjamin
"""

import numpy as np
from scipy import signal as ssg
from scipy.linalg import hankel
from sklearn.linear_model import LinearRegression


def poleesprit(s, K=20, fs=1):

    nSig = len(s)
    M = min(int(2*K), np.floor(nSig / 2).astype(int))
    Nl = nSig - M + 1
    Nt = int(np.floor(Nl / M))
    R = np.zeros((M, M))
    for kSig in range(Nt):
        deb = kSig * M
        fin = deb + 2 * M - 1
        xtmp2 = s[deb:fin]
        H = hankel(xtmp2[0:M], xtmp2[M - 1:len(xtmp2)])
        R += H @ H.T

    u, s, d = np.linalg.svd(R)
    nx, ny = u.shape

    Up = u[1:nx, 0:K]
    Um = u[0:nx - 1, 0:K]

    Phi = np.linalg.pinv(Um) @ Up
    z, w = np.linalg.eig(Phi)
    freq = np.angle(z) / 2. / np.pi * fs
    alp = -fs * np.log(abs(z))

    cond = (freq > 0) * (freq < fs / 2)
    alp = alp[cond]
    freq = freq[cond]

    isrt = np.argsort(freq)
    freq = np.sort(freq)

    alp = alp[isrt]
    return np.exp(2 * 1j * np.pi * freq / fs) * np.exp(-alp / fs)


def signalreconstruction(s, z):

    t = np.arange(len(s)).reshape(-1, 1)
    V = np.exp(t.dot(np.log(z).reshape(1, -1)))
    bk, dum1, dum2, dum3 = np.linalg.lstsq(V, s.reshape(-1, 1))
    sp = 2 * np.real(V.dot(bk.reshape(-1, 1))).reshape(len(s))
    sres = s - sp
    bk = bk.reshape(len(bk))

    return bk, sp, sres


def mufflingtone(s, fs, fo, velocity, isMuffler):

    nHarm = fs / 2 / fo
    K = int(2.2 * nHarm + 2)

    if isMuffler:
        avec = np.array([10.85294005,
                         5.96801726,
                         5.50031435,
                         3.91138653,
                         4.96272957])

        bvec = np.array([ -8.812294670217101,
                         9.566326909567415271e-01,
                         4.989209153737430569e-01,
                         3.708924496945606109,
                         2.329127051833076223])

        if velocity < 0.33:
            a = avec[-1]
            b = bvec[-1]
        elif velocity < 0.52:
            a = avec[-2]
            b = bvec[-2]
        elif velocity < 0.74:
            a = avec[-3]
            b = bvec[-3]
        elif velocity < 1.28:
            a = avec[1]
            b = bvec[1]
        elif velocity >= 1.28:
            a = avec[0]
            b = bvec[0]

        alpha_e = a * np.log10(fo) + b

    idxMax1 = np.argmax(abs(s))
    force_decay = s[idxMax1:]
    force_decay = force_decay - np.mean(force_decay)
    if len(force_decay) > int(0.3*fs):
        force_decay = force_decay[:int(0.3*fs)]
    zk = poleesprit(force_decay, K=K, fs=fs)
    bk, dum1, dum2 = signalreconstruction(s, zk)

    nc = len(s) - 1
    if isMuffler:
        omk = np.angle(zk)
        zprimek = np.exp(1j * omk) * np.exp(-alpha_e / fs)
        bprimek = bk * (zk**nc) / (zprimek**nc)

        Vprime = np.exp(np.arange(nc + fs).reshape(-1, 1) @
                        np.log(zprimek).reshape(1, -1))
        sprime = 2 * np.real(Vprime @ bprimek.reshape(-1, 1))

        setouffee = sprime[nc+1:].squeeze()
        sRecon = np.concatenate((s, setouffee))
    else:
        Vprime = np.exp(np.arange(nc + 4*fs).reshape(-1, 1) @
                        np.log(zk).reshape(1, -1))
        sRecon = 2 * np.real(Vprime @ bk.reshape(-1, 1)).squeeze()

    nWin = 4096

    sExt = np.concatenate((np.zeros(int(nWin / 2)),
                           sRecon,
                           np.zeros(int(nWin / 2))))
    envRMS = np.zeros(sRecon.shape)
    for k in range(len(sRecon)):
        w = sExt[k:k + nWin]
        w2 = 1 / nWin * np.sum(np.abs(w)**2)
        envRMS[k] = w2

    idx = np.argwhere(100 * envRMS / max(envRMS) < 0.01)
    if len(idx) == 0:
        idx = len(sRecon)
    else:
        idx = idx[0, 0]
    return sRecon[:idx]

class Pianotone:

    signal = []
    sampling_frequency = []
    subband = []
    time_vector = []
    spectrum_frequency_vector = []
    spectrum = []
    spectrogram_frequency_vector = []
    spectrogram_time_vector = []
    spectrogram = []

    fundamental_frequency = []
    gross_partial_frequency = []
    partial_frequency = []
    partial_damping = []
    partial_amplitude = []
    partial_EDC = []
    partial_T15 = []
    reconstructed_signal = []
    residual_signal = []
    pole_frequency = []
    pole_damping = []
    pole_amplitude = []
    global_EDC = []
    global_T15 = []
    inharmonicity = []
    inharmonicity_frequency = []

    spectral_centroid = []
    spectral_spread = []
    skewness = []
    kurtosis = []

    # Constructor method
    def ___init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def computespectrogram(self, winLength=256, win=np.hanning(256),
                           nfft=256, novlp=256 * 3 / 4, fs=2e4):
        f, t, Sxx = ssg.spectrogram(self.signal, fs, win,
                                    winLength, novlp, nfft)
        self.spectrogram = Sxx
        self.spectrogram_frequency_vector = f
        self.spectrogram_time_vector = t
        return f, t, Sxx

    def computespectrum(self, win='rect', nfft=2048, fs=2e4):
        N = len(self.signal)
        if win == 'rect':
            w = np.ones(N)
        if win == 'hann':
            w = np.hanning(N)
        if win == 'hamming':
            w = np.hamming(N)

        sig2fft = self.signal * w
        sp = np.fft.fft(sig2fft, nfft)
        freq = np.fft.fftfreq(nfft, 1 / fs)
        L = int(nfft / 2)
        self.spectrum = sp[0:L]
        self.spectrum_frequency_vector = freq[0:L]
        return freq[0:L], sp[0:L]

    def subbandfiltering(self, fmax=3e3, ordFilt=2000):

        fs = self.sampling_frequency
        if fmax > fs / 2:
            fmax = fs / 2

        sg = self.signal
        nSig = len(sg)
        ordFilt = min(ordFilt, int(nSig / 4))

        fo = self.fundamental_frequency
        band = fo / 8

        if self.spectrum == []:
            self.computespectrum('rect', int(2**np.ceil(np.log2(abs(fs)))), fs)

        Pxx = abs(self.spectrum)**2
        freq = self.spectrum_frequency_vector
        fPeak = fo
        numHarm = int(np.floor(fmax / fo))
        fc = []
        filteredSignal = np.zeros((numHarm, nSig - (ordFilt + 1)))

        for k in range(numHarm):

            idx1 = np.argwhere(freq > fPeak - 10)[0, 0].astype(int)
            idx2 = np.argwhere(freq < fPeak + 10)[-1, -1].astype(int)
            Ptmp = Pxx[idx1:idx2]
            idxMax = np.argmax(Ptmp) + idx1
            fPeak = freq[idxMax]
            fc.append(fPeak)
            fcoef = ssg.firwin(ordFilt, band / (fs / 2))
            bb = fcoef * np.cos(2 * np.pi * fPeak / fs * np.arange(len(fcoef)))
            xwf = ssg.filtfilt(bb, 1, sg[::-1])[::-1]
            filteredSignal[k, :] = xwf[:-len(bb) - 1]
            fPeak += fo

        self.subband = filteredSignal
        self.gross_partial_frequency = np.array(fc)
        return filteredSignal

    def globalesprit(self, K=20):

        fs = self.sampling_frequency
        fo = self.fundamental_frequency
        s = self.signal

        idxMax1 = np.argmax(abs(s))
        xMax = s[idxMax1:]
        sref = xMax
        x = xMax - np.mean(xMax)

        nHarm = np.floor(fs / 2 / fo)
        K = 4 * nHarm + 2
        z = poleesprit(x, K, fs)
        freq = np.angle(z) / 2 / np.pi * fs
        alp = -fs * np.log(abs(z))
        b, srec, sres = signalreconstruction(sref, z)

        self.reconstructed_signal = np.concatenate((self.signal[0:idxMax1],
                                                    srec))
        self.residual_signal = sres
        self.pole_frequency = freq
        self.pole_damping = alp
        self.pole_amplitude = b

    def esprit(self, K=2):

        fs = self.sampling_frequency
        fo = self.fundamental_frequency
        band = fo / 4

        if self.subband == []:
            self.subbandfiltering(fs / 2)

        fc = self.gross_partial_frequency
        nHarm, nSig = self.subband.shape
        pFreq = []
        pDamp = []
        pZ = []
        idxnan = []
        idxfinite = []
        idxMax1 = np.argmax(self.signal)
        xMax = self.signal[idxMax1:]

        for kHarm in range(nHarm):
            xtmp = self.subband[kHarm, :]
            xtmp = xtmp[idxMax1:]
            idxMaxPartial = np.argmax(xtmp)
            xtmp = xtmp[idxMaxPartial:]
            nSig = len(xtmp)
            M = min(100, np.floor(nSig / 2).astype(int))
            Nl = nSig - M + 1
            Nt = int(np.floor(Nl / M))
            R = np.zeros((M, M))
            for kSig in range(Nt):
                deb = kSig * M
                fin = deb + 2 * M - 1
                xtmp2 = xtmp[deb:fin]
                H = hankel(xtmp2[0:M], xtmp2[M - 1:len(xtmp2)])
                R = R + H @ H.T

            u, s, d = np.linalg.svd(R)
            nx, ny = u.shape

            Up = u[1:nx, 0:K]
            Um = u[0:nx - 1, 0:K]

            Phi = np.linalg.pinv(Um) @ Up
            z, w = np.linalg.eig(Phi)
            z = z[abs(z) <= 1]
            freq = np.angle(z) / 2. / np.pi * fs
            alp = -fs * np.log(abs(z))
            cond = (freq > fc[kHarm] - band) * (freq < fc[kHarm] + band)
            idx = np.argwhere(cond)[:, 0].astype(int)

            if len(idx) > 0:
                freq = freq[idx]
                alp = alp[idx]
                z = z[idx]
                pFreq.append(freq.tolist())
                pDamp.append(alp.tolist())
                pZ.append(z.tolist())
                idxfinite.append(kHarm)
            else:
                freq = [np.nan]
                alp = [np.nan]
                z = [np.nan]
                pFreq.append(freq)
                pDamp.append(alp)
                pZ.append(z)
                idxnan.append(kHarm)

        pFreq = np.array(pFreq).reshape(-1, 1)[:, 0].squeeze()
        pDamp = np.array(pDamp).reshape(-1, 1)[:, 0].squeeze()
        pZtmp = np.array([x for x in pZ if np.isfinite(x)])
        pZ = pZtmp.reshape(-1, 1)[:, 0].squeeze()

        t = np.arange(len(xMax)).reshape(-1, 1)
        V = np.exp(t.dot(np.log(pZ).reshape(1, -1)))
        bk, dum1, dum2, dum3 = np.linalg.lstsq(V, xMax.reshape(-1, 1))
        sp = 2 * np.real(V.dot(bk.reshape(-1, 1))).reshape(len(xMax))
        if len(idxnan) > 0:
            bktmp = np.zeros(nHarm)
            for k in range(len(idxfinite)):
                bktmp[idxfinite[k]] = bk[k, 0]
            for k in range(len(idxnan)):
                bktmp[idxnan[k]] = np.nan
            bk = bktmp

        self.reconstructed_signal = np.concatenate((self.signal[0:idxMax1],
                                                    sp))
        self.partial_frequency = pFreq
        self.partial_damping = pDamp
        self.partial_amplitude = bk

    def computeEDC(self, isFilt=True):

        fs = self.sampling_frequency
        idxMax1 = np.argmax(self.signal)
        xMax = self.signal[idxMax1:]
        x = abs(xMax)**2
        global_EDC = 10 * np.log10(np.cumsum(x[::-1])[::-1])
        global_EDC = global_EDC - max(global_EDC)
        self.global_EDC = global_EDC

        partial_EDC = []
        if isFilt:
            if self.subband == []:
                self.subbandfiltering(fs / 2)

            nHarm, nSig = self.subband.shape
            partial_EDC = np.zeros((nHarm, nSig - idxMax1))
            for kHarm in range(nHarm):
                xtmp = self.subband[kHarm, :]
                xtmp = abs(xtmp[idxMax1:])**2
                EDC_tmp = 10 * np.log10(np.cumsum(xtmp[::-1])[::-1])
                partial_EDC[kHarm, :] = EDC_tmp - max(EDC_tmp)

        self.partial_EDC = partial_EDC
        return global_EDC, partial_EDC

    def computeT15(self, isFilt=True):

        if self.global_EDC == []:
            self.computeEDC(isFilt)

        gedc = self.global_EDC
        pedc = self.partial_EDC
        nSmpl = len(gedc)
        tvec = (np.arange(nSmpl) / self.sampling_frequency)
        idx = range(int(nSmpl * 1 / 2))
        tidx = tvec[idx].reshape(-1, 1)
        gtmp = gedc[idx]

        model = LinearRegression().fit(tidx, gtmp)
        slope = model.coef_
        gT15 = -15 / slope

        pT15 = []
        if isFilt:
            nHarm, nSig = pedc.shape
            pT15 = np.zeros(nHarm)
            for k in range(nHarm):
                tedc = pedc[k, idx]
                model = LinearRegression().fit(tidx, tedc)
                slope = model.coef_
                pT15[k] = -15 / slope

        self.global_T15 = gT15
        self.partial_T15 = pT15
        return gT15, pT15

    def computespectralmoments(self, winLength=256, win=np.hanning(256),
                               nfft=256, novlp=256 * 3 / 4, fs=2e4):

        if self.spectrogram == []:
            self.computespectrogram(winLength, win, nfft, novlp, fs)

        f = self.spectrogram_frequency_vector
        Sxx = abs(self.spectrogram)**2

        nFreq, nFrame = Sxx.shape
        cgs = np.zeros(nFrame)
        spread = np.zeros(nFrame)

        for k in range(nFrame):
            Sxxtmp = Sxx[:, k]
            cgs[k] = np.sum(Sxxtmp * f) / np.sum(Sxxtmp)
            spread2 = np.sum((f - cgs[k])**2 * Sxxtmp) / np.sum(Sxxtmp)
            spread[k] = np.sqrt(spread2)

        self.spectral_centroid = cgs
        self.spectral_spread = spread

        return cgs, spread

    def computeinharmonicity(self):

        if self.partial_frequency == []:
            self.esprit(2)

        fk = self.partial_frequency
        fo = self.partial_frequency[0]
        nHarm = len(fk)
        n = np.arange(1, nHarm + 1)

        Best = np.sum(fk**2 - (n * fo)**2) / np.sum(n**4 * fo**2)
        self.inharmonicity = Best

        fkinharmo = n * fo * np.sqrt(1 + Best * n**2)
        self.inharmonicity_frequency = fkinharmo

        return Best, fkinharmo
