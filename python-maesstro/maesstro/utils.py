import json
import logging
from pathlib import Path
import pkgutil
from typing import (
    Dict,
    List,
    Optional,
    Union,
)

import jsonschema
from shapely.geometry import Polygon

LOGGER = logging.getLogger(__name__)


def workspace() -> Path:
    """Returh the Path object for computation workspace."""
    return Path.cwd()


def montjoie_workspace(idx: Optional[int] = None) -> Path:
    """Create and return the output directory relative to the workspace for
    computation on note `idx`.
    """
    wspace = workspace()
    outdir = wspace / 'montjoie'
    if idx is not None:
        outdir = outdir / str(idx)
    outdir.mkdir(exist_ok=True)
    return outdir


_primitive_value = Union[str, int, float, Dict, List, None]
_object = Dict[str, _primitive_value]
_array = List[_primitive_value]
json_value = Union[_primitive_value, _object, _array]


def jsonschema_validate(instance: json_value, schema_name: json_value) -> None:
    """Validate `instance` (JSON value) against the JSON Schema named
    `schema_name`.
    """
    raw_schema = pkgutil.get_data(
        'maesstro', 'schemata/{}.json'.format(schema_name))
    if raw_schema is None:
        raise IOError('unable to locate "{}" schema file'.format(schema_name))
    schema_data = raw_schema.decode('utf-8')
    schema = json.loads(schema_data)
    jsonschema.validate(instance, schema)


# make polygon objects from list of coordinates
def makepolyfromxy(x, y):

    r = []
    for ksb in range(len(x)):
        r.append((x[ksb], y[ksb]))
    return Polygon(r)


def extents(f):
    delta = f[1] - f[0]
    return [f[0] - delta / 2, f[-1] + delta / 2]
