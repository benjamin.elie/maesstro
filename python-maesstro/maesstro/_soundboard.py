#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 11 14:31:23 2019

@author: benjamin
"""

import numpy as np
import h5py
import os


class Soundboard:

    # Geometry
    x_normalized = []
    y_normalized = []
    x_extended = []
    y_extended = []
    ref_point = []
    area = []
    width = []
    length = []
    thickness = []
    ribs = []
    bridge = []

    # Mechanical properties
    orthotropy_angle = []
    eigen_frequencies = []
    eigen_vectors = []
    modal_mass = []
    modal_stiffness = []
    modal_damping = []
    mobility = []
    basis_dim = []
    solver = []

    # Constructor method
    def ___init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)

    def readh5modes(self, h5file='SORTIES_MODES_TABLE.h5'):

        if os.path.isfile(h5file):
            h = h5py.File(h5file, 'r')
            mn = h['/masses_modales'][()]
            kn = h['/raideurs_modales'][()]
            Rn = h['/amortissements_modaux'][()]
            P = h['/coefficients_deformees'][()]
            nbr_p, nbr_q = h['/basis_dimension']
            width, Length = h['/soundboard_dimension']

            if P.shape[0] == len(mn):
                P = P.T
        self.modal_mass = mn
        self.eigen_vectors = P
        self.eigen_frequencies = np.sqrt(kn / mn) / 2 / np.pi
        self.modal_stiffness = kn
        self.modal_damping = Rn
        self.basis_dim = [int(nbr_p), int(nbr_q)]
        self.width = width
        self.length = Length

    def computemobility(self, excPoint, obsPoint, freqVec=np.arange(5001)):

        eta = self.damping
        xDim, yDim = self.basis_dim
        nMode = xDim * yDim
        nPts = excPoint.shape[0]
        nFreq = len(freqVec)
        omg = freqVec * 2 * np.pi
        genForce = np.zeros((nMode, nPts))

        mr = self.modal_mass
        kr = self.modal_stiffness

        U = self.eigen_vectors
        modemax = len(self.modal_mass)
        lbd = (2 * np.pi * self.eigen_frequencies)**2

        # Damping matrix
        Cr = np.eye(modemax) * eta * np.sqrt(np.diag(lbd)) * np.diag(mr)
        cr = np.diag(Cr)
        del Cr

        xe = excPoint[:, 0]
        ye = excPoint[:, 1]
        cpt = 0

        for q in range(1, yDim + 1):
            sinql = np.sin(q * np.pi * ye / self.length)
            for p in range(1, xDim + 1):
                genForce[cpt, :] = np.sin(p * np.pi * xe / self.width) * sinql
                cpt += 1

        genForce2 = U.T @ genForce

        a = np.zeros((modemax, nFreq, nPts)) +  \
        1j * np.zeros((modemax, nFreq, nPts))
        for k in range(nFreq):
            elem = (kr - mr * omg[k]**2 + 1j * omg[k] * cr)
            a[:, k, :] = np.diag(1 / elem) @ genForce2
            
        del genForce2, elem

        Pa = np.zeros((nMode, nFreq, nPts)) + \
        1j * np.zeros((nMode, nFreq, nPts))
        for k in range(nPts):
            Pa[:, :, k] = U @ a[:, :, k].squeeze()

        del U, a
        genForceTmp = genForce.reshape((nMode, 1, nPts))
        gF = np.tile(genForceTmp, (1, nFreq, 1))
        del genForce, genForceTmp

        W = np.sum(Pa * gF, axis=0).reshape((nFreq, nPts))
        del Pa, gF
        Y = 1j * np.tile(omg.reshape(-1, 1), (1, nPts)) * W

        self.mobility = Y
        
        Ymean = 10**np.mean(np.log10(abs(Y)), axis=0)
        
        return Y, Ymean


class Superstructure(Soundboard):

    # Geometry
    deport = []
    width = []
    length = []
    thickness = []
    area = []
    number = []
    x_normalized = []
    y_normalized = []
    obj_angle = []
    x_extended = []
    y_extended = []

    # Constructor method
    def ___init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)
