from configparser import ConfigParser, SectionProxy
from functools import wraps
from typing import (
    Any,
    Callable,
    Optional,
)

import h5py

from ._config import (
    GlobalConfig,
)
from . import (
    _lva,
    _montjoie,
)


__all__ = [
    'modes_table',
    'marteau_corde',
    'mouvement_table',
    'rayonnement',
]


def _load_config(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args: Any, config: Optional[ConfigParser] = None,
                **kwargs: Any) -> Any:
        if config is None:
            config = GlobalConfig()
            config.load()
        return func(*args, config=config, **kwargs)
    return wrapper


@_load_config
def modes_table(settings: SectionProxy, config: ConfigParser) -> h5py.File:
    """Run "modes-table" module and return the opened HDF5 results file.

    `settings` argument is a dict containing configuraton for "modes-table"
    module.
    """
    return _lva.ModesTableRunner(config, settings).run_module()


@_load_config
def marteau_corde(settings: SectionProxy, config: ConfigParser) -> h5py.File:
    """Run "marteau-corde" module.

    `settings` argument is a dict containing configuraton for "marteau-corde"
    module.
    """
    return _montjoie.run(config, settings)


@_load_config
def mouvement_table(settings: SectionProxy, config: ConfigParser) -> h5py.File:
    """Run "mouvement-table" module and return the opened HDF5 results file.

    `settings` argument is a dict containing configuraton for
    "mouvement-table" module.
    """
    return _lva.MouvementTableRunner(config, settings).run_module()


@_load_config
def rayonnement(settings: SectionProxy, config: ConfigParser) -> h5py.File:
    """Run "rayonnement" module and return the opened HDF5 results file.

    `settings` argument is a dict containing configuraton for "rayonnement"
    module.
    """
    return _lva.RayonnementRunner(config, settings).run_module()
