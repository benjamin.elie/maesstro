# List of authors

## IMSIA - ENSTA Paris

* Benjamin Elie
* Benjamin Cotté

## LMS - Ecole Polytechnique

* Xavier Boutillon

## Logilab SA

* Nicolas Chauvat
* Denis Laxalde
* Frank Bessou
* Viet Hung Nhu

## LVA - INSA Lyon

* Benjamin Trévisan



